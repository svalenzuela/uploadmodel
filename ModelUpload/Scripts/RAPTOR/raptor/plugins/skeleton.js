/**
 * Raptor Engine - Core
 * Copyright (c) 2010 RAPTORCODE STUDIOS
 * All rights reserved.
 */
 
 /**
 * Author: Kaj Dijksta
 */

raptorjs.skeleton = function() {
	this.bones = [];
	this.rootBone;
	
}

/**
 * Rendercallback gets called every frame
 * xmlNode visualScene
 */
raptorjs.skeleton.prototype.parseCollada = function( visualScene ) {
	
	var skeletonClass = this;
	
	$(visualScene).children().each(function(index, node){
		var type = $(node).attr("type");
		
		if(type == "JOINT") {
		
			// Recursively find all bones
			skeletonClass.rootBone = raptorjs.createObject("bone");
			skeletonClass.rootBone.name = "root";
			skeletonClass.rootBone.position = [0,0.859,0]
			skeletonClass.searchBones(node, skeletonClass.rootBone);
			
		} 
		
		console.log(index, node, type);
	});
	
	
	this.calculateWorldPos(this.rootBone);
	
	console.log(this);
}

raptorjs.skeleton.prototype.calculateWorldPos = function( parentBone ) {
	var children = parentBone.children;
	
	for(var c = 0; c<children.length; c++) {	
		var child = children[c];

		child.worldPosition = raptorjs.vector3.add( parentBone.position, child.position );
		
		
		this.calculateWorldPos(child);
	}

}

raptorjs.skeleton.prototype.searchBones = function( parentBoneNode, parentBoneObject ) {

	
	var skeletonClass = this;
	
	// find children bones
	$(parentBoneNode).children().each(function(i, node){
	
		if($(node).attr("name")){
			var bone = raptorjs.createObject("bone");
			bone.name = $(node).attr("id");
			bone.position = node.getElementsByTagName("translate")[0].firstChild.nodeValue.split(' ').map(parseFloat);
			//bone.scale = node.getElementsByTagName("scale")[0].firstChild.nodeValue.split(' ').map(parseFloat);
			
			var rotations = node.getElementsByTagName("rotate");
			
			for(var b = 0; b<rotations.length;b++) {
				var rotationNode = rotations[b];
				var sid = rotationNode.getAttribute("sid");
				
				switch(sid) {
					case "rotateX":
						bone.rotateX =  rotationNode.firstChild.nodeValue.split(' ').map(parseFloat);
					break;
					case "rotateY":
						bone.rotateY =  rotationNode.firstChild.nodeValue.split(' ').map(parseFloat);
					break;
					case "rotateZ":
						bone.rotateZ =  rotationNode.firstChild.nodeValue.split(' ').map(parseFloat);
					break;
				}
			}
			
			
			parentBoneObject.addChild(bone);
		
			skeletonClass.searchBones(node, bone);
		}
	});
	
	skeletonClass.bones.push(parentBoneObject);
}

raptorjs.bone = function() {
	this.name;
	
	this.worldPosition = [0,0,0];
	
	this.position = [0,0,0];
	this.rotationX;
	this.rotationY;
	this.rotationZ;
	this.scale;
	
	this.children = [];
}

raptorjs.bone.prototype.addChild = function(bone){
	this.children.push(bone);
}