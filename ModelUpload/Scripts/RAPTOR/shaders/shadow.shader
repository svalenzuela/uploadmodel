/**
 * Raptor Engine - Core
 * Copyright (c) 2010 RAPTORCODE STUDIOS
 * All rights reserved.
 */
  
/**
 * Author: Kaj Dijksta
 */
 
    attribute vec3 position;
    attribute vec2 uv;
	
    uniform mat4 viewProjection;

    varying vec2 v_textureCoord;

    void main(void) {
        v_textureCoord = uv;
		gl_Position = viewProjection * vec4(position, 1.0);
    }
	
	// #raptorEngine - Split
	
	precision highp float;

	uniform samplerCube shadowSampler;
	uniform sampler2D positionSampler;
	
	uniform float far;
	uniform mat4 lightViewProjection;
	uniform vec3 lightPosition;

	varying vec2 v_textureCoord;
	
	float DecodeFloatRGBA( vec4 rgba ) {
		return dot( rgba, vec4(1.0, 1.0 / 255.0, 1.0 / 65025.0, 1.0 / 160581375.0) );
	}
	
	vec2 calculateShadowOcclusion( vec3 worldPosition ) {
	
		float depth = length( lightPosition - worldPosition );
		float shadowBias = 0.001;
		
		vec4 projCoords = lightViewProjection *  vec4(worldPosition, 1.0);
		
		projCoords.xy /= projCoords.w;
		projCoords = 0.5 * projCoords + 0.5;
		
		float sampleDepth = DecodeFloatRGBA( textureCube(shadowSampler, projCoords.xyz) ) * far ;
		
		return vec2( ( depth < sampleDepth + shadowBias ) ? 1.0 : 0.03 );
	}
	
	
	void main() {
	
		vec3 worldPosition = texture2D(positionSampler, v_textureCoord).xyz;
		
		vec2 sample = calculateShadowOcclusion( worldPosition );
		
		gl_FragColor = vec4(sample, sample);
	}

	