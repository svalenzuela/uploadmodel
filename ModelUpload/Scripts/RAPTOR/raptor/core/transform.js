/*
 * Copyright 2013, Raptorcode Studios Inc, 
 * Author, Kaj Dijkstra.
 * All rights reserved.
 *
 */

raptorjs.transform = function(){
	this.entity = false;
	this.world = raptorjs.matrix4.identity();
	this.local = raptorjs.matrix4.identity();
	
	this.position = [0, 0, 0];
	this.scale = [1, 1, 1];
	this.rotation = [0, 0, 0];//Degrees
}


raptorjs.transform.prototype.translate = function(x,y,z) {
	this.position[0] += x;
	this.position[1] += y;
	this.position[2] += z;
}

raptorjs.transform.prototype.translateTo = function(x,y,z) {

	this.position = [x,y,z];

	this.update();
}

raptorjs.transform.prototype.update = function(x,y,z) {
	this.local = raptorjs.matrix4.identity();

	
	
	console.log(this.local);
	//this.local = raptorjs.matrix4.scale(this.local, this.scale);
	console.log(this.local);
	this.local = raptorjs.matrix4.translate(this.local, this.position);
	console.log(this.local);
	this.local = raptorjs.matrix4.rotateZYX(this.local, [this.rotation[0] * ( Math.PI * 180), this.rotation[1]* ( Math.PI * 180), this.rotation[2]* ( Math.PI * 180)]);
	console.log(this.local);
	this.local = raptorjs.matrix4.scale(this.local, this.scale);
	
	
	//if(this.entity) {
		//this.world =  raptorjs.matrix4.mul( this.local, this.entity.parent.transform.world );
		this.world = this.local;
	//}
	
}
raptorjs.transform.prototype.scaleXYZ = function(x,y,z) {
	this.scale = [x,y,z];
	this.update();
}

raptorjs.transform.prototype.rotate = function(x,y,z) {
	
}
raptorjs.transform.prototype.rotateTo = function(x,y,z) {
	this.rotation = [x,y,z];
	this.update();
}

raptorjs.transform.prototype.rotateZ = function(x) {
	this.world = raptorjs.matrix4.rotateZ( this.world, raptorjs.math.degToRad( x ) ); 
}

raptorjs.transform.prototype.rotateX = function(x) {
	this.world = raptorjs.matrix4.rotateX( this.world, raptorjs.math.degToRad( x ) ); 
}
raptorjs.transform.prototype.rotateY = function(y) {
	this.world = raptorjs.matrix4.rotateY( this.world, raptorjs.math.degToRad( y ) ); 
}

raptorjs.transform.prototype.getWorldPosition = function(y) {
	return [this.world[3][0], this.world[3][1], this.world[3][2]];
}

raptorjs.transform.prototype.getUpdatedWorldMatrix = function() {
	return this.world;
}

raptorjs.transform.prototype.translateVec = function(v) {
	this.world = raptorjs.matrix4.translate(this.world, v);
}