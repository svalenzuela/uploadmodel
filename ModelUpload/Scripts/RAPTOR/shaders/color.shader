
	precision highp float;

	#ifndef NORMAL_MAP
	#define NORMAL_MAP 0
	#endif
	
	#ifndef PARALLAX
	#define PARALLAX 0
	#endif
	
	attribute vec3 position;
	attribute vec2 textcoord;
	
	attribute vec3 normal;
	attribute vec3 tangent;
	attribute vec3 bitangent;
	
	uniform mat4 world;
	uniform mat4 view;
	uniform mat4 viewProjection;
	uniform float normals;
	

	varying vec2 v_textcoord;
	
    varying vec4 v_normal;
	varying vec4 v_tangent;
	varying vec3 v_binormal;
	varying vec2 v_offset;
	varying vec4 v_worldposition;
	varying vec4 v_position;
	varying float v_depth;
	
	varying vec3 v_view;
	
	mat3 transpose(mat3 matrix) {

		return mat3( matrix[0].x, matrix[1].x, matrix[2].x,
					 matrix[0].y, matrix[1].y, matrix[2].y,
					 matrix[0].z, matrix[1].z, matrix[2].z );
				 
	}
	
	void main(void) {
	
		v_textcoord = textcoord;
		
		v_normal  =  normalize(world * vec4(normal, 0.0));
		
		#if NORMAL_MAP == 1
			v_tangent =   normalize(world * vec4(tangent, 0.0));
			v_binormal =   cross(tangent, normal);
		#endif
		
		
		#if PARALLAX == 1

			const float height_map_range = 0.32;

			vec3 vs_normal = ( view * v_normal ).xyz;
			vec3 vs_tangent = ( view * v_tangent ).xyz;
			vec3 vs_bitangent = ( view * vec4(v_binormal, 0.0) ).xyz;
	//vec3 vs_bitangent = bitangent;
			
			
			vec4 a = view * vec4(position, 1.0);
			
			v_view = a.xyz;

			mat3 tbn = mat3( normalize(vs_tangent), normalize(vs_bitangent), normalize(vs_normal) );
			mat3 vs_to_ts = transpose( tbn );

			vec3 ts_view = vs_to_ts * v_view;

			v_offset = (ts_view.xy / ts_view.z) * height_map_range;
		
		

		
		#endif
		
		
		
		v_worldposition = world * vec4(position, 1.0);
		v_position 		= viewProjection * vec4(position, 1.0);
		v_depth  = v_position.z;
		
		gl_Position = v_position;

	}

	// #raptorEngine - Split
	
	#extension GL_EXT_draw_buffers : require
	#define PI 3.14159265358979323846
	
	precision highp float;
	
	
	float Square(float f){
		return (f * f);
	}
	
	vec3 Square(vec3 f){
		return (f * f);
	}
	
	float rcp(float a) {
		return 1.0 / a;
	}
	
	vec3 Diffuse_Lambert( vec3 DiffuseColor )
	{
		return DiffuseColor * (1.0 / PI);
	}

	float D_GGX( float Roughness, float NoH )
	{
		float a = Roughness * Roughness;
		float a2 = a * a;
		float d = ( NoH * a2 - NoH ) * NoH + 1.0;	// 2 mad
		return a2 / ( PI*d*d );					// 4 mul, 1 rcp
	}

	vec3 F_Schlick( vec3 SpecularColor, float VoH )
	{

		return SpecularColor + ( clamp( 50.0 * SpecularColor.g, 0.0, 1.0 ) - SpecularColor ) * exp2( (-5.55473 * VoH - 6.98316) * VoH );
	}

	float Luminance( vec3 LinearColor )
	{
		return dot( LinearColor, vec3( 0.3, 0.59, 0.11 ) );
	}

	
	float BiasedNDotL(float NDotLWithoutSaturate )
	{
		return clamp(NDotLWithoutSaturate * 1.08 - 0.08, 0.0, 1.0);
	}	
	
	vec3 Diffuse_Burley( vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH )
	{
		float FD90 = 0.5 + 2.0 * VoH * VoH * Roughness;
		float FdV = 1.0 + (FD90 - 1.0) * exp2( (-5.55473 * NoV - 6.98316) * NoV );
		float FdL = 1.0 + (FD90 - 1.0) * exp2( (-5.55473 * NoL - 6.98316) * NoL );
		return DiffuseColor / PI * FdV * FdL;
	}
	
	vec3 Diffuse( vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH )
	{
		//#if   PHYSICAL_DIFFUSE == 0
		//		return Diffuse_Lambert( DiffuseColor );
		//#elif PHYSICAL_DIFFUSE == 1
				return Diffuse_Burley( DiffuseColor, Roughness, NoV, NoL, VoH );
		//#elif PHYSICAL_DIFFUSE == 2
		//		return Diffuse_OrenNayar( DiffuseColor, Roughness, NoV, NoL, VoH );
		//#endif
	}
	
	vec3 PointLightDiffuse(float _Roughness, vec3 albedo, vec3 VectorToLight, vec3 V, vec3 N )
	{
		vec3 L = VectorToLight;

		vec3 H = normalize(V + L);
		float NoL = clamp( dot(N, L), 0.0, 1.0 );
		float NoV = clamp( dot(N, V), 0.0, 1.0 );
		float VoH = clamp( dot(V, H), 0.0, 1.0 );

		return Diffuse( albedo, _Roughness, NoV, NoL, VoH );
	}
	
	float ClampedPow(float X,float Y)
	{
			return pow( max( abs(X), 0.000001), Y );
	}
	
	float PhongShadingPow(float X, float Y)
	{

		return ClampedPow(X, Y);
	}

	float D_Blinn( float Roughness, float NoH )
	{
		float m = Roughness * Roughness;
		float m2 = m * m;
		float n = 2.0 / m2 - 2.0;
		return (n+2.0) / (2.0*PI) * PhongShadingPow( NoH, n );		// 1 mad, 1 exp, 1 mul, 1 log
	}
	
	float Distribution( float Roughness, float NoH )
	{
	//#if   PHYSICAL_SPEC_D == 0
		return D_Blinn( Roughness, NoH );
	//#elif PHYSICAL_SPEC_D == 1
	//	return D_Beckmann( Roughness, NoH );
	//#elif PHYSICAL_SPEC_D == 2
	//	return D_GGX( Roughness, NoH );
	//#endif
	}

	float Vis_Schlick( float Roughness, float NoV, float NoL )
	{
			float k = Square( Roughness ) * 0.5;
			float Vis_SchlickV = NoV * (1. - k) + k;
			float Vis_SchlickL = NoL * (1. - k) + k;
			return 0.25 / ( Vis_SchlickV * Vis_SchlickL );
	}
	

	float GeometricVisibility( float Roughness, float NoV, float NoL, float VoH, vec3 L, vec3 V )
	{
   // #if   PHYSICAL_SPEC_G == 0
	//        return Vis_Implicit();
	//#elif PHYSICAL_SPEC_G == 1
   //         return Vis_Neumann( NoV, NoL );
   // #elif PHYSICAL_SPEC_G == 2
   //         return Vis_Kelemen( L, V );
	//#elif PHYSICAL_SPEC_G == 3
			return Vis_Schlick( Roughness, NoV, NoL );
   // #elif PHYSICAL_SPEC_G == 4
	//        return Vis_Smith( Roughness, NoV, NoL );
	//#endif
	}
	
	vec3 F_Fresnel( vec3 SpecularColor, float VoH )
	{
			vec3 SpecularColorSqrt = sqrt( clamp( vec3(0.0, 0.0, 0.0), vec3(0.99, 0.99, 0.99), SpecularColor ) );
			vec3 n = ( 1.0 + SpecularColorSqrt ) / ( 1.0 - SpecularColorSqrt );
			vec3 g = sqrt( n*n + VoH*VoH - 1.0 );
			return 0.5 * Square( (g - VoH) / (g + VoH) ) * ( 1.0 + Square( ((g+VoH)*VoH - 1.0) / ((g-VoH)*VoH + 1.0) ) );
	}

	vec3 Fresnel( vec3 SpecularColor, float VoH )
	{
	//#if   PHYSICAL_SPEC_F == 0
	//		return F_None( SpecularColor );
	//#elif PHYSICAL_SPEC_F == 1
			return F_Schlick( SpecularColor, VoH );
	//#elif PHYSICAL_SPEC_F == 2
		//	return F_Fresnel( SpecularColor, VoH );
	//#endif
	}

   vec3 PointLightSpecular(vec3 specular, float _Roughness, vec3 VectorToLight, vec3 V, vec3 N)
	{
			float Roughness = _Roughness;
			float Energy = 1.0;

			float a = Roughness * Roughness;

			float SourceRadius = 0.4;
			const float SourceLength = 0.2;
			
			vec3 R = reflect( -V, N );
			float RLengthL = inversesqrt( dot( VectorToLight, VectorToLight ) );
		   
			if( SourceLength > 0.0 )
			{
					float LineAngle = clamp( SourceLength * RLengthL, 0.0, 1.0 );
					Energy *= a / clamp( a + 0.5 * LineAngle, 0.0, 1.0 );

					// Closest point on line segment to ray
					vec3 Ld = VectorToLight * SourceLength;
					vec3 L0 = VectorToLight - 0.5 * Ld;
					vec3 L1 = VectorToLight + 0.5 * Ld;

					// Shortest distance
					float a = Square( SourceLength );
					float b = dot( R, Ld );
					float t = clamp( dot( L0, b*R - Ld ) / (a - b*b), 0.0, 1.0 );

					VectorToLight = L0 + t * Ld;
			}
		   
			if( SourceRadius > 0.0 )
			{
					float SphereAngle = clamp( SourceRadius * RLengthL, 0.0, 1.0 );
					Energy *= Square( a / clamp( a + 0.5 * SphereAngle, 0.0, 1.0 ) );
				   
					// Closest point on sphere to ray
					vec3 ClosestPointOnRay = dot( VectorToLight, R ) * R;
					vec3 CenterToRay = ClosestPointOnRay - VectorToLight;
					vec3 ClosestPointOnSphere = VectorToLight + CenterToRay * clamp( SourceRadius * inversesqrt( dot( CenterToRay, CenterToRay ) ), 0.0, 1.0 );
					VectorToLight = ClosestPointOnSphere;
			}
								   
			// TODO: код Spot
		   
			//vec3 L =  VectorToLight;
			vec3 L = normalize( VectorToLight );

			vec3 H = normalize(V + L);
			float NoL = clamp( dot(N, L), 0.0, 1.0 );
			float NoV = clamp( dot(N, V), 0.0, 1.0 );
			float NoH = clamp( dot(N, H), 0.0, 1.0 );
			float VoH = clamp( dot(V, H), 0.0, 1.0 );                     
		   
			// Generalized microfacet specular
			float D = Distribution( Roughness, NoH );
			float Vis = GeometricVisibility( Roughness, NoV, NoL, VoH, L, V );
			vec3 F = Fresnel( specular, VoH );
			return (Energy * D * Vis) * F;
	}

	
	vec3 PointLightSubsurface(float alpha, vec3 L, vec3 V, vec3 N, float SubsurfaceExtinction )
	{
			vec3 _SubColor = vec3(1.0);
		
			vec3 H = normalize(V + L);
	
			float InScatter = pow(clamp(dot(L, -V), 0.0, 1.0), 12.0) * mix(3.0, 0.1, alpha);
		   
			float OpacityFactor = alpha;

			float NormalContribution = clamp(dot(N, H) * OpacityFactor + 1.0 - OpacityFactor, 0.0, 1.0);
			
			//float BackScatter = InGBufferData.GBufferAO * NormalContribution / (PI * 2.0);
			float BackScatter = NormalContribution / (PI * 2.0);
		   
			return  _SubColor * (mix(BackScatter, 1.0, InScatter) * SubsurfaceExtinction);
	}
	
	float LuminanceUE( vec3 LinearColor )
	{
		return dot( LinearColor, vec3( 0.3, 0.59, 0.11 ) );
	}
	
	
	vec4 GetDynamicLighting(vec3 specular, float _Roughness, vec3 albedo, float alpha, vec3 lightDir, vec3 viewDir, float atten,  vec3 normal, float ambientOcclusion)
	{
		vec3 N = normal;                   
		vec3 L = normalize( lightDir ); 
		float NoL = BiasedNDotL( dot(N, L) );
		float DistanceAttenuation = atten;
		float LightRadiusMask = 1.0;
		float SpotFalloff = 1.;

		vec4 OutLighting = vec4(0.0);
			   
		if (LightRadiusMask > 0.0 && SpotFalloff > 0.0)
		{
				float OpaqueShadowTerm = 1.0;
				float SSSShadowTerm = 1.0;
			   
				float NonShadowedAttenuation = DistanceAttenuation * LightRadiusMask * SpotFalloff;
				//float ShadowedAttenuation = NonShadowedAttenuation * OpaqueShadowTerm;
				
				//float term = dot(L, N) * 100000.0;
				
				float ShadowedAttenuation = NonShadowedAttenuation * OpaqueShadowTerm ;
				
				ShadowedAttenuation += ambientOcclusion;
											   
				if (ShadowedAttenuation > 0.0)
				{
						vec3 LightColor = albedo;

						vec3 DiffuseLighting = PointLightDiffuse( _Roughness, albedo, L, viewDir, N );
						vec3 SpecularLighting = PointLightSpecular( specular,  _Roughness, L, viewDir, N );
						vec3 SubsurfaceLighting = PointLightSubsurface( alpha, L, viewDir, N, SSSShadowTerm );   
					   
						vec4 LightColor4 = vec4(LightColor, LuminanceUE(LightColor));
						vec4 DiffuseLighting4 = vec4(DiffuseLighting, 0.0);
						vec4 SpecularLighting4 = vec4(SpecularLighting, LuminanceUE(SpecularLighting));
						vec4 SubsurfaceLighting4 = vec4(SubsurfaceLighting, 0.0);
					   
						OutLighting += LightColor4 * (  (NoL * ShadowedAttenuation) *
																(DiffuseLighting4 + SpecularLighting4) +
																SubsurfaceLighting4 * NonShadowedAttenuation );
						//OutLighting = SpecularLighting4;(DiffuseLighting4 + SpecularLighting4)
				
				}
		}

		return OutLighting;
	}
	
	
	
	#ifndef NORMAL_MAP
	#define NORMAL_MAP 0
	#endif
	
	#ifndef TEXTURE
	#define TEXTURE 0
	#endif	
	
	#ifndef ROUGHNESS_MAP
	#define ROUGHNESS_MAP 0
	#endif	
	
	#ifndef PARALLAX
	#define PARALLAX 0
	#endif
	
	precision highp float;
	
	uniform sampler2D diffuseSampler;
	uniform sampler2D normalSampler;
	uniform sampler2D roughnessSampler;
	uniform sampler2D heightSampler;
	
	
	uniform float normals;
	uniform float far;
	uniform float roughness;
	uniform float metallic;
	uniform float alpha;
	
	uniform vec3 diffuseColor;
	
	uniform vec3 cameraPosition;
	
	uniform float attenuation; 
	
	uniform float SourceRadius;
	uniform float SourceLength;
	uniform float specular;

	
	
	varying vec2 v_textcoord;
	varying vec4 v_normal;
	varying vec4 v_tangent;
	varying vec3 v_binormal;
	
	varying vec4 v_worldposition;
	varying vec4 v_position;
	varying float v_depth;
	
	varying vec2 v_offset;
	varying vec3 v_view;
	
	void main() {
		vec2 textureCoordinate = v_textcoord;
		vec3 normal;
		
			#if NORMAL_MAP == 1
			 
				vec4 normalMap = texture2D(normalSampler, textureCoordinate) * 2.0 - 1.0;
				normal = normalize((v_tangent.xyz * normalMap.x) + (v_binormal.xyz * normalMap.y) + (v_normal.xyz * normalMap.z));

			#else

				normal = v_normal.xyz;

			#endif
		
		
		
		#if PARALLAX == 1

			const float max_samples = 30.0;
			const float min_samples = 8.0;

			vec3 view = normalize(v_view);


			float num_steps = mix(max_samples, min_samples, dot(view, normal));

			float current_height = 0.0;
			float step_size = 1.0 / float(num_steps);
			float prev_height = 1.0;

			float step_index = 0.0;

			
			vec2 tex_offset_per_step = step_size * v_offset;
			vec2 tex_current_offset = v_textcoord;
			float current_bound = 1.0;
			float parallax_amount = 0.0;

			vec2 pt1 = vec2(0.0);
			vec2 pt2 = vec2(0.0);

			vec2 tex_offset = vec2(0.0);

			
			for(int x = 0; x < 30; x++) {
				if(step_index < num_steps)
				{
					tex_current_offset -= tex_offset_per_step;

					current_height = texture2D(heightSampler, tex_current_offset).r; //, dx, dy

					current_bound -= step_size;

					if(current_height > current_bound)
					{
						pt1 = vec2(current_bound, current_height);
						pt2 = vec2(current_bound + step_size, prev_height);

						tex_offset = tex_current_offset - tex_offset_per_step;

						step_index = num_steps + 1.0;
					}
					else
					{
						step_index++;
						prev_height = current_height;
					}
				}
			}

			float delta1 = pt1.x - pt1.y;
			float delta2 = pt2.x - pt2.y;
			float denominator = delta2 - delta1;

			if(denominator == 0.0)
			{
				parallax_amount = 0.0;
			}
			else
			{
				parallax_amount = (pt1.x * delta2 - pt2.x * delta1) / denominator;
			}
			
			vec2 parallax_offset = v_offset * (1.0 - parallax_amount);
			textureCoordinate.xy -= parallax_offset;

		
		#endif
	
	
		vec3 baseColor;
		vec4 diffuseMap;
		
		#if TEXTURE == 1
			
			diffuseMap = ( texture2D(diffuseSampler, textureCoordinate));
			baseColor = diffuseMap.rgb;
			
		#else
		
			baseColor = diffuseColor;
			
		#endif	
		
		
		float f_roughness = roughness;
	
		
		//#if ROUGHNESS_MAP == 1
		// 
		//	f_roughness = length(texture2D(roughnessSampler, textureCoordinate)) / 3.0;

		//#endif
		
		vec3 diffuse = baseColor ;
		//vec3 specularColor = lerp( 0.08 * specular.xxx, baseColor, metallic );
		
		
		#if DEFERRED == 1
	

			gl_FragData[0] = vec4(diffuse, f_roughness);
			gl_FragData[1] = vec4(normal, v_depth / far);
			gl_FragData[2] = vec4(v_worldposition.xyz, specular);//float(SHADING_MODEL_ID ) / 256.
			
		#else
	
	//#if NONMETAL
	//	half3 DiffuseColor = BaseColor;
	//	half SpecularColor = 0.04;
	//#else
	//	half DielectricSpecular = 0.08 * Specular;
	//	half3 DiffuseColor = BaseColor - BaseColor * Metallic;	// 1 mad
	//	half3 SpecularColor = (DielectricSpecular - DielectricSpecular * Metallic) + BaseColor * Metallic;	// 2 mad
	//#endif
	
	
			vec3 lightPosition = vec3(10.0, 100.0, 10.0);
			vec3 ToLight = (lightPosition - v_worldposition.xyz);
			
			vec3 viewDir = normalize(cameraPosition - v_worldposition.xyz);
			
			vec3 pbr = GetDynamicLighting(vec3(.5), f_roughness, diffuse, alpha, ToLight, viewDir, 2.,  normal, 1.0).xyz;
			//vec3 specular, float _Roughness, vec3 albedo, float alpha, vec3 lightDir, vec3 viewDir, float atten,  vec3 normal, float ambientOcclusion)
			//gl_FragColor = vec4(pbr, 1.0);
			vec3 gamma = vec3(1.0/2.2 );
			vec3 finalColor = pow(pbr.xyz, gamma);
			
			gl_FragColor = vec4(pbr.xyz, 1.0);
			//gl_FragColor = vec4(diffuse.xyz, 1.0);
			
		#endif
		//gl_FragColor = vec4(normal, 1.0);//texture2D(texture, v_textcoord);
	}

