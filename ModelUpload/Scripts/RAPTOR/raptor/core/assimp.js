/**
 * Player object
**/
raptorjs.assimp = function ()
{
	this.modelPath = 'media/models/';
	
	this.meshes = [];
	this.scene  = raptorjs.createObject("sceneNode");
	this.shader = raptorjs.createObject("shader");
	
	this.models = [];

	this.animations;
	
	this.skeleton;
	
	this.lightNames = ["PhotometricLight", "FPoint"];
	
	
	
	this.shadingModelIDS = [];
	
	this.bindMaterial("$ColladaAutoName$_108", 10.0);
	this.bindMaterial("Box002", 1.0);
}


raptorjs.assimp.prototype.stringStartsWith = function( string, compare ) {
	if (string.substring(0, compare.length) == compare) 
		return true;
	else
		return false;
}

raptorjs.assimp.prototype.nameIsLight = function( name ) {
	for(var c = 0; c<this.lightNames.length; c++) {
		if(this.stringStartsWith(name, this.lightNames[c])) {
			
			return true;
				
		}
	}
	
	return false;
}


raptorjs.assimp.prototype.addModel = function( json ) {
	this.models.push(json);
}

raptorjs.assimp.prototype.bindMaterial = function( entityName, shadingModelID ) {
	this.shadingModelIDS.push({name:entityName, id:shadingModelID});
}

raptorjs.assimp.prototype.getShadingIdByEntityName = function( entityName ) {
	for(var c = 0; c<this.shadingModelIDS.length; c++) {
		console.log(this.shadingModelIDS[c].name, entityName);
		if(this.shadingModelIDS[c].name == entityName)
			return this.shadingModelIDS[c].id;
		
	}
	
	return 0;
}

raptorjs.assimp.prototype.serializeUrl = function( url, part_two ) {
	if(url) {
		// Remove backslash
		var urlParts = url.split("\\");
		
		url = urlParts[urlParts.length-1];
		
		
		
		// Force extension to png
		urlParts = url.split(".");
		
		urlParts[urlParts.length-1] = "png";
		
		url = urlParts.join(".");
		
		// type
		urlParts = url.split("_");
		if(urlParts.length > 1) {
			urlParts[urlParts.length-1] = part_two + ".png";
			
		} else {
			
			
		}
		url = urlParts.join("_");
		
		
		//remove slash
		urlParts = url.split("/");
		
		return urlParts[urlParts.length-1];
	}
	 else {
		 
		 return false;
	 }
}
/**
 * update object
**/
raptorjs.assimp.prototype.loadData = function( objects ) {
	
	//this.shader.createFomFile("shaders/color.shader");
	//this.shader.update();

	console.log(objects);
	
	objects.parsedMeshes = [];

	var materials 	= objects.materials;
	var meshes 		= objects.meshes;
	var rootnode 	= objects.rootnode;
	var children	= rootnode.children;
	this.animations = objects.animations;
	var materialCache = [];
	
	
	// get materials
	for(var c = 0; c < materials.length; c++) {
		
		var material = materials[c];
		var properties = material.properties;
		
		var name = this.getPropertyByName(properties, '?mat.name', 0).value;
		var diffuseAdress = this.serializeUrl( this.getPropertyByName(properties, '$tex.file', 1).value, "diff");
		var normalAdress = this.serializeUrl( this.getPropertyByName(properties, '$tex.file', 1).value, "ddn");
		var roughAdress = this.serializeUrl( this.getPropertyByName(properties, '$tex.file', 1).value, "rough");
		var depthAdress = this.serializeUrl( this.getPropertyByName(properties, '$tex.file', 1).value, "depth");
		
		
		var diffuseColor = this.getPropertyByName(properties, '$clr.diffuse', 0).value;
		
		//var normalAdress = this.serializeUrl( this.getPropertyByName(properties, '$tex.file', 5).value);
		//semantic 1 = diffuse
		//semantic 3 = diffuse
		//semantic 5 = normal
		
		
		raptorjs.resources.addTexture("media/textures/sponza/" + diffuseAdress, name + "_diffuse");
		raptorjs.resources.addTexture("media/textures/sponza/" + normalAdress, name + "_normal");
		raptorjs.resources.addTexture("media/textures/sponza/" + roughAdress, name + "_rough");
		raptorjs.resources.addTexture("media/textures/sponza/" + depthAdress, name + "_depth");
		

		//var material = {name: name};

		//material.addProperty("diffuse", diffuseAdress);
		//material.addProperty("normal", normalAdress);

		materialCache[c] = {};
		materialCache[c].name = name;
		
		if(diffuseColor) {
			materialCache[c].diffuseColor = [diffuseColor[0], diffuseColor[1], diffuseColor[2]];
		}
		
	}
	
	// get meshes
	for(var c = 0; c < meshes.length; c++) {
		
		var meshInfo = meshes[c];
		
		//flatten
		var indices = [];

		for(var i = 0; i<meshInfo.faces.length; i++) {
			var face = meshInfo.faces[i];
			
			indices[i * 3] = face[0];
			indices[i * 3 + 1] = face[1];
			indices[i * 3 + 2] = face[2];

		}

		var mesh = raptorjs.createObject("mesh");
	
		if(meshInfo.texturecoords){
			console.log(meshInfo, "Meshinfo");
			
			mesh.createMeshFromArrays( indices, meshInfo.vertices, meshInfo.normals, meshInfo.texturecoords[0], meshInfo.tangents, meshInfo.bitangents );
			mesh.material = materialCache[meshInfo.materialindex];
			
		}
		
		objects.parsedMeshes.push(mesh);
	}

	this.addModel(objects);
}

raptorjs.assimp.prototype.load = function ( jsonFilename ) {
	var json = raptorjs.resources.loadTextFileSynchronous( this.modelPath + jsonFilename );
	var objects 	= JSON.parse(json);
	this.loadData(objects);
}


raptorjs.assimp.prototype.parseNode = function( currentAssimpJson, assimpNode, parentEntity ) {
	
	var entity = raptorjs.createObject("entity");
	entity.name = assimpNode.name;
	entity.transform.world = raptorjs.matrix4.fromArray(assimpNode.transformation);
	entity.transform.local = raptorjs.matrix4.fromArray(assimpNode.transformation);
	
	if(this.nameIsLight(entity.name)) {
		entity.type = "PointLight";

		raptorjs.system.scene.addEntity( entity );
	} else {
		
		if(assimpNode.meshes) {
			entity.type = "Actor";
		} else {
			entity.type = "transform";
			raptorjs.system.scene.addEntity( entity );
		}

	}
	
	if(assimpNode.meshes) {
		
		
		var meshID = assimpNode.meshes[0];
		var mesh = currentAssimpJson.parsedMeshes[meshID];

		var transformation = assimpNode.transformation;
		var materialInfo = mesh.material;//raptorjs.createObject("material");
		
		var material = raptorjs.createObject("material");
		
		if(mesh.material) {

			var diffuseTexture = raptorjs.resources.getTexture(materialInfo.name + "_diffuse");
			
			if(diffuseTexture) {
				var diffuseSampler = raptorjs.createObject("sampler2D");
				
				diffuseSampler.addTexture(diffuseTexture);
				
				material.addTexture(diffuseSampler);
				
				console.log(materialInfo.name + "_diffuse", material);
			} else{
				console.log("noo _diffuse", material);
				
			}
			
			var normalTexture = raptorjs.resources.getTexture(materialInfo.name + "_normal");
			
			if(normalTexture) {
				var normalSampler = raptorjs.createObject("sampler2D");
				normalSampler.addTexture(normalTexture);
				material.addNormal(normalSampler);
			}
			
			var roughnessTexture = raptorjs.resources.getTexture(materialInfo.name + "_normal");
			
			if(roughnessTexture) {
				var roughnessSampler = raptorjs.createObject("sampler2D");
				roughnessSampler.addTexture(roughnessTexture);
				material.addRoughness(roughnessSampler);
			}
			
			var depthTexture = raptorjs.resources.getTexture(materialInfo.name + "_depth");
			
			if(depthTexture) {
				var roughnessSampler = raptorjs.createObject("sampler2D");
				roughnessSampler.addTexture(depthTexture);
				material.addDisplacement(roughnessSampler);
			}
			
			

			material.diffuseColor = materialInfo.diffuseColor;
			
			var shadingModelID = this.getShadingIdByEntityName(assimpNode.name);

			material.setShadingModelID(shadingModelID);
			
			mesh.addMaterial(material);

			entity.addMesh(mesh);
			
			//entity.transform.world = raptorjs.matrix4.mul( raptorjs.matrix4.fromArray(transformation), raptorjs.matrix4.fromArray( rootnode.transformation ) );
			
			
			entity.transform.local = raptorjs.matrix4.fromArray(transformation);
			entity.transform.position = raptorjs.matrix4.getWorldPosition(entity.transform.local);
			entity.transform.scale = raptorjs.matrix3.getScale( entity.transform.local );
			entity.transform.rotation =  raptorjs.matrix3.getRotation( entity.transform.local );
			entity.transform.world = entity.transform.local;//raptorjs.matrix4.mul( parentEntity.transform.world, entity.transform.local );
			//entity.objectTransform = raptorjs.matrix4.mul( raptorjs.matrix4.fromArray(transformation), raptorjs.matrix4.fromArray( rootnode.transformation ) );

			var normMatrix = raptorjs.matrix3.normalizeMatrix( entity.transform.local  ) ;

			entity.mesh.boundingBox = raptorjs.createObject("boundingBox");
			entity.mesh.boundingBox.fitBoxToPoints_( mesh.subMeshes[0].vertices, normMatrix );

			raptorjs.system.scene.addEntity( entity, entity.transform.local );

		}
		
		
		
		
		
	}
	
	parentEntity.addChild(entity);
	
	//if(child.name == "Bip001") {
		
		//this.parseSkeleton( child );

	//}
	
	if(assimpNode.children) {
		var children = assimpNode.children;
		
		for(var i = 0; i < children.length; i++) {
			var child = children[i];
			
			this.parseNode(currentAssimpJson, child, entity);
		}
	}

	console.log(raptorjs.system.scene);
}



raptorjs.assimp.prototype.buildModels = function( jsonFilename ) {
	
	var models = this.models;
	
	for(var c = 0; c<models.length; c++) {
		var currentAssimpJson = models[c];
		var rootnode = currentAssimpJson.rootnode;
		var children = rootnode.children;
	
		var rootEntity = raptorjs.createObject("entity");
		rootEntity.name = "root";
		rootEntity.transform.world = raptorjs.matrix4.identity();// raptorjs.matrix4.fromArray( rootnode.transformation );
		
		raptorjs.system.scene.rootEntity = rootEntity;
		
		this.parseNode(currentAssimpJson, rootnode, rootEntity);
	}
}

raptorjs.assimp.prototype.parseSkeleton = function( child ) {

	this.skeleton = raptorjs.createObject("skeleton");
	
	this.skeleton.parse(child, this.rootnode);
	this.skeleton.parseAnimation(this.animations[0].channels);
	
	
	
	var bones = this.skeleton.bones;
	
	
	for(var c = 0; c<bones.length; c++) {
		
		var bone = bones[c];
		
		this.skeleton.animate(bone, 0);
		
		
		
		var transform = bone.globalTransformation;
		
		var mesh = this.meshes[1];
		
			var entity = raptorjs.createObject("entity");
			
			
			//var diffuseTexture = raptorjs.resources.getTexture(mesh.material.name + "_diffuse");
			//var diffuseSampler = raptorjs.createObject("sampler2D");
			//diffuseSampler.texture = diffuseTexture;

			var material = mesh.material;
			//material.addTexture(diffuseSampler);

			
			mesh.addMaterial(material);

			entity.addMesh(mesh);
			entity.bone = bone;
			entity.transform.world = transform;

			
			bone.entity = entity;
			
			//raptorjs.system.scene.addEntity(entity);
		
	}
	
	//this.animate();
	
	
	//console.log(this.skeleton);
}


raptorjs.assimp.prototype.animate = function(  ) {
	var entitys = this.scene.getEntitys();

	for(var c = 0; c<entitys.length; c++) {
		
		var bone = entitys[c].bone;
		if(bone) {
			
			var bone = this.skeleton.animate(bone, floor(asdasd/2));

			entitys[c].transform.world = bone.globalTransformation;
		}
		
	}
	
}




raptorjs.assimp.prototype.getPropertyByName = function( properties, name, semantic ) {

	for(var c = 0; c<properties.length; c++) {
		var property = properties[c];
		
		if(property.key == name && property.semantic == semantic)
			return property;
	}
	
	return false;
}