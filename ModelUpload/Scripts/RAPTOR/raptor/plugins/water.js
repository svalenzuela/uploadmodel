raptorjs.ocean = function(){
	this.shader;
	this.sphereMesh;
	this.entity;
	this.create();
}

raptorjs.ocean.prototype.create = function() {

	
	
	this.shader = raptorjs.createObject("shader");
	this.shader.createFomFile("shaders/water.shader");
	

	
	//var sphereMesh = raptorjs.primitives.createSphere(15, 64, 64);
	var sphereMesh = raptorjs.primitives.createSphere(57, 16, 16);
	
	
	var mesh = raptorjs.createObject('mesh');
	mesh.name = 'skySphere';
	mesh.addSubMesh(sphereMesh);
	
	this.entity = raptorjs.createObject("entity");
	this.entity.addMesh(mesh);
	//entity.transform.scale(50, 50, 50);	
	this.entity.transform.translate(0, 0, 0);
	console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
console.log(this.entity.mesh);
console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	this.entity.shader = this.shader;

	//raptorjs.scene.addEntity( this.entity );
}

raptorjs.ocean.prototype.update = function(){
	this.entity.shader.setUniform('g_LightPosition', raptorjs.sunLight.position);
	this.entity.shader.setUniform('g_CameraPosition', raptorjs.mainCamera.eye);
	
}

raptorjs.ocean.prototype.render = function() {
	
	var shader = this.shader;
	var camera = raptorjs.mainCamera;
	var shaderProgram = shader.program;

	gl.useProgram(shader.program);
	shader.setUniform("worldViewProjection", camera.worldViewProjection );

	var textureSampler = shader.getUniformByName('textureSampler');
	var primitive = this.entity.mesh;
	
	var buffer = primitive.vertexPositionBuffer;
	var attribute = shader.getAttributeByName('position');
	gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
	gl.vertexAttribPointer(attribute, buffer.itemSize, gl.FLOAT, false, 0, 0);

	var buffer = primitive.textureCoordBuffer;
	var attribute = shader.getAttributeByName('uv');
	gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
	gl.vertexAttribPointer(attribute, buffer.itemSize, gl.FLOAT, false, 0, 0);

	//gl.activeTexture(gl.TEXTURE0);
	//gl.bindTexture(gl.TEXTURE_2D, fftOceanTexture);
	//gl.uniform1i(textureSampler, 0);

	var buffer = primitive.vertexIndexBuffer;
	gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, buffer );
	gl.drawElements( gl.LINE_STRIP, buffer.numItems, gl.UNSIGNED_SHORT, 0 ); 
}