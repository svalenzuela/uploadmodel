



raptorjs.framebuffer = function() {
	this.width = 1024;
	this.height = 1024;
	
	this.target = gl.TEXTURE_2D;
	
	this._className = "_framebuffer";
	
	this.glFramebuffer;
	this.face = gl.TEXTURE_CUBE_MAP_POSITIVE_X;

	this.samplers = []; // render to samplers
	
	this.attachment = gl.COLOR_ATTACHMENT0;
	//GL_DEPTH_ATTACHMENT
}

raptorjs.framebuffer.prototype.addSampler = function( sampler ) { 
	this.samplers.push( sampler );
}

raptorjs.framebuffer.prototype.getSampler = function( sampler ) { 
	
}

/**
 * Create framebuffer
 * @param {(int)} width
 * @param {(int)} height
 * @param {(object)} properties
**/
raptorjs.framebuffer.prototype.create = function() { //type, depth

	var WEBGL_draw_buffers = raptorjs.extensions.WEBGL_draw_buffers;
	
	var attachments = [];

	
	this.glFramebuffer = gl.createFramebuffer();
	
	gl.bindFramebuffer(gl.FRAMEBUFFER, this.glFramebuffer);
	
	var renderbuffer = gl.createRenderbuffer();
	
	gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
	gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);
	gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this.width, this.height);
	
	for(var c = 0; c<this.samplers.length; c++) {

		var sampler = this.samplers[c];
		
		if(this.target == gl.TEXTURE_2D) {	
			
			sampler.setTarget();
			
			var textures = sampler.getTextures();
		
		} else {
			
			var textures = [ sampler.getTexture( this.face ) ];
			
		}
		
		
		
		
		for(var b = 0; b<textures.length; b++) {
		
			var texture = textures[b];

			var WEBGL_draw_buffers = raptorjs.extensions.WEBGL_draw_buffers;
			
			if(this.samplers.length == 1) {
				var attachment = this.attachment;
			} else {
				switch(c) {
					case 0:
						var attachment = WEBGL_draw_buffers.COLOR_ATTACHMENT0_WEBGL
					break;
					case 1: 
						var attachment = WEBGL_draw_buffers.COLOR_ATTACHMENT1_WEBGL
					break;
					case 2: 
						var attachment = WEBGL_draw_buffers.COLOR_ATTACHMENT2_WEBGL
					break;
					case 3: 
						var attachment = WEBGL_draw_buffers.COLOR_ATTACHMENT3_WEBGL
					break;
				}
			}

			attachments.push(attachment);
			
			gl.bindTexture(sampler.target, texture.glTexture);
			
			switch(sampler.filter) {
				case gl.NEAREST:
					gl.texParameteri(sampler.target, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
					gl.texParameteri(sampler.target, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
					gl.texParameteri(sampler.target, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
					gl.texParameteri(sampler.target, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
				break;
				case gl.LINEAR:
					gl.texParameteri(sampler.target, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
					gl.texParameteri(sampler.target, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
					gl.texParameteri(sampler.target, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
					gl.texParameteri(sampler.target, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
				break;
				default:
					gl.texParameteri(sampler.target, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
					gl.texParameteri(sampler.target, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
					gl.texParameteri(sampler.target, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
					gl.texParameteri(sampler.target, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
			}


			gl.texImage2D(texture.face, 0, sampler.format, this.width, this.height, 0, sampler.format, sampler.type, null);
			gl.framebufferTexture2D(gl.FRAMEBUFFER, attachment, texture.face, texture.glTexture, 0);
	
					/*
				var framebufferFormat = raptorjs.extensions.WEBGL_color_buffer_float.RGBA32F_EXT;
				var renderbufferAttachment = attachment;
	
				switch( sampler.type ) {
					case gl.FLOAT:
						
						switch( sampler.format ) {
							
							case gl.RGBA:
							
								var framebufferFormat = raptorjs.extensions.WEBGL_color_buffer_float.RGBA32F_EXT;
								
							break;
							
							case gl.RGB:
							
								var framebufferFormat = raptorjs.extensions.WEBGL_color_buffer_float.RGB32F_EXT;
								
							break;
						}
						
					break;
					
					case gl.UNSIGNED_BYTE:
					
						var framebufferFormat = gl.DEPTH_COMPONENT16;
						
						var renderbufferAttachment = gl.DEPTH_ATTACHMENT;
					break;
				}
				

				*/
				if(sampler.format != gl.RGBA) {
					console.log("kajkaj");
					//var framebufferFormat = gl.RGBA4;
					//var framebufferFormat = raptorjs.extensions.WEBGL_color_buffer_float.RGBA32F_EXT;
					//var renderbufferAttachment = gl.DEPTH_ATTACHMENT;
				} else {
					//console.log("pietpiet", sampler.format);
					//var framebufferFormat = gl.DEPTH_COMPONENT16;
					
				}
				
				
				//gl.renderbufferStorage(gl.RENDERBUFFER, framebufferFormat, this.width, this.height);
				//gl.framebufferRenderbuffer(gl.FRAMEBUFFER, WEBGL_draw_buffers.COLOR_ATTACHMENT0_WEBGL, gl.RENDERBUFFER, renderbuffer);
				//if(attachment == WEBGL_draw_buffers.COLOR_ATTACHMENT0_WEBGL)
				//gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);

			//	gl.framebufferRenderbuffer(gl.FRAMEBUFFER, renderbufferAttachment, gl.RENDERBUFFER, renderbuffer);

			if (gl.checkFramebufferStatus(gl.FRAMEBUFFER) != gl.FRAMEBUFFER_COMPLETE) {
			   alert("this combination of attachments does not work");
			}
							
				
			
			console.log("face", texture.face);

			texture.attachment = attachment;
			texture.dataType = 'framebuffer';
			texture.width = this.width;
			texture.height = this.height;
			texture.data = texture.glTexture;
			
			
			//gl.bindRenderbuffer(gl.RENDERBUFFER, null);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
	}
	
	if(this.samplers.length > 1)
		raptorjs.extensions.WEBGL_draw_buffers.drawBuffersWEBGL(attachments);
	
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	
}