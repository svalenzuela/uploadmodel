/**
 * Raptor Engine - Core
 * Copyright (c) 2010 RAPTORCODE STUDIOS
 * All rights reserved.
 */
  
/**
 * Author: Kaj Dijksta
 */
 
    attribute vec3 position;
    attribute vec2 uv;
	
    uniform mat4 viewProjection;

    varying vec2 v_textureCoord;

    void main(void) {
        v_textureCoord = uv;
		gl_Position = viewProjection * vec4(position, 1.0);
    }
	
	// #raptorEngine - Split
	

    #extension GL_OES_standard_derivatives : enable
	
	precision highp float;

	uniform sampler2D normalDepthSampler;
	uniform sampler2D randomSampler;
	uniform sampler2D positionSampler;
	uniform sampler2D RandomNormalTextureSampler;
	
	uniform mat4 invEyePj; 
	uniform mat4 viewMatrix; 
	uniform mat4 invProjection; 
	
	uniform float screenWidth;
	uniform float screenHeight;
	uniform float far;
	
	varying vec2 v_textureCoord;
	
	uniform vec3 scale[ 32 ];
	uniform vec4 kernelRad[ 8 ];

	uniform vec4 parameters;
	uniform mat3 view;
	uniform mat4 viewProjectionInverseMatrix;

	
	uniform vec3 cameraPositionWorldSpace;
	
// Depth-Only based SSAO
	vec3 Ambiant_Depth_Occlusion( vec2 uv ) {

		//mediump vec4 parameters = vec4( .4, 0.015, 1.2, 1.4 );
		
		const mediump float step = 1.0 - 1.0 / 8.0;
		const mediump float fScale = 0.025; // 0.025
		
		mediump float n = 0.0;
		
		mediump vec3 directions[8];

		directions[0] = normalize(vec3( 1.0, 1.0, 1.0))*fScale*(n+=step);
		directions[1] = normalize(vec3(-1.0,-1.0,-1.0))*fScale*(n+=step);
		directions[2] = normalize(vec3(-1.0,-1.0, 1.0))*fScale*(n+=step);
		directions[3] = normalize(vec3(-1.0, 1.0,-1.0))*fScale*(n+=step);
		directions[4] = normalize(vec3(-1.0, 1.0 ,1.0))*fScale*(n+=step);
		directions[5] = normalize(vec3( 1.0,-1.0,-1.0))*fScale*(n+=step);
		directions[6] = normalize(vec3( 1.0,-1.0, 1.0))*fScale*(n+=step);
		directions[7] = normalize(vec3( 1.0, 1.0,-1.0))*fScale*(n+=step);

		mediump vec3 randomSample = texture2D(randomSampler,  uv * vec2(16.0)).xyz;
		
		randomSample = (2.0 * randomSample - 1.0);
		
		mediump float sceneDepth = texture2D(normalDepthSampler, uv).w;  	  

		mediump float sceneDepthU = sceneDepth * far;  

		mediump vec3 vSampleScale = parameters.zzw * clamp( sceneDepthU / 5.3, 0.0, 1.0 ) * ( 1.0 + sceneDepthU / 8.0 );

		mediump float fDepthRangeScale = far / vSampleScale.z * 0.85;

		vSampleScale.xy *= 1.0 / sceneDepthU;
		vSampleScale.z  *= 2.0 / far;

		float fDepthTestSoftness = 64.0 / vSampleScale.z;

		// sample
		mediump vec4 sceneDeptF[2];    
		mediump vec4 vSkyAccess = vec4(0.0);
		mediump vec3 iSample;
		mediump vec4 vDistance;
		
		highp vec4 fRangeIsInvalid = vec4(0.0);

		const float scaleQuality = 0.5;
		
		vec4 vDistanceScaled;

		// Help Angle a bit
		// for(int i=0; i<1; i++)
		// {    
		
			iSample = reflect(directions[0], randomSample) * vSampleScale;		
			sceneDeptF[0].x = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w  + iSample.z;  

			iSample.xyz *= scaleQuality;
			sceneDeptF[1].x = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  

			iSample = reflect(directions[1], randomSample) * vSampleScale;		
			sceneDeptF[0].y = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  

			iSample.xyz *= scaleQuality;
			sceneDeptF[1].y = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  


			iSample = reflect(directions[2], randomSample) * vSampleScale;		
			sceneDeptF[0].z = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  

			iSample.xyz *= scaleQuality;
			sceneDeptF[1].z = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  


			iSample = reflect(directions[3], randomSample) * vSampleScale;		
			sceneDeptF[0].w = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  

			iSample.xyz *= scaleQuality;
			sceneDeptF[1].w = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  

			const float definitionValue = 0.6;

			// for(int s=0; s<2; s++)
			// {
			
				vDistance = sceneDepth - sceneDeptF[0]; 
				vDistanceScaled = vDistance * fDepthRangeScale;
				fRangeIsInvalid = (clamp( abs(vDistanceScaled), 0.0, 1.0 ) + clamp( vDistanceScaled, 0.0, 1.0 )) / 2.0;  
				vSkyAccess += mix(clamp((-vDistance)*fDepthTestSoftness, 0.0, 1.0), vec4(definitionValue), fRangeIsInvalid);
				
				vDistance = sceneDepth - sceneDeptF[1]; 
				vDistanceScaled = vDistance * fDepthRangeScale;
				fRangeIsInvalid = (clamp( abs(vDistanceScaled), 0.0, 1.0 ) + clamp( vDistanceScaled, 0.0, 1.0 )) / 2.0;  
				vSkyAccess += mix(clamp((-vDistance)*fDepthTestSoftness, 0.0, 1.0), vec4(definitionValue), fRangeIsInvalid);
				
			// }
			
			
			iSample = reflect(directions[4], randomSample) * vSampleScale;		
			sceneDeptF[0].x = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  

			iSample.xyz *= scaleQuality;
			sceneDeptF[1].x = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  

			iSample = reflect(directions[5], randomSample) * vSampleScale;		
			sceneDeptF[0].y = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  

			iSample.xyz *= scaleQuality;
			sceneDeptF[1].y = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  


			iSample = reflect(directions[6], randomSample) * vSampleScale;		
			sceneDeptF[0].z = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  

			iSample.xyz *= scaleQuality;
			sceneDeptF[1].z = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  


			iSample = reflect(directions[7], randomSample) * vSampleScale;		
			sceneDeptF[0].w = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  

			iSample.xyz *= scaleQuality;
			sceneDeptF[1].w = texture2D( normalDepthSampler, uv.xy + iSample.xy ).w + iSample.z;  

			//const float definitionValue = 0.001;
			
			// for(int d=0; d<2; d++)
			// {
				vDistance = sceneDepth - sceneDeptF[0]; 
				vDistanceScaled = vDistance * fDepthRangeScale;
				fRangeIsInvalid = (clamp( abs(vDistanceScaled), 0.0, 1.0 ) + clamp( vDistanceScaled, 0.0, 1.0 )) / 2.0;  
				vSkyAccess += mix(clamp((-vDistance)*fDepthTestSoftness, 0.0, 1.0), vec4(definitionValue), fRangeIsInvalid);
				
				vDistance = sceneDepth - sceneDeptF[1]; 
				vDistanceScaled = vDistance * fDepthRangeScale;
				fRangeIsInvalid = (clamp( abs(vDistanceScaled), 0.0, 1.0 ) + clamp( vDistanceScaled, 0.0, 1.0 )) / 2.0;  
				vSkyAccess += mix(clamp((-vDistance)*fDepthTestSoftness, 0.0, 1.0), vec4(definitionValue), fRangeIsInvalid);
			// }
			
			
		// }

		float ambientOcclusion = dot( vSkyAccess, vec4( ( 1.0 / 16.0 ) * 2.0 ) ) - parameters.y; // 0.075f
		ambientOcclusion = clamp(mix( 0.9, ambientOcclusion, parameters.x ), 0.0, 1.0);
		
		return vec3(ambientOcclusion);
	}

void main() {

	vec3 taps[16];  
	taps[0] = vec3(-0.364452, -0.014985, -0.513535);
	taps[1] = vec3(0.004669, -0.445692, -0.165899);
	taps[2] = vec3(0.607166, -0.571184, 0.377880);
	taps[3] = vec3(-0.607685, -0.352123, -0.663045);
	taps[4] = vec3(-0.235328, -0.142338, 0.925718);
	taps[5] = vec3(-0.023743, -0.297281, -0.392438);
	taps[6] = vec3(0.918790, 0.056215, 0.092624);
	taps[7] = vec3(0.608966, -0.385235, -0.108280);
	taps[8] = vec3(-0.802881, 0.225105, 0.361339);
	taps[9] = vec3(-0.070376, 0.303049, -0.905118);
	taps[10] = vec3(-0.503922, -0.475265, 0.177892);
	taps[11] = vec3(0.035096, -0.367809, -0.475295);
	taps[12] = vec3(-0.316874, -0.374981, -0.345988);
	taps[13] = vec3(-0.567278, -0.297800, -0.271889);
	taps[14] = vec3(-0.123325, 0.197851, 0.626759);
	taps[15] = vec3(0.852626, -0.061007, -0.144475);


	float far = 40.0;
   // vec3 SvPosition = texture2D(positionSampler, v_textureCoord).xyz;
   // vec4 normal =  texture2D(normalDepthSampler, v_textureCoord).xyzw;


	

	
	gl_FragColor = vec4(Ambiant_Depth_Occlusion(v_textureCoord), 1.0);

}
	