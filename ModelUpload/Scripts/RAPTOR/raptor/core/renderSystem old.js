/*
 * Copyright 2012, Raptorcode Studios Inc, 
 * Author, Kaj Dijkstra.
 * All rights reserved.
 *
 */
 

/**
 * Rendersystem
**/
raptorjs.renderSystem = function( ) {
	this.testShader;

	this.uberShader;
	this.ssaoShader;
	this.ssaoShaders = [];
	this.ssgiShader;
	this.infoShader;
	
	this.colorInfoSampler;
	this.colorInfoShader;
	this.colorInfoTransparentShader;
	
	this.bumpInfoShader;
	this.depthShader;
	this.shadowShader;
	this.reflectionShader;
	this.diffuseAcc;
	this.diffuseAccSampler;
	 
	this.infoFrameBuffer;
	this.reflectionFramebuffer;
	this.colorInfoFramebuffer;
	this.ssaoConvolutionX;
	this.ssaoConvolutionY;
	this.convolutionShaderX;
	this.convolutionShaderY;
	this.shadowOcclusionFramebuffer;
	this.shadowOcclusionSampler;
	this.prevDiffuseFramebuffer;
	
	
	this.prevDiffuseShader;
		
	this.globalIllumination;
	this.irradianceVolume;
	this.fxaa;
	this.smaa;
	this.hrd;
	
	this.sceneNodes = [];
	
	this.activeScene;
	this.activeCamera;
	this.ready = false;
	
	
	this.buffers = [];
	this.deferredBuffers = [];
	
	this.quadView = raptorjs.matrix4.lookAt([0, 0, 0], [0, -1, 0], [0, 0, -1]);
	this.quadProjection = raptorjs.matrix4.orthographic(-1, 1, -1, 1, -1, 1);
	this.quadViewProjection = raptorjs.matrix4.mul(this.quadView, this.quadProjection);

	//shadow
	this.splitPoints = [];
	this.mOptimalAdjustFactors;
	this.shadowMaps = [];
	this.normalEntitys = [];
	
	this.shadowFramebuffer;
	this.shadowTexture;
	this.shadowSampler;
	this.numberOfSplitpoints = 3;

	this.lambda = 0.75;
	this.pssmOffset = 1000;

	this.depthEntitys = [];
	this.entityContainer = [];
	this.materialContainers = [];
	this.prevViewProjection;
	
	this.lightPosition = [400, 1800, 650];
	
	var near = .1;
	var far = 3600;
	var view = raptorjs.matrix4.lookAt(this.lightPosition,  [0,-1,0], [0, 1, 0] );
	var projection = raptorjs.matrix4.orthographic(-1060, 1060, -680, 1500, -near, far);
	var viewProjection = raptorjs.matrix4.composition( projection, view );

	this.shadowMapT = {};
	this.shadowMapT.view = view;
	this.shadowMapT.projection = projection;
	this.shadowMapT.viewProjection = viewProjection;
	this.shadowMapT.near = near;
	this.shadowMapT.far = far;
	this.shadowMapT.eye = this.lightPosition;

	this.ssaoSampler;
	this.shadowConvXSampler;
	
	this.shadowConvolutionX;
	this.shadowConvolutionY;
	
	this.superbuffers;
	
	this.msaa;
	this.shadow_kernel;
	this.indexType;
	
	this.ready = false;
	
	this.randomRotationSampler
	this.randomSampler;
	
	this.bake = false;
	this.shadowType = 'PCF'; // 'VARIANCE', PCF	
	this.antiAlias = false; // FXAA, SMAA, MSAA, false
	
	this.deferred = false; // forward = true, deferred = false
	this.useSSAO = false;
	this.ssaoOnly = true;

	this.mtrFramebuffer ;
	this.mtrColorTexture;
	this.mtrNormalTexture;
	
	this.deferredShader;
}

var cornerVertices = [[-1,1,-1],[1,1,-1],[1,-1,-1],[-1,-1,-1],  //near
					  [-1,1,1],[1,1,1],[1,-1,1],[-1,-1,1] ];	
	
	
/**
 * create super buffers for great performance boosts
**/
raptorjs.renderSystem.prototype.createSuperBuffers = function() {

	var scene = raptorjs.system.activeScene;
	var indexCounter = 0;
	
	var normalArray = [];
	var uvArray = [];
	var vertexArray = [];
	var indexArray = [];
	
	for(var c = 0; c<scene.entitys.length; c++) {
		var entity = scene.entitys[0];
		var mesh = entity.mesh;
		
		var subMeshes = mesh.subMeshes[0];

		var indices = subMeshes.indices;
		var normals = subMeshes.normals;
		var textcoords = subMeshes.textcoords;
		var vertices = subMeshes.vertices;
		
		normalArray = normalArray.concat(normals);
		uvArray = uvArray.concat(textcoords);
		vertexArray = vertexArray.concat(vertices);
		
		indexArray = indices;
		
		indexCounter += indices.length;
	}

	var vertexIndexBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vertexIndexBuffer);
	
	if(this.indexType == gl.UNSIGNED_INT)
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(indexArray), gl.STATIC_DRAW);
	else 
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexArray), gl.STATIC_DRAW);
		
	vertexIndexBuffer.itemSize = 1;
	vertexIndexBuffer.numItems = indexArray.length;
	
	var vertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexArray), gl.STATIC_DRAW);
	vertexPositionBuffer.itemSize = 3;
	vertexPositionBuffer.numItems = vertexArray.length / 3;
	
	var textureCoordBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(uvArray), gl.STATIC_DRAW);
	textureCoordBuffer.itemSize = 2;
	textureCoordBuffer.numItems = uvArray.length / 2;
	
	var vertexNormalBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vertexNormalBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normalArray), gl.STATIC_DRAW);
	vertexNormalBuffer.itemSize = 3;
	vertexNormalBuffer.numItems = normalArray.length / 3;
	
	var buffers = {};
	
	buffers.textureCoordBuffer = textureCoordBuffer;
	buffers.vertexNormalBuffer = vertexNormalBuffer;
	buffers.vertexPositionBuffer = vertexPositionBuffer;
	buffers.vertexIndexBuffer = vertexIndexBuffer;

	this.superbuffers = buffers;
}				 


/**
 * get frustum corner coordinates
 * @param {(matrix4)} projection
 * @param {(matrix4)} view
 * @param {(matrix4)} world
**/
raptorjs.renderSystem.prototype.getFrustumCorners = function(projection, view, world) {
	
	var viewClone = raptorjs.matrix4.copyMatrix(view);
	
	viewClone = raptorjs.matrix4.setTranslation(viewClone, [0,0,0]);
	
	if(world) {
		var viewProjection = raptorjs.matrix4.inverse( raptorjs.matrix4.composition(projection, viewClone) );
	} else {
		var viewProjection = raptorjs.matrix4.inverse( projection );
	}
	
	var corners = [];

	for(var c =0; c < cornerVertices.length;c++)
	{
		var vert = cornerVertices[c];
		
		vert.push(0.0);
		vert = raptorjs.matrix4.transformPoint(viewProjection, vert);
		
		corners.push(vert);
	}
	
	return corners;
};


/**
 * test position (create sphere)
 * @param {(vector3)} position
**/
raptorjs.renderSystem.prototype.test = function(pos) {

	var defaultnouvs = raptorjs.resources.getTexture("random");
	var diffuseSampler = raptorjs.createObject("sampler2D");
	diffuseSampler.texture = defaultnouvs;
	
	var material = raptorjs.createObject("material");
	material.addTexture(diffuseSampler);
	
		
	var sphereMesh = raptorjs.primitives.createSphere(60.1, 20, 20);
	var entity = raptorjs.createObject("entity");
	var mesh = raptorjs.createObject('mesh');
	
	mesh.name = 'testSphere';
	mesh.addSubMesh(sphereMesh);
	mesh.addMaterial(material);
	
	entity.transform.translate(pos[0], pos[1], pos[2]);	
	entity.addMesh(mesh);
	
	console.log(mesh);

	raptorjs.scene.addEntity( entity );
}


/**
 * draw quad 
 * @param {(shaderObject)} shader
 * @param {(framebuffer)} framebuffer
 * @param {(boolean)} don't update
**/
raptorjs.renderSystem.prototype.drawQuad = function(shader, framebuffer, noUpdate) {
	var quadVertices = this.quadVertices;
	var quadIndices = this.quadIndices;
	var quadUv = this.quadUv;
	
	gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer );	
	gl.clearColor( 0, 0, 0, 0 );
	
	if(framebuffer == 'null')
		gl.viewport(0, 0, framebuffer.width, framebuffer.height);
		
	gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );
	gl.useProgram( shader.program );

	if(!noUpdate)
		shader.update();

	var attribute = shader.getAttributeByName('position');
	gl.bindBuffer(gl.ARRAY_BUFFER, quadVertices);
	gl.vertexAttribPointer(attribute, quadVertices.itemSize, gl.FLOAT, false, 0, 0);

	var attribute = shader.getAttributeByName('uv');
	gl.bindBuffer(gl.ARRAY_BUFFER, quadUv);
	gl.vertexAttribPointer(attribute, quadUv.itemSize, gl.FLOAT, false, 0, 0);
	
	gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, quadIndices );
	gl.drawElements( gl.TRIANGLES, quadIndices.numItems, gl.UNSIGNED_SHORT, 0 );
}



var parseXml;

if (typeof window.DOMParser != "undefined") {
	parseXml = function(xmlStr) {
		return ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");
	};
} else if (typeof window.ActiveXObject != "undefined" &&
	   new window.ActiveXObject("Microsoft.XMLDOM")) {
	parseXml = function(xmlStr) {
		var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async = "false";
		xmlDoc.loadXML(xmlStr);
		return xmlDoc;
	};
} else {
	throw new Error("No XML parser found");
}

	

function findSemantic(nodes, value) {
	
	for(var c = 0;c<nodes.length; c++)
		if(nodes[c].getAttribute('semantic') == value)
			return nodes[c];
			
}

function get_buffer_data(node) {	
	return node[0].childNodes[0].data.split(' ').map(parseFloat);
}

function addressFromSurface(node){
	if(node) {
		var address = node;
		if(address) {
			address = address.getElementsByTagName("init_from")[0].firstChild.nodeValue;
			address = address.split("_");
			
			
			address.pop();
			
			if(address[address.length-1] == "tga")
				address.pop();
			
			address = address.join("_");
			
			return address;
		}
	}
}

function getTextureName(a){
	console.log(a);
	var parts = a.split("_");
	if(parts[parts.length-1] == "diff")
	parts.pop();
	
	parts = parts.join("_");
	return parts;
}

function stripUrl(a) {
	var b = a.split('\\');
	return b[b.length-1];
}

function stripExt(a) {
	var b = a.split('.');
	return b[0];
}

	
/**
 * draw quad 
 * @param {(shaderObject)} shader
 * @param {(framebuffer)} framebuffer
 * @param {(boolean)} don't update
**/
raptorjs.renderSystem.prototype.loadMeshFromJSON = function( directory, textureMap, meshCount ) {
	
	var FF = !(window.mozInnerScreenX == null);
	var workerCounter = 0;
	
	if (FF) {
	
		for(var c = 0; c<meshCount; c++) {
		
			var meshData = raptorjs.loadTextFileSynchronous('media/models/'+directory+'/mesh'+c+'.json');
			
			var meshData = JSON.parse(meshData);
			
			var arrayBuffers = {};
			
			if(this.indexType == gl.UNSIGNED_INT)
				arrayBuffers.indices = new Int32Array(meshData.indices);
			else
				arrayBuffers.indices = new Int16Array(meshData.indices);
			
			arrayBuffers.vertices = new Float32Array(meshData.vertices);
			arrayBuffers.normals = new Float32Array(meshData.normals);
			arrayBuffers.textureCoords = new Float32Array(meshData.textureCoords);
			arrayBuffers.tangents = new Float32Array(meshData.tangents);
			arrayBuffers.binormals = new Float32Array(meshData.binormals);
			arrayBuffers.material = meshData.material;
			
			var material = arrayBuffers.material;
			
			var maps = [material.displacementMaps,
						material.normals,
						material.displacementMaps,
						material.specularMaps,
						material.textures,
						material.transparencyMaps];
			
			for(var b = 0;b<maps.length;b++) {
				if(maps[b])
					raptorjs.resources.addTexture(maps[b], maps[b]);
			}
		
			raptorjs.setLoadingText("Serialized Model: " + arrayBuffers);
			
			this.entityContainer.push(arrayBuffers); 
			
			workerCounter++;
			
			if(workerCounter == meshCount)
				raptorjs.resources.loadNextTexture();
		}
			
	} else {

		var worker = new Worker('raptor/webWorker.js');
		var c = 0; 
		
		worker.onmessage = function(e) {
		
			c++;
			
			var mesh = e.data;
			
			updateLoadingBar(meshCount, c);
			
			var material = mesh.material;
			
			var maps = [material.displacementMaps,
						material.normals,
						material.displacementMaps,
						material.specularMaps,
						material.textures,
						material.transparencyMaps];
			
			for(var b = 0;b<maps.length;b++) {
				if(maps[b]) {
					raptorjs.resources.addTexture(maps[b], maps[b]);
				}
			}
			
			raptorjs.system.entityContainer.push(mesh); 
			
			workerCounter++;
			
			if(workerCounter == meshCount)
				raptorjs.resources.loadNextTexture();
		}
		
		worker.onerror = function(e) {
			console.log('ERROR: Line ', e.lineno, ' in ', e.filename, ': ', e.message);
		}
		
		worker.postMessage({'directory' : directory});
	}
}
	
	
/**
 * create mesh from json
 * @param {(shaderObject)} shader
 * @param {(framebuffer)} framebuffer
 * @param {(boolean)} don't update
**/
raptorjs.renderSystem.prototype.createMeshFromJSON = function() {
	for(var c = 0; c<this.entityContainer.length; c++) {
		var jsonEntity = this.entityContainer[c];
		var jsonMaterial = jsonEntity.material;
		
		var material = raptorjs.createObject("material");
		var mesh = raptorjs.createObject('mesh');

		mesh.createMeshFromArrays(	jsonEntity.indices, 
									jsonEntity.vertices, 
									jsonEntity.normals, 
									jsonEntity.textureCoords, 
									jsonEntity.tangents, 
									jsonEntity.binormals );
							
		if(jsonMaterial.normals){
			var normalTexture = raptorjs.resources.getTexture(jsonMaterial.normals);
			
			var normalSampler = raptorjs.createObject("sampler2D");
			normalSampler.texture = normalTexture;
			
			material.addNormal(normalSampler);
		}
		
		if(jsonMaterial.displacementMaps){
			var displacementTexture = raptorjs.resources.getTexture(jsonMaterial.displacementMaps);
			
			var displacementSampler = raptorjs.createObject("sampler2D");
			displacementSampler.texture = displacementTexture;
			
			material.addDisplacement(displacementSampler);
		}
		
		if(jsonMaterial.specularMaps){
			var specularTexture = raptorjs.resources.getTexture(jsonMaterial.specularMaps);
			
			var specularSampler = raptorjs.createObject("sampler2D");
			specularSampler.texture = specularTexture;
			
			material.addSpecularMap(specularSampler);
		}
		
		if(jsonMaterial.textures){
			var diffuseTexture = raptorjs.resources.getTexture(jsonMaterial.textures);
			
			var diffuseSampler = raptorjs.createObject("sampler2D");
			diffuseSampler.texture = diffuseTexture;
			
			material.addTexture(diffuseSampler);
		}
		
		if(jsonMaterial.transparencyMaps){
			var transparencyTexture = raptorjs.resources.getTexture(jsonMaterial.transparencyMaps);
			
			var transparencySampler = raptorjs.createObject("sampler2D");
			transparencySampler.texture = transparencyTexture;
					
			material.addTransparentyMap(transparencySampler);
		}		

		mesh.addMaterial(material);

		var entity = raptorjs.createObject("entity");
		
		entity.addMesh(mesh);

		raptorjs.scene.addEntity( entity );
	
		console.log(mesh, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
	}
}

/**
 * load mesh from file
 * @param {(string)} url
 * @param {(texture)} textureMap
**/
raptorjs.renderSystem.prototype.loadMeshFromFile = function( url, textureMap ) {

	var data = raptorjs.loadTextFileSynchronous('media/models/'+url);
	var name = url.split('.')[0];
	
	raptorjs.setLoadingText("loaded Model: " + url);
	
	// get all data
	var xmlDoc = parseXml(data).documentElement;

	var effects = xmlDoc.getElementsByTagName('library_effects')[0];
	var materials = xmlDoc.getElementsByTagName('library_materials')[0];
	var geometries = xmlDoc.getElementsByTagName('library_geometries')[0];
	var lights = xmlDoc.getElementsByTagName('library_lights')[0];
	var images = xmlDoc.getElementsByTagName('library_images')[0];
	var visuals = xmlDoc.getElementsByTagName('library_visuals')[0];

	var library_visual_scenes = xmlDoc.getElementsByTagName('visual_scene')[0].getElementsByTagName("node");
	
	for(var c = 0; c<library_visual_scenes.length;c++) {
		var visualNode = library_visual_scenes[c];
	}

	
	var entity = raptorjs.createObject("entity");
	var entityContainer = [];
	
	// serialize materials
	var materialContainer = [];
	var materialChildNodes = materials.getElementsByTagName("material");
	
	for(var c = 0; c<materialChildNodes.length;c++) {
		var materialNode = materialChildNodes[c];
		
		var material = {};
		
		//get effect of this material
		var effectName = materialNode.getElementsByTagName('instance_effect')[0].getAttribute('url');
		
		if(effectName.charAt(0) == "#")
			effectName = effectName.substr(1)
		
		var effect;
		var effectChilds = effects.getElementsByTagName("effect");

		// loop trough effects and get effect
		for(var b = 0;b<effectChilds.length; b++) {
			var effectNode = effectChilds[b];
			if(effectNode.getAttribute("id") == effectName)
				effect = effectNode;
		}
		
		var imageChilds = images.getElementsByTagName("image");
		
		var profile = effect.getElementsByTagName("profile_COMMON")[0];
		var technique = profile.getElementsByTagName("technique")[0];
		var phong = technique.getElementsByTagName("phong")[0];
		
		if(!phong) phong = technique.getElementsByTagName("blinn")[0];
		
		var extra = profile.getElementsByTagName("extra")[0];
		
		// look for bump
		if(extra) {
			var technique = extra.getElementsByTagName("technique")[0];
			var bump = technique.getElementsByTagName("bump")[0];
			
			if(bump) {
				var bumpTexture = bump.getElementsByTagName("texture")[0];
			}	
		}
		
		if(phong) {
		
			var diffuse = phong.getElementsByTagName("diffuse")[0].getElementsByTagName("texture")[0];
			var ambient = phong.getElementsByTagName("ambient")[0].getElementsByTagName("texture")[0];
			var transparent = phong.getElementsByTagName("transparent");

			if(transparent[0]) {
				transparent = transparent[0].getElementsByTagName("texture")[0];
			} else {
				transparent = false;
			}				
			
			var newParams = profile.getElementsByTagName("newparam");
			
			var ambiantSurface;
			var diffuseSurface;
			var bumpSurface;
			var transparencySurface = false;

			if(diffuse)
			for(var b = 0;b<imageChilds.length; b++) {
				var imageNode = imageChilds[b];
				console.log(imageNode.getAttribute("id"),imageNode, diffuse.getAttribute("texture"));
				
				if(imageNode.getAttribute("id") == diffuse.getAttribute("texture")) {
					
					diffuseSurface = stripExt( stripUrl(imageNode.getElementsByTagName("init_from")[0].firstChild.data) );
					
				}
			}

			for(var g = 0; g<newParams.length;g++) {
				var newParam = newParams[g];
				console.log(newParam);
				if(bumpTexture) {
					if(newParam.getAttribute("sid") == bumpTexture.getAttribute("texture")) {
						var sampler = newParam.getElementsByTagName("sampler2D")[0];	// type
						var source = sampler.getElementsByTagName("source")[0].firstChild.nodeValue;
						
						for(var v = 0; v<newParams.length;v++) {
							var newParam2 = newParams[v];
								if(newParam2.getAttribute("sid") ==  source)
									bumpSurface = newParam2;
						}
					}
				}
				
				
				if(ambient) {
					if(newParam.getAttribute("sid") == ambient.getAttribute("texture")) {
						var sampler = newParam.getElementsByTagName("sampler2D")[0];	// type
						var source = sampler.getElementsByTagName("source")[0].firstChild.nodeValue;
						
						for(var v = 0; v<newParams.length;v++) {
							var newParam2 = newParams[v];
								if(newParam2.getAttribute("sid") ==  source)
									ambiantSurface = newParam2;
						}
					}
				}
			
				if(diffuse) {
					if(newParam.getAttribute("sid") == diffuse.getAttribute("texture")) {
						var sampler = newParam.getElementsByTagName("sampler2D")[0];	// type
						var source = sampler.getElementsByTagName("source")[0].firstChild.nodeValue;
						
						for(var v = 0; v<newParams.length;v++) {
							var newParam2 = newParams[v];
								if(newParam2.getAttribute("sid") ==  source)
									diffuseSurface = newParam2;
						}
					}
					
					
				}
				
				if(transparent) {
					if(newParam.getAttribute("sid") == transparent.getAttribute("texture")) {
						var sampler = newParam.getElementsByTagName("sampler2D")[0];	// type
						var source = sampler.getElementsByTagName("source")[0].firstChild.nodeValue;
						
						for(var v = 0; v<newParams.length;v++) {
							var newParam2 = newParams[v];
								if(newParam2.getAttribute("sid") ==  source)
									transparencySurface = newParam2;
						}
					}
					
					
				}

			}
		}

		var ambientAddress = addressFromSurface(ambiantSurface);
		var bumpAddress = addressFromSurface(bumpSurface);
		var transparentAddress = addressFromSurface(transparencySurface);
		
		console.log("diffuse :", diffuseSurface);
		console.log("ambient: ", addressFromSurface(ambiantSurface));
		console.log("bump :",addressFromSurface(bumpSurface));
		console.log("transparency :",addressFromSurface(transparencySurface));
		
		if(textureMap) {
			name = textureMap;
		}
		
		if(ambientAddress) {
			raptorjs.resources.addTexture("media/textures/"+name+"/"+ambientAddress+".png", ambientAddress);
			
			var specularAddress = getTextureName(ambientAddress) + "_spec";
			var offsetAddress = getTextureName(ambientAddress) + "_offset";
			
			raptorjs.resources.addTexture("media/textures/"+name+"/"+specularAddress+".png", specularAddress);
		}
		
	/*
		if(!ambientAddress) {
			raptorjs.resources.addTexture("media/textures/"+name+"/"+diffuseSurface+".png", diffuseSurface);
			ambientAddress = diffuseSurface;
			
			var offsetAddress = getTextureName(diffuseSurface) + "_offset";
			
			var specularAddress = getTextureName(ambientAddress) + "_spec";
			raptorjs.resources.addTexture("media/textures/"+name+"/"+specularAddress+".png", specularAddress);
		}
		
		console.log("specular : ", specularAddress);
		
		if(!bumpAddress)
			var bumpAddress = getTextureName(ambientAddress) + "_ddn";
	*/

		raptorjs.resources.addTexture("media/textures/"+name+"/"+bumpAddress+".png", bumpAddress);
		raptorjs.resources.addTexture("media/textures/"+name+"/"+offsetAddress+".png", offsetAddress);

		if(transparentAddress)
			raptorjs.resources.addTexture("media/textures/"+name+"/"+transparentAddress+".png", transparentAddress);
		else
			transparentAddress = false;
		
		material.name = materialNode.getAttribute('id').replace('-material', '');
	
		
		material.ambient = ambientAddress;
		material.normal = bumpAddress;
		material.transparent = transparentAddress;
		material.specular = specularAddress;
		material.offset = offsetAddress;

		materialContainer[c] = material;
	}

	raptorjs.setLoadingText("Serialized Model: " + url);
	
	this.materialContainers.push(materialContainer);
	
	var geometryNodes = geometries.getElementsByTagName("geometry");
	
	// serialize meshes
	for(var g = 0; g<geometryNodes.length;g++) {
	
		var child = geometryNodes[g];
		var source = child.getElementsByTagName("source");
		
		var positionSource = source[0];
		var normalSource = source[1];
		var coordSource = source[2];
		var tangentSource = source[3];
		var binormalSource = source[4];
		
		if(coordSource) {
		
			var triangles = child.getElementsByTagName("triangles");
			
			
			var subEntity = raptorjs.createObject("subEntity");
			var mesh = raptorjs.createObject("mesh");
			
			mesh.vertices 	= new Float32Array( get_buffer_data( positionSource.getElementsByTagName('float_array') ) );
			mesh.textcoords  = new Float32Array(  get_buffer_data( coordSource.getElementsByTagName('float_array') ) );
			mesh.normals   	= new Float32Array( get_buffer_data( normalSource.getElementsByTagName('float_array') ) ); 
			
			//if(tangentSource){
			//	mesh.tangents   = new Float32Array( get_buffer_data( tangentSource.getElementsByTagName('float_array') ) ); 
			//	mesh.binormals   = new Float32Array( get_buffer_data( binormalSource.getElementsByTagName('float_array') ) ); 
			//}
			
			for(var n = 0; n<triangles.length;n++) {
				var inputs = triangles[n].getElementsByTagName('input');

				mesh.indices =  triangles[0].getElementsByTagName('p')[0].childNodes[0].data.split(' ').map(parseFloat);
				mesh.materialName = triangles[0].getAttribute('material').replace('-material', '');
				
				this.entityContainer.push(mesh);
			
			}
		}
	}
	
	raptorjs.setLoadingText("Serialized Meshes: " + url);
}


/**
 * check if material exists
 * @param {(array)} meshes
 * @param {(string)} materialName
**/
raptorjs.renderSystem.prototype.materialExist = function(meshes, materialName) {
	for(var c = 0; c<meshes.length; c++) {
		if(meshes[c][0].materialName == materialName) {
			return c;
		}
	}
	
	return false;
}


/**
 * Concat float32
 * @param {(int)} first
 * @param {(int)} second
**/
function Float32Concat(first, second)
{
	var firstLength = first.length;
	var result = new Float32Array(firstLength + second.length);

	result.set(first);
	result.set(second, firstLength);

	return result;
}


/**
 * Concat float32
 * @param {(int)} first
 * @param {(int)} second
**/
function Int32Concat(first, second)
{
	var firstLength = first.length;
	var result = new Int32Array(firstLength + second.length);

	result.set(first);
	result.set(second, firstLength);

	return result;
}


/**
 * finalize mesh
**/
raptorjs.renderSystem.prototype.finalizeLoadedMeshes = function() {

	var materialContainers = this.materialContainers;
	var meshes = this.entityContainer;
	var materialMeshes = [];
	var unsortedEntitys = [];
	
	for(var z = 0; z<materialContainers.length; z++) {
		var materials = materialContainers[z];
		
		for(var b = 0; b<materials.length;b++) {
		
			var currentMaterial = materials[b];
			
			materialMeshes[b] = [];
			
			for(var c = 0; c<meshes.length; c++) {
				var mesh = meshes[c];
				
				if(mesh.materialName == currentMaterial.name)
					materialMeshes[b].push(mesh);
			}
			
			// create one mesh per material
			var indexArray = new Int32Array();
			var vertexArray = new Float32Array();
			var textcoordArray = new Float32Array();
			var normalArray = new Float32Array();
			var tangentArray;
			var binormalArray;
			
			if(mesh.tangents) {
				var tangentArray = new Float32Array();
				var binormalArray = new Float32Array();
			}
			
			var indexCounter = 0;
			
			var currentMaterialMeshes = materialMeshes[b];
			
			for(var c = 0; c<currentMaterialMeshes.length; c++) {
				var mesh = currentMaterialMeshes[c];
				var indices = mesh.indices;
				
				indices = indices.map(function(x) { return x + indexCounter; });
				
				indexArray = Int32Concat(indexArray, new Int32Array(indices));
				
				vertexArray = Float32Concat(vertexArray, mesh.vertices);
				textcoordArray = Float32Concat(textcoordArray, mesh.textcoords);
				normalArray = Float32Concat(normalArray, mesh.normals);
				
				if(mesh.tangents) {
					tangentArray = Float32Concat(tangentArray, mesh.tangents);
					binormalArray = Float32Concat(binormalArray, mesh.binormals);
				}
				
				indexCounter = vertexArray.length/3;
			}
			
			var diffuseTexture = raptorjs.resources.getTexture(currentMaterial.ambient);
	
			var diffuseSampler = raptorjs.createObject("sampler2D");
			diffuseSampler.texture = diffuseTexture;
			diffuseSampler.anisotropic = true;
			
			var material = raptorjs.createObject("material");
			var mesh = raptorjs.createObject('mesh');
			
			mesh.createMeshFromArrays(indexArray, vertexArray, normalArray, textcoordArray, tangentArray, binormalArray);
			mesh.createTangentAndBinormal();
			
			if(currentMaterial.normal) {
				var normalTexture = raptorjs.resources.getTexture(currentMaterial.normal);
				var normalSampler = raptorjs.createObject("sampler2D");
				
				if(normalTexture) {
					normalSampler.texture = normalTexture;
					material.addNormal(normalSampler);
				}
			} 
			
			if(currentMaterial.transparent) {
				
				console.log("add transparency");
				var transparencySampler = raptorjs.createObject("sampler2D");
				var transparencyTexture = raptorjs.resources.getTexture(currentMaterial.transparent);
				transparencySampler.texture = transparencyTexture;
				
				material.addTransparentyMap(transparencySampler);
			} 
			
			if(currentMaterial.specular) {
				
				var specularTexture = raptorjs.resources.getTexture(currentMaterial.specular);
				
				if(specularTexture) {
					var specularSampler = raptorjs.createObject("sampler2D");
					specularSampler.texture = specularTexture;
					
					material.addSpecularMap(specularSampler);
				}
			}

			if(currentMaterial.offset) {
				
				var offsetTexture = raptorjs.resources.getTexture(currentMaterial.offset);
				
				if(offsetTexture) {
					var offsetSampler = raptorjs.createObject("sampler2D");
					offsetSampler.texture = offsetTexture;
					
					material.addDisplacement(offsetSampler);
				}
			}
			
			material.addTexture(diffuseSampler);
			
			mesh.addMaterial(material);
			
			var entity = raptorjs.createObject("entity");
			
			entity.addMesh(mesh);
			
			unsortedEntitys.push( entity );
		}
	}
	
	var orderedScene = [];
	
	for(var c = 0; c<unsortedEntitys.length; c++){
		var entity = unsortedEntitys[c];
		var mesh = entity.mesh;
		var material = mesh.materials[0];
		
		if(material.normals.length == 0)
			raptorjs.scene.addEntity( entity );
	}
	
	for(var c = 0; c<unsortedEntitys.length; c++){
		var entity = unsortedEntitys[c];
		var mesh = entity.mesh;
		var material = mesh.materials[0];
		
		if(material.normals.length > 0)
			raptorjs.scene.addEntity( entity );
	}

	// convert to json
	var sceneEntitys = raptorjs.scene.entitys;
	var cleanEntitys = [];
	
	for(var c = 0; c<sceneEntitys.length; c++) {
		var entity = sceneEntitys[c];
		var mesh = entity.mesh;
		var material = mesh.materials[0];
		
		var cleanEntity = {};
		
		cleanEntity.tangents = mesh.tangentBuffer.data;
		cleanEntity.binormals = mesh.binormalBuffer.data;
		cleanEntity.normals = mesh.vertexNormalBuffer.data;
		cleanEntity.textureCoords = mesh.textureCoordBuffer.data;
		cleanEntity.indices = mesh.vertexIndexBuffer.data;
		cleanEntity.vertices = mesh.vertexPositionBuffer.data;
		
		var cleanMaterial = {};
		
		cleanMaterial.displacementMaps = getUrlFromMap( material.displacementMaps[0] );
		cleanMaterial.normals = getUrlFromMap( material.normals[0]);
		cleanMaterial.specularMaps = getUrlFromMap( material.specularMaps[0] );
		cleanMaterial.textures = getUrlFromMap( material.textures[0] );
		cleanMaterial.transparencyMaps = getUrlFromMap( material.transparencyMaps[0] );

		cleanEntity.material = cleanMaterial;
		
		var p = document.createElement("p");
		$(p).attr("id", "download"+c);
	
		$("#Properties").append(p);
		
		Downloadify.create(p ,{
			filename: 'mesh'+c+'.json',
			data: JSON.stringify(cleanEntity),
			onComplete: function(){ alert('Your File Has Been Saved!'); },
			onCancel: function(){ alert('You have cancelled the saving of this file.'); },
			onError: function(){ alert('You must put something in the File Contents or there will be nothing to save!'); },
			swf: 'media/libs/downloadify/downloadify.swf',
			downloadImage: 'media/libs/downloadify/download.png',
			width: 100,
			height: 30,
			transparent: true,
			append: false
		});
	}

	function getUrlFromMap(map) {
		if(map)
			return map.texture.url;
		else
			return false;
	} 

	
	var entityContainer = raptorjs.scene.entitys;
	
	raptorjs.setLoadingText("Created Material Buffers: ");
	var diffuseTexture = raptorjs.resources.getTexture("white");
	var diffuseSampler = raptorjs.createObject("sampler2D");
	diffuseSampler.texture = diffuseTexture;

	
	var indexArray = new Int32Array();
	var vertexArray = new Float32Array();
	var textcoordArray = new Float32Array();
	var normalArray = new Float32Array();
	
	var indexCounter = 0;

	for(var b = 0; b<entityContainer.length;b++) {
		var mesh = entityContainer[b].mesh;
		var material = mesh.materials[0];
		
		var indices = new Int32Array(mesh.vertexIndexBuffer.data);
		
		for(var f = 0; f<indices.length; f++) {
			indices[f] += indexCounter;
		}
		
		indexArray = Int32Concat(indexArray, indices);

		vertexArray = Float32Concat(vertexArray, new Float32Array(mesh.vertexPositionBuffer.data));
		textcoordArray = Float32Concat(textcoordArray, new Float32Array(mesh.textureCoordBuffer.data));
		normalArray = Float32Concat(normalArray, new Float32Array(mesh.vertexNormalBuffer.data));

		indexCounter = vertexArray.length/3;
	
	}

	
	raptorjs.setLoadingText("Created Depth Buffers: ");

	var material = raptorjs.createObject("material");
	material.addTexture(diffuseSampler);
	
	var meshObject = raptorjs.createObject('mesh');
	meshObject.createMeshFromArrays(indexArray, vertexArray, normalArray, textcoordArray);
	meshObject.addMaterial(material);
	
	var entity = raptorjs.createObject("entity");
	entity.addMesh(meshObject);
	
	this.depthEntitys.push(entity);
}
	
	
/**
 * isBitSet
 * @param {(int)} value
 * @param {(int)} position
**/
function isBitSet( value, position ) {

	return value & ( 1 << position );

};
	

/**
 * ends With
 * @param {(string)} suffix
**/	
String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};


/**
 * starts with
 * @param {(string)} suffix
**/	
if (typeof String.prototype.startsWith != 'function') {
  // see below for better implementation!
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}
raptorjs.renderSystem.prototype.setUniform = function( name, value ) {
	var scene = this.activeScene;
	var entitys = scene.getEntitys();

	for(var e = 0;e<entitys.length;e++) {
		var entity = entitys[e];
		var mesh = entity.mesh;
		
		mesh.colorInfoShader.setUniform(name, value);
	}
}

raptorjs.renderSystem.prototype.render = function( ) {
	
	
	var camera = raptorjs.mainCamera;
	camera.update();
	
	var viewProjection = camera.worldViewProjection;
	var view = camera.view;
	
	var scene = raptorjs.assimpLoader.scene;
	//var scene = this.activeScene;
	var entitys = scene.getEntitys();

	//gl.frontFace(gl.CCW);

	var currentShaderName;
	
	//if(this.deferred)
	gl.bindFramebuffer(gl.FRAMEBUFFER, this.mtrFramebuffer);
	//else // if forward
	//	gl.bindFramebuffer(gl.FRAMEBUFFER, null );
	
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.viewport(0, 0, raptorjs.width, raptorjs.height);
	gl.frontFace(gl.CCW);
	
	
		//console.log(viewProjection);
	
	for(var e = 0;e<entitys.length;e++) {
		var entity = entitys[e];
		var mesh = entity.mesh;
		var material = mesh.materials[0];
		var sampler = material.textures[0];
		var texture = sampler.texture;

		if(material) {
		
			var color = material.color;
			var world = entity.getUpdatedWorldMatrix();
			colorInfoShader = mesh.colorInfoShader;
			
			
			//if(mesh.shaderName != currentShaderName){
			//	colorInfoShader = mesh.colorInfoShader;
				
				//currentShaderName = mesh.shaderName;
				
				colorInfoShader.setUniform("worldViewProjection", viewProjection );
				colorInfoShader.update();
				
				
				//if(!this.deferred)
				//	colorInfoShader.setUniform("lightViewProjection1", shadowViewProjection );
				
				colorInfoShader.setUniform("cameraPosition", camera.eye );
				colorInfoShader.setUniform("world", world  );
				//colorInfoShader.setUniform("world3x3", raptorjs.matrix4.toMatrix3( world ) );
				
					
			//}
			
			if( material.textures.length > 0 ) {
			
				var textureSampler = material.textures[0];
				var texture = textureSampler.texture;
				var textureUniform = colorInfoShader.getUniformByName('texture');
				
				
				gl.activeTexture( gl.TEXTURE0 );
				gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
				gl.uniform1i(textureUniform, 0);
				
				var attribute = colorInfoShader.getAttributeByName('uv');
				gl.bindBuffer(gl.ARRAY_BUFFER, mesh.textureCoordBuffer);
				gl.vertexAttribPointer(attribute, 2, gl.FLOAT, false, 0, 0);
			
			}
			
			if(material.normals.length > 0) {
				
				var textureSampler = material.normals[0];
				var texture = textureSampler.texture;
				var textureUniform = colorInfoShader.getUniformByName('normalSampler');
				
				gl.activeTexture( gl.TEXTURE0 + 1);
				gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
				gl.uniform1i(textureUniform, 1);
				
			}
			

			if(material.transparencyMaps.length > 0) {
				gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
				gl.enable(gl.BLEND);
				gl.disable(gl.DEPTH_TEST);
			} else  {
				gl.disable(gl.BLEND);
				gl.enable(gl.DEPTH_TEST);
			}
			
			if(material.useParallax) {
				colorInfoShader.setUniform("view", camera.view );
			}
			

			var attribute = colorInfoShader.getAttributeByName('normal');
			gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexNormalBuffer);
			gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);

			var attribute = colorInfoShader.getAttributeByName('tangent');
			gl.bindBuffer(gl.ARRAY_BUFFER, mesh.tangentBuffer);
			gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);
			
			var attribute = colorInfoShader.getAttributeByName('binormal');
			gl.bindBuffer(gl.ARRAY_BUFFER, mesh.binormalBuffer);
			gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);

			var attribute = colorInfoShader.getAttributeByName('position');
			
			gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexPositionBuffer);
			gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);

			console.log(mesh.vertexIndexBuffer);
			
			gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, mesh.vertexIndexBuffer);
			gl.drawElements( gl.TRIANGLES, mesh.vertexIndexBuffer.numItems, gl.UNSIGNED_INT, 0 );  
		
			
		}
	}
	
	//this.deferredShader.setUniform("cameraPosition", camera.eye );
	
	//this.drawQuad( this.deferredShader, null );
	
	
	
	gl.flush();
	
}


/**
 * update buffers
**/	
raptorjs.renderSystem.prototype.updateBuffers = function( ) {
	
	var camera = raptorjs.mainCamera;

	var viewProjection = camera.worldViewProjection;
	var view = camera.view;
	
	var scene = this.activeScene;
	var entitys = scene.getEntitys();

	var shadowMap = this.shadowMaps[0];
	var shadowInfo = this.shadowMapT;

	var shadowViewProjection = shadowInfo.viewProjection;
	
	var corners = this.getFrustumCorners( camera.projection, camera.view );
	var worldCorners = this.getFrustumCorners( camera.projection, camera.view, true );
	
	gl.enable(gl.CULL_FACE);
	
	testest++;
	
	if(testest == 60) {
		
		//render shadow
		var shadowFramebuffer = this.shadowFramebuffer;
		var depthShader = this.depthShader;
	
		gl.bindFramebuffer(gl.FRAMEBUFFER, shadowFramebuffer);
		gl.viewport(0, 0, shadowFramebuffer.width, shadowFramebuffer.height);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		gl.useProgram(depthShader.program);
		
		depthShader.setUniform("viewProjection", shadowInfo.viewProjection );
		depthShader.setUniform("view", shadowInfo.view );
		
		for(var e = 0;e<entitys.length;e++) {
			var entity = entitys[e];//this.depthEntitys[e];
			var mesh = entity.mesh;
			
			
			var world = entity.getUpdatedWorldMatrix();
	
			depthShader.setUniform("world", world );
			depthShader.update();

			var attribute = depthShader.getAttributeByName('position');
			gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexPositionBuffer);
			gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);
	
			//var attribute = depthShader.getAttributeByName('normal');
			//gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexNormalBuffer);
			//gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);
			
			if(mesh.renderType == "indexed") {
				gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, mesh.vertexIndexBuffer);
				gl.drawElements( gl.TRIANGLES, mesh.vertexIndexBuffer.numItems, this.indexType, 0 ); 
			} else {
				gl.drawArrays(gl.TRIANGLES, 0, mesh.vertexPositionBuffer.numItems);
			}
		}
		
		if(this.shadowType == "VARIANCE") {
			
			this.drawQuad(this.convolutionShaderX, this.shadowConvolutionX );
			this.drawQuad(this.convolutionShaderY, this.shadowConvolutionY );
		
		}
	 }
	 
	 
	 gl.frontFace(gl.CCW);
	 
	 

	 
	 
	/*

	gl.frontFace(gl.CCW);
	
	var ssaoShader = this.ssaoShader;
	var colorInfoFramebuffer = this.colorInfoFramebuffer;
	var colorInfoShader = {};

	var currentShaderName;
	
	if(this.deferred)
		gl.bindFramebuffer(gl.FRAMEBUFFER, colorInfoFramebuffer );
	else // if forward
		gl.bindFramebuffer(gl.FRAMEBUFFER, null );
	
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.viewport(0, 0, colorInfoFramebuffer.width, colorInfoFramebuffer.height);
	
	for(var e = 0;e<entitys.length;e++) {
		var entity = entitys[e];
		var mesh = entity.mesh;
		var material = mesh.materials[0];
		var sampler = material.textures[0];
		var texture = sampler.texture;

		if(material) {
		
			var color = material.color;
			var world = entity.getUpdatedWorldMatrix();
			
			if(mesh.shaderName != currentShaderName){
				colorInfoShader = mesh.colorInfoShader;
				
				currentShaderName = mesh.shaderName;
				
				colorInfoShader.setUniform("worldViewProjection", viewProjection );
				if(!this.deferred)
				colorInfoShader.setUniform("lightViewProjection1", shadowViewProjection );
				
				colorInfoShader.setUniform("cameraPosition", camera.eye );
				colorInfoShader.setUniform("test", camera.eye[2] );
				
				colorInfoShader.setUniform("world", world  );
				//colorInfoShader.setUniform("world3x3", raptorjs.matrix4.toMatrix3( world ) );
				
				colorInfoShader.update();	
			}
			
			if( material.textures.length > 0 ) {
			
				var textureSampler = material.textures[0];
				var texture = textureSampler.texture;
				var textureUniform = colorInfoShader.getUniformByName('texture');
				
				
					gl.activeTexture( gl.TEXTURE0 );
					gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
					gl.uniform1i(textureUniform, 0);
				
				var attribute = colorInfoShader.getAttributeByName('uv');
				gl.bindBuffer(gl.ARRAY_BUFFER, mesh.textureCoordBuffer);
				gl.vertexAttribPointer(attribute, 2, gl.FLOAT, false, 0, 0);
			
			}
			
			if(material.normals.length > 0) {
				
				var textureSampler = material.normals[0];
				var texture = textureSampler.texture;
				var textureUniform = colorInfoShader.getUniformByName('normalSampler');
				
				//gl.activeTexture( gl.TEXTURE0 + 1);
				//gl.bindTexture( gl.TEXTURE_2D, texture.glTexture );
				//gl.uniform1i(textureUniform, 1);
				
			}
			

			if(material.transparencyMaps.length > 0) {
				gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
				gl.enable(gl.BLEND);
				gl.disable(gl.DEPTH_TEST);
			} else  {
				gl.disable(gl.BLEND);
				gl.enable(gl.DEPTH_TEST);
			}
			
			if(material.useParallax) {
				colorInfoShader.setUniform("view", camera.view );
			}
			

			var attribute = colorInfoShader.getAttributeByName('normal');
			gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexNormalBuffer);
			gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);

			var attribute = colorInfoShader.getAttributeByName('tangent');
			gl.bindBuffer(gl.ARRAY_BUFFER, mesh.tangentBuffer);
			gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);
			
			var attribute = colorInfoShader.getAttributeByName('binormal');
			gl.bindBuffer(gl.ARRAY_BUFFER, mesh.binormalBuffer);
			gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);

			var attribute = colorInfoShader.getAttributeByName('position');
			
			gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexPositionBuffer);
			gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);
			
			if(!this.ssaoOnly) {
				gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, mesh.vertexIndexBuffer);
				gl.drawElements( gl.TRIANGLES, mesh.vertexIndexBuffer.numItems, this.indexType, 0 );  
				
				
				
			}
		}
	}
*/
	
	//if(this.deferred) {
	
		var ssaoShader = this.ssaoShader;
		var shaderProgram = ssaoShader.program;
		var infoFrameBuffer = this.infoFrameBuffer;
		var infoShader = this.infoShader;
		var currentShaderName;
		
		gl.useProgram(infoShader.program);
		gl.bindFramebuffer(gl.FRAMEBUFFER, infoFrameBuffer);
		gl.viewport(0, 0, infoFrameBuffer.width, infoFrameBuffer.height);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		
		for(var e = 0;e<entitys.length;e++) {
			var entity = entitys[e];
			var mesh = entity.mesh;
			var material = mesh.materials[0];
			var world = entity.getUpdatedWorldMatrix();
			
			if(mesh.shaderName != currentShaderName) {
			
				infoShader = mesh.infoShader;
				
				currentShaderName = mesh.shaderName;
					
				infoShader.setUniform("worldViewProjection", viewProjection );
				//infoShader.setUniform("view", view );
				infoShader.setUniform("world", world );
				infoShader.update();
				
			}
			
			var attribute = infoShader.getAttributeByName('position');
			gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexPositionBuffer);
			gl.vertexAttribPointer(attribute, 3, gl.FLOAT, false, 0, 0);
			
			if(material.normals.length > 0) {
				var sampler = material.normals[0];
				var normalTexture = sampler.texture;	

				var attribute = infoShader.getAttributeByName('uv');
				gl.bindBuffer(gl.ARRAY_BUFFER, mesh.textureCoordBuffer);
				gl.vertexAttribPointer(attribute, 2, gl.FLOAT, false, 0, 0);
				
				var glTexture = normalTexture.glTexture;
				var textureUniform = infoShader.getUniformByName('normalSampler');
				gl.activeTexture( gl.TEXTURE0 );
				gl.bindTexture( gl.TEXTURE_2D, glTexture );
				gl.uniform1i(textureUniform, 0);
			}
			
			if(mesh.renderType == "indexed") {
				gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, mesh.vertexIndexBuffer);
				gl.drawElements( gl.TRIANGLES, mesh.vertexIndexBuffer.numItems, this.indexType, 0 );  
			} else {
				gl.drawArrays(gl.TRIANGLES, 0, mesh.vertexPositionBuffer.numItems);
			}
		}

		gl.disable(gl.CULL_FACE);
		gl.disable(gl.DEPTH_TEST);
		gl.disable(gl.BLEND);


		if(this.useSSAO) {
		
			// screen space ambiant occlusion
			if(this.ssaoOnly) {
				this.drawQuad(ssaoShader, this.ssaoFramebuffer  );// this.ssaoFramebuffer
				
				// ssao blur
				this.drawQuad(this.ssaoConvolutionShaderX, this.ssaoConvolutionX );
				this.drawQuad(this.ssaoConvolutionShaderY, null );
			} else {
				this.drawQuad(ssaoShader, this.ssaoFramebuffer );// this.ssaoFramebuffer
			
				// ssao blur
				this.drawQuad(this.ssaoConvolutionShaderX, this.ssaoConvolutionX );
				this.drawQuad(this.ssaoConvolutionShaderY, this.ssaoConvolutionY );
			}
		}
		

		if(!this.ssaoOnly) {
		
			
			// light accumulation
			// this.uberShader.setUniform("lightPosition1", shadowInfo.eye);
			this.uberShader.setUniform("frustumWorldCorners", worldCorners);
			this.uberShader.setUniform("cameraPosition", camera.eye );
			this.uberShader.setUniform("test", camera.eye[2] );
			this.uberShader.setUniform("lightViewProjection1", shadowViewProjection );
			
			gl.viewport(0, 0, raptorjs.width, raptorjs.height);
			
			//if(this.antiAlias)
			this.drawQuad(this.uberShader, null    ); //this.diffuseAcc 
			//else
			//	this.drawQuad(this.uberShader, null );
			//	this.globalIllumination.update();
			//		this.hdr.update();
		}
		
	//} // end deferred
	
	
	switch(this.antiAlias) {
	
		case "FXAA":
			
			this.fxaa.update();
			
		break;
		
		case "SMAA":	// default
			
			this.smaa.update();
		
		break;
		
		case "MSAA":
			this.msaa.setMatrices(viewProjection, this.prevViewProjection);
			this.msaa.MSAAShader.setUniform("frustumWorldCorners", worldCorners);
			this.msaa.MSAAShader.setUniform("cameraPosition", camera.eye );
			this.msaa.MSAAShader.setUniform("test", camera.eye[2] );
			this.msaa.pipeline();
			

			this.drawQuad(this.prevDiffuseShader, this.prevDiffuseFramebuffer);//this.diffuseAcc
			this.prevViewProjection = viewProjection;	
		break;
		
		default: 
		
		break;
		
	}
	
	//this.fxaa.update();

	//	this.shadowShader.setUniform("frustumWorldCorners", worldCorners);
	//	this.shadowShader.setUniform("cameraPosition", camera.eye );
	//	this.shadowShader.setUniform("test", camera.eye[2] );
	//	this.shadowShader.setUniform("lightViewProjection1", shadowViewProjection );
	//	gl.viewport(0, 0, raptorjs.width, raptorjs.height);
	//	this.drawQuad(this.shadowShader, this.shadowOcclusionFramebuffer);//this.diffuseAcc
	// this.uberShader.setUniform("worldToLPV", this.globalIllumination.worldToLPVNormTexRender);
	// screen space ambiant occlusion
	// this.drawQuad(this.ssgiShader, null);
	
	// global illumination

	//if(!this.prevViewProjection)
	//	this.prevViewProjection = viewProjection;
	//this.irradianceVolume.update();
	
	gl.flush();
	
	//this.drawQuad(this.prevDiffuseShader, this.prevDiffuseFramebuffer);//this.diffuseAcc
	//this.prevViewProjection = viewProjection;
}


/**
 * gauss
**/	
function gauss(x, sigma) {
  return Math.exp(- (x * x) / (2.0 * sigma * sigma));
}


/**
 * build kernel
 * @param {(int)} sigma
**/	
function buildKernel(sigma) {
	var kMaxKernelSize = 25;
	var kernelSize = 2 * Math.ceil(sigma * 3.0) + 1;
	if (kernelSize > kMaxKernelSize) {
		kernelSize = kMaxKernelSize;
	}
	var halfWidth = (kernelSize - 1) * 0.5
	var values = new Array(kernelSize);
	var sum = 0.0;
	for (var i = 0; i < kernelSize; ++i) {
		values[i] = gauss(i - halfWidth, sigma);
		sum += values[i];
	}
	// Now normalize the kernel.
	for (var i = 0; i < kernelSize; ++i) {
		values[i] /= sum;
	}
	return values;
}


/**
 * create sampler from framebuffer
 * @param {(framebuffer)} framebuffer
**/	
raptorjs.renderSystem.prototype.samplerFromFramebuffer = function(framebuffer) {
	var texture = raptorjs.createObject('texture');
	
	if(framebuffer.type == "depth")
		texture.dataType = 'depth';
	else
		texture.dataType = 'framebuffer';
		
		
	texture.data = framebuffer.texture;
	texture.width = framebuffer.width;
	texture.height = framebuffer.height;

	var sampler = raptorjs.createObject("sampler2D");
	sampler.texture = texture;
	
	return sampler;
}


/**
 * Initialize all shaders and renderbuffers.
 */
raptorjs.renderSystem.prototype.createBuffers = function() {
	this.createShadowSurface();
	
	//depth shader
	this.depthShader = raptorjs.createObject("shader");
	this.depthShader.definePragma("VARIANCE", (this.shadowType == "VARIANCE") ? 1 : 0 );
	this.depthShader.createFomFile("shaders/regularDepth.shader");
	this.depthShader.setUniform("far", raptorjs.mainCamera.far );
	
	//var diffuseTexture = raptorjs.resources.getTexture("randomRotation");
	var diffuseTexture = raptorjs.textureFromTypedArray(randomImage, 64, 64);
	this.randomRotationSampler = raptorjs.createObject("sampler2D");
	this.randomRotationSampler.texture = diffuseTexture;
	this.randomRotationSampler.MIN_FILTER = gl.NEAREST;
	this.randomRotationSampler.MAG_FILTER = gl.NEAREST;
	
	//var diffuseTexture = raptorjs.resources.getTexture("ssao");
	var diffuseTexture = raptorjs.textureFromTypedArray(randomImage2, 4, 4);
	this.randomSampler = raptorjs.createObject("sampler2D");
	this.randomSampler.texture = diffuseTexture;
	this.randomSampler.MIN_FILTER = gl.NEAREST;
	this.randomSampler.MAG_FILTER = gl.NEAREST;
	
	//if(this.deferred) {
	
	raptorjs.setLoadingText("Create Frame Buffers: ");

	this.inforSampler 	= this.samplerFromFramebuffer(this.infoFrameBuffer);
//	this.shadowConvXSampler = this.samplerFromFramebuffer(this.shadowConvolutionX);
//	this.shadowConvYSampler = this.samplerFromFramebuffer(this.shadowConvolutionY);
	this.shadowOcclusionSampler = this.samplerFromFramebuffer(this.shadowOcclusionFramebuffer);
	this.colorInfoSampler 	= this.samplerFromFramebuffer(this.colorInfoFramebuffer);
	this.diffuseAccSampler  = this.samplerFromFramebuffer(this.diffuseAcc);
	//var prevDiffuseSampler = this.samplerFromFramebuffer(this.prevDiffuseFramebuffer); 
	
	this.ssaoSampler = this.samplerFromFramebuffer(this.ssaoFramebuffer);
	this.ssaoSampler.format = gl.RGB;
	this.ssaoSampler.internalformat = gl.RGB;
	this.ssaoSampler.WRAP_S = gl.CLAMP_TO_EDGE;
	this.ssaoSampler.WRAP_T = gl.CLAMP_TO_EDGE;	
	
	this.ssaoConvolutionXSampler = this.samplerFromFramebuffer(this.ssaoConvolutionX);
	this.ssaoConvolutionXSampler.WRAP_S = gl.CLAMP_TO_EDGE;
	this.ssaoConvolutionXSampler.WRAP_T = gl.CLAMP_TO_EDGE;	
	
	this.ssaoConvolutionYSampler = this.samplerFromFramebuffer(this.ssaoConvolutionY);
	this.ssaoConvolutionYSampler.WRAP_S = gl.CLAMP_TO_EDGE;
	this.ssaoConvolutionYSampler.WRAP_T = gl.CLAMP_TO_EDGE;
	
/*
	this.shadowConvXSampler.format = gl.RG;
	this.shadowConvXSampler.internalformat = gl.RG;
	
	this.shadowConvYSampler.format = gl.RG;
	this.shadowConvYSampler.internalformat = gl.RG;
	
*/
	var infoTexture = raptorjs.createObject('texture');

	infoTexture.data = this.infoFrameBuffer.texture;
	infoTexture.dataType = 'framebuffer';
	infoTexture.width = this.infoFrameBuffer.width;
	infoTexture.height = this.infoFrameBuffer.height;

	infoSampler = raptorjs.createObject("sampler2D");
	infoSampler.texture = infoTexture;

/*
	//convolution shader
	this.convolutionShaderX = raptorjs.createObject("shader");
	this.convolutionShaderX.createFomFile("shaders/convolution.shader");

	//this.convolutionShaderX.setUniform("kernel", kernel2 );
	this.convolutionShaderX.setUniform("viewProjection", this.quadViewProjection);
	this.convolutionShaderX.setUniform("imageIncrement", [1/this.shadowConvolutionX.width,0] );
	this.convolutionShaderX.setUniform("image", this.shadowSampler );

	//convolution shader
	this.convolutionShaderY = raptorjs.createObject("shader");
	this.convolutionShaderY.createFomFile("shaders/convolution.shader");
	
	//this.convolutionShaderY.setUniform("kernel", kernel2 );
	this.convolutionShaderY.setUniform("viewProjection", this.quadViewProjection);
	this.convolutionShaderY.setUniform("imageIncrement", [0, 1 / this.shadowConvolutionY.height] );
	this.convolutionShaderY.setUniform("image", this.shadowConvXSampler  );
*/	
	
	this.ssaoConvolutionShaderX = raptorjs.createObject("shader");
	this.ssaoConvolutionShaderX.createFomFile("shaders/convolution.shader");
	this.ssaoConvolutionShaderX.setUniform("viewProjection", this.quadViewProjection);
	this.ssaoConvolutionShaderX.setUniform("imageIncrement", [1 / raptorjs.width,0] );
	this.ssaoConvolutionShaderX.setUniform("image", this.ssaoSampler );
	this.ssaoConvolutionShaderX.setUniform("depthSampler", infoSampler);
	this.ssaoConvolutionShaderX.setUniform("far", raptorjs.mainCamera.far );
	
	


	
	

	this.ssaoConvolutionShaderY = raptorjs.createObject("shader");
	this.ssaoConvolutionShaderY.createFomFile("shaders/convolution.shader");
	this.ssaoConvolutionShaderY.setUniform("viewProjection", this.quadViewProjection );
	this.ssaoConvolutionShaderY.setUniform("imageIncrement", [0, 1 / raptorjs.height] );
	this.ssaoConvolutionShaderY.setUniform("image", this.ssaoConvolutionXSampler );
	this.ssaoConvolutionShaderY.setUniform("far", raptorjs.mainCamera.far );
	this.ssaoConvolutionShaderY.setUniform("depthSampler", infoSampler);


	//normal shader
	this.infoShader = raptorjs.createObject("shader");
	this.infoShader.definePragma("NORMAL_MAP", 0);
	this.infoShader.createFomFile("shaders/info.shader");
	
	this.bumpInfoShader = raptorjs.createObject("shader");
	this.bumpInfoShader.definePragma("NORMAL_MAP", 1);
	this.bumpInfoShader.createFomFile("shaders/info.shader");

	//this.infoShader.setUniform("far", raptorjs.mainCamera.far );
	//this.infoShader.setUniform("near", raptorjs.mainCamera.near );
	//this.infoShader.setUniform("shadowSampler1", raptorjs.system.shadowConvYSampler );
	//this.infoShader.setUniform("shadowBias", .0012  );
	//this.infoShader.setUniform("g_minVariance", 0.001);
	//this.infoShader.setUniform("kernel", buildKernel(1) );
	
	//this.colorInfoShader = raptorjs.createObject("shader");
	//this.colorInfoShader.definePragma("TEXTURE", 1);
	//this.colorInfoShader.createFomFile("shaders/colorInfo.shader");
	//this.colorInfoShader.setUniform("uvScale", 1 );

	this.reflectionShader = raptorjs.createObject("shader");
	this.reflectionShader.createFomFile("shaders/texture.shader");
	
	/*
		this.colorInfoSimpleShader = raptorjs.createObject("shader");
		this.colorInfoSimpleShader.createFomFile("shaders/colorInfoSimple.shader");

		var randomSampler = this.createRandomSphereTexture(64);
		randomSampler.MIN_FILTER = gl.NEAREST;
		randomSampler.MAG_FILTER = gl.NEAREST;
	*/

	
	var vec3 = raptorjs.vector3;
	var vec4 = raptorjs.vector4;
	var radius = 0.02;
	
	//this.irradianceVolume = raptorjs.createObject("particleSandbox");
	
	var scale = [ vec3.scale( vec3(-0.556641,-0.037109,-0.654297), radius ), 
				vec3.scale( vec3(0.173828,0.111328,0.064453), radius ), 
				vec3.scale( vec3(0.001953,0.082031,-0.060547), radius ), 
				vec3.scale( vec3(0.220703,-0.359375,-0.062500), radius ), 
				vec3.scale( vec3(0.242188,0.126953,-0.250000), radius ), 
				vec3.scale( vec3(0.070313,-0.025391,0.148438), radius ), 
				vec3.scale( vec3(-0.078125,0.013672,-0.314453), radius ), 
				vec3.scale( vec3(0.117188,-0.140625,-0.199219), radius ), 
				vec3.scale( vec3(-0.251953,-0.558594,0.082031), radius ), 
				vec3.scale( vec3(0.308594,0.193359,0.324219), radius ), 
				vec3.scale( vec3(0.173828,-0.140625,0.031250), radius ), 
				vec3.scale( vec3(0.179688,-0.044922,0.046875), radius ), 
				vec3.scale( vec3(-0.146484,-0.201172,-0.029297), radius ), 
				vec3.scale( vec3(-0.300781,0.234375,0.539063), radius ), 
				vec3.scale( vec3(0.228516,0.154297,-0.119141), radius ), 
				vec3.scale( vec3(-0.119141,-0.003906,-0.066406), radius ), 
				vec3.scale( vec3(-0.218750,0.214844,-0.250000), radius ), 
				vec3.scale( vec3(0.113281,-0.091797,0.212891), radius ), 
				vec3.scale( vec3(0.105469,-0.039063,-0.019531), radius ), 
				vec3.scale( vec3(-0.705078,-0.060547,0.023438), radius ), 
				vec3.scale( vec3(0.021484,0.326172,0.115234), radius ), 
				vec3.scale( vec3(0.353516,0.208984,-0.294922), radius ), 
				vec3.scale( vec3(-0.029297,-0.259766,0.089844), radius ), 
				vec3.scale( vec3(-0.240234,0.146484,-0.068359), radius ), 
				vec3.scale( vec3(-0.296875,0.410156,-0.291016), radius ), 
				vec3.scale( vec3(0.078125,0.113281,-0.126953), radius ), 
				vec3.scale( vec3(-0.152344,-0.019531,0.142578), radius ), 
				vec3.scale( vec3(-0.214844,-0.175781,0.191406), radius ), 
				vec3.scale( vec3(0.134766,0.414063,-0.707031), radius ), 
				vec3.scale( vec3(0.291016,-0.833984,-0.183594), radius ), 
				vec3.scale( vec3(-0.058594,-0.111328,0.457031), radius ), 
				vec3.scale( vec3(-0.115234,-0.287109,-0.259766), radius ) ];
				
	var kernelRad = [	[1.163003/radius,4.624262/radius,9.806342/radius,2.345541/radius], 
					[2.699039/radius,6.016871/radius,3.083554/radius,3.696197/radius], 
					[1.617461/radius,2.050939/radius,4.429457/radius,5.234036/radius], 
					[3.990876/radius,1.514475/radius,3.329241/radius,7.328508/radius], 
					[2.527725/radius,3.875453/radius,8.760140/radius,1.412308/radius], 
					[2.885205/radius,1.977866/radius,3.617674/radius,3.453552/radius], 
					[1.712336/radius,5.341163/radius,4.771728/radius,2.965737/radius], 
					[1.204293/radius,1.108428/radius,2.109570/radius,2.475453/radius] ]; 
					
					
					

	var JITTER_SIZE = 32;
	var RAND_MAX = 1.0;
	var JITTER_SAMPLES = 8;
	var data = [];
	
	var lb = {};
	lb.SlicePitch = 1.0 / 256;
	lb.RowPitch = 1.0 / 256;
	
    for (var i = 0; i<JITTER_SIZE; i++) {
        for (var j = 0; j<JITTER_SIZE; j++) {
            var rot_offset = (Math.random() / RAND_MAX - 1) * 2 * 3.1415926;

            for (var k = 0; k<JITTER_SAMPLES*JITTER_SAMPLES/2; k++) {

                var x, y;
				
                var v = [];

                x = k % (JITTER_SAMPLES / 2);
                y = (JITTER_SAMPLES - 1) - k / (JITTER_SAMPLES / 2);

                v[0] = (x * 2 + 0.5) / JITTER_SAMPLES;
                v[1] = (y + 0.5) / JITTER_SAMPLES;
                v[2] = (x * 2 + 1 + 0.5) / JITTER_SAMPLES;
                v[3] = v[1];
                
                // jitter
                v[0] += (Math.random() * 2 / RAND_MAX - 1) / JITTER_SAMPLES;
                v[1] += (Math.random() * 2 / RAND_MAX - 1) / JITTER_SAMPLES;
                v[2] += (Math.random() * 2 / RAND_MAX - 1) / JITTER_SAMPLES;
                v[3] += (Math.random() * 2 / RAND_MAX - 1) / JITTER_SAMPLES;

                // warp to disk
                var d = [];
                d[0] = sqrt(v[1]) * cos(2 * 3.1415926 * v[0] + rot_offset);
                d[1] = sqrt(v[1]) * sin(2 * 3.1415926 * v[0] + rot_offset);
                d[2] = sqrt(v[3]) * cos(2 * 3.1415926 * v[2] + rot_offset);
                d[3] = sqrt(v[3]) * sin(2 * 3.1415926 * v[2] + rot_offset);

                d[0] = (d[0] + 1.0) / 2.0;
                data[k*lb.SlicePitch + j*lb.RowPitch + i*4 + 0] = (d[0] * 255);
                d[1] = (d[1] + 1.0) / 2.0;
                data[k*lb.SlicePitch + j*lb.RowPitch + i*4 + 1] = (d[1] * 255);
                d[2] = (d[2] + 1.0) / 2.0;
                data[k*lb.SlicePitch + j*lb.RowPitch + i*4 + 2] = (d[2] * 255);
                d[3] = (d[3] + 1.0) / 2.0;
                data[k*lb.SlicePitch + j*lb.RowPitch + i*4 + 3] = (d[3] * 255);
            }
        }
    }

	var jitterTexture = raptorjs.textureFromArray(data, JITTER_SIZE, JITTER_SIZE, true);
	var jitterSampler = raptorjs.createObject("sampler2D");
	jitterSampler.texture = jitterTexture;

	
	
	this.shadowShader = raptorjs.createObject("shader");
	this.shadowShader.createFomFile("shaders/shadow.shader");
	this.shadowShader.setUniform("viewProjection", this.quadViewProjection );
	this.shadowShader.setUniform("far", raptorjs.mainCamera.far );
	this.shadowShader.setUniform("shadowBias", .5);
	this.shadowShader.setUniform("shadowDepthSampler", this.shadowSampler);
	this.shadowShader.setUniform("infoSampler", infoSampler);
	
	//this.prevDiffuseShader = raptorjs.createObject("shader");
	//this.prevDiffuseShader.createFomFile("shaders/copy.shader");
	//this.prevDiffuseShader.setUniform("viewProjection", this.quadViewProjection );
	//this.prevDiffuseShader.setUniform("texture", this.diffuseAccSampler );
	
	this.ssaoShaders = [];
	
	for(var c = 0; c<3; c++) {
		
		this.ssaoShader = raptorjs.createObject("shader");
		
		if(this.ssaoOnly) this.ssaoShader.definePragma("AMBIANT_ONLY", 1);
		
		this.ssaoShader.definePragma("SSAO_TYPE", c );
		
		this.ssaoShader.createFomFile("shaders/ambiantOcclusion.shader");
		
		this.ssaoShader.setUniform("viewProjection", this.quadViewProjection );
		this.ssaoShader.setUniform("screenWidth", raptorjs.width );
		this.ssaoShader.setUniform("screenHeight", raptorjs.height );
		this.ssaoShader.setUniform("far", raptorjs.mainCamera.far );
		this.ssaoShader.setUniform("randomSampler",  this.randomSampler);
		this.ssaoShader.setUniform("infoSampler", infoSampler);
		this.ssaoShader.setUniform("diffuseSampler", this.colorInfoSampler);
		//this.ssaoShader.setUniform("lightPosition", this.lightPosition);
		//this.ssaoShader.setUniform("definitionValue", .001);
		//this.ssaoShader.setUniform("scaleQuality", .2);
		this.ssaoShader.setUniform("scale", scale );
		this.ssaoShader.setUniform("kernel", scale );
		this.ssaoShader.setUniform("kernelRad", kernelRad );
		
		this.ssaoShaders.push(this.ssaoShader);
	}
	
	this.ssaoShader = this.ssaoShaders[0];
	
	for(var c = 0; c<scale.length; c++){
		var currentScale = scale[c];
	}
	
	this.globalIllumination = raptorjs.createObject("globalIllumination");
	this.globalIllumination.init();

		switch(this.antiAlias) {
			case "FXAA":
				
				//this.fxaa = raptorjs.createObject("fxaa");
				//this.fxaa.setColorSampler(this.diffuseAccSampler);
				
			break;
			
			case "SMAA":	// default
				
				this.smaa = raptorjs.createObject("smaa");
				this.smaa.setColorSampler( this.diffuseAccSampler );
				
			break;
			
			case "MSAA":
				this.msaa = raptorjs.createObject("MSAA");
				this.msaa.setColorSampler(this.diffuseAccSampler, prevDiffuseSampler);
				this.msaa.setDepthSampler(infoSampler);
			break;
		}
	
	//this.hdr = raptorjs.createObject("hdr");
	//this.hdr.setDiffuseSampler(this.diffuseAccSampler);
	
	//this.testShader = raptorjs.createObject("shader");
	//this.testShader.createFomFile("shaders/test.shader");
	this.uberShader = raptorjs.createObject("shader");
	this.uberShader.definePragma("VARIANCE", (this.shadowType == "VARIANCE") ? 1 : 0 );
	this.uberShader.createFomFile("shaders/uberPass.shader");
	this.uberShader.setUniform("viewProjection", this.quadViewProjection );
	
	this.uberShader.setUniform("infoSampler", infoSampler);
	this.uberShader.setUniform("diffuseSampler", this.colorInfoSampler);
	this.uberShader.setUniform("ambiantOccSampler", this.ssaoConvolutionYSampler ); // ssaoConvolutionYSampler
	this.uberShader.setUniform("randomSampler",  this.randomSampler);
	//this.uberShader.setUniform("shadow_kernel", shadow_kernel);
	this.uberShader.setUniform("sRotSampler", this.randomRotationSampler );

	 
	// if variance use saperate convolution
	if(this.shadowType == "VARIANCE") {
		this.uberShader.setUniform("shadowSampler1", this.shadowConvYSampler);
	} else {
		this.uberShader.setUniform("shadowSampler1", this.shadowSampler);
	}
	
	// saperate shadow
	// this.uberShader.setUniform("shadowSampler1", this.shadowOcclusionSampler);
	this.uberShader.setUniform("globalIlluminationSampler", this.globalIllumination.initLPVSampler);
	this.uberShader.setUniform("shadowBias", 9.999);
	this.uberShader.setUniform("far", raptorjs.mainCamera.far );
	//this.uberShader.setUniform("g_minVariance", 0.001);
	this.uberShader.setUniform("occlusionAmount", 1);
	this.uberShader.setUniform("specularPow", 6.381);
	this.uberShader.setUniform("spec", 0.281);
	//this.uberShader.setUniform("gloss", .001);
	this.uberShader.setUniform("randomSampler", this.randomSampler);
	this.uberShader.setUniform("screenWidth", raptorjs.width );
	this.uberShader.setUniform("screenHeight", raptorjs.height );

	/*
	this.ssgiShader = raptorjs.createObject("shader");
	this.ssgiShader.createFomFile("shaders/globalillumination.shader");
	this.ssgiShader.setUniform("viewProjection", this.quadViewProjection );
	this.ssgiShader.setUniform("infoSampler", infoSampler);
	this.ssgiShader.setUniform("albedoSampler",  this.colorInfoSampler  );
	this.ssgiShader.setUniform("diffuseAccSampler",this.diffuseAccSampler );
	this.ssgiShader.setUniform("randomSampler", randomSampler);
	this.ssgiShader.setUniform("screenWidth", raptorjs.width );
	this.ssgiShader.setUniform("screenHeight", raptorjs.height );
	this.ssgiShader.setUniform("far", raptorjs.mainCamera.far );
	
*/
	//random
	//var randomTexture = raptorjs.resources.getTexture("random");
	//var randomSampler = raptorjs.createObject("sampler2D");
	//randomSampler.texture = randomTexture;

	//info



		//this.ssaoShader.setUniform("colorInfoSampler", this.colorInfoSampler );
	//this.ssaoShader.setUniform("shadowSampler1", this.shadowConvYSampler);
	//this.ssaoShader.setUniform("shadowSampler1", this.shadowSampler);
	
	/*
	
	//default shader
	this.defaultShader = raptorjs.createObject("shader");
	this.defaultShader.createFomFile("shaders/texture.shader");

	
	
	
	this.finalTexture = raptorjs.createObject("texture");
	this.finalTexture.data = this.fxaaFrameBuffer.texture;
	this.finalTexture.dataType = 'framebuffer';
	this.finalTexture.width = this.fxaaFrameBuffer.width;
	this.finalTexture.height = this.fxaaFrameBuffer.height;
		
	this.finalSampler = raptorjs.createObject("sampler2D");
	this.finalSampler.texture = this.finalTexture;

	this.fxaaShader = raptorjs.createObject("shader");
	this.fxaaShader.createFomFile("shaders/fxaa3.shader");
	
	
	
	this.fxaaShader.setUniform("projection", this.quadProjection );
	this.fxaaShader.setUniform("view", this.quadView );
	//this.fxaaShader.setUniform("occlusionSampler", this.yConvSampler );
	this.fxaaShader.setUniform("colorSampler", colorInfoSampler );
	//this.fxaaShader.setUniform("test", this.shadowConvYSampler );
	this.fxaaShader.setUniform("edgeTreshold", 1.0 );
	this.fxaaShader.setUniform("edgeTresholdMin", 1.0 );
	this.fxaaShader.setUniform("subpixQuality", 0.25 );
	this.fxaaShader.setUniform("screenSizeInv", [1.0/raptorjs.width, 1.0/raptorjs.height] );
	*/
}


/**
 * create random points on a sphere
 * @param {(int)} size
**/
raptorjs.renderSystem.prototype.createRandomSphereTexture = function( size ) {
	var width = size;
	var height = size;

	var dataArray = [];
	
	for (var y = 0; y <= width; y++) {
		for (var x = 0; x <= height; x++) {
		
			var u = Math.random();
			var v = Math.random();
			var theta = 2 * Math.PI * u;
			var phi = Math.PI * v;
			var sinTheta = Math.sin(theta);
			var cosTheta = Math.cos(theta);
			var sinPhi = Math.sin(phi);
			var cosPhi = Math.cos(phi);
			var ux = cosTheta * sinPhi;
			var uy = cosPhi;
			var uz = sinTheta * sinPhi;
			
			var randomLength = Math.random();
			
			dataArray.push(ux  , uy , uz , 1.0);
		}
	}
	
	var text = raptorjs.textureFromArray(dataArray, width, height, true);
	
	var sampler = raptorjs.createObject("sampler2D");
	sampler.texture = text;
	
	return sampler;
}


/**
 * Create framebuffers.
 */
raptorjs.renderSystem.prototype.createFramebuffers = function () {
	this.infoFrameBuffer = this.createFrameBuffer(raptorjs.width, raptorjs.height, { type : gl.FLOAT, format: gl.RGB, internalformat: gl.RGB });
	this.reflectionFramebuffer = this.createFrameBuffer(1024, 1024);
	// this.fxaaFrameBuffer = this.createFrameBuffer(raptorjs.width, raptorjs.height);
	this.ssaoFramebuffer = this.createFrameBuffer(raptorjs.width, raptorjs.height, {  format: gl.RG, internalformat: gl.RG });
	this.ssaoConvolutionX = this.createFrameBuffer(raptorjs.width, raptorjs.height, {  format: gl.RG, internalformat: gl.RG });
	this.ssaoConvolutionY = this.createFrameBuffer(raptorjs.width, raptorjs.height,  { format: gl.RG, internalformat: gl.RG });
	
	this.shadowConvolutionX = this.createFrameBuffer(2048, 2048, {  filter : gl.LINEAR, type : gl.FLOAT, format: gl.RG, internalformat: gl.RG  });
	this.shadowConvolutionY = this.createFrameBuffer(2048, 2048, {  filter : gl.LINEAR, type : gl.FLOAT, format: gl.RG, internalformat: gl.RG  });
	
	this.shadowOcclusionFramebuffer = this.createFrameBuffer(raptorjs.width, raptorjs.height, { format: gl.RG, internalformat: gl.RG  });
	
	this.diffuseAcc = this.createFrameBuffer(raptorjs.width, raptorjs.height);
	this.colorInfoFramebuffer = this.createFrameBuffer(raptorjs.width, raptorjs.height, { });
	//this.prevDiffuseFramebuffer  = this.createFrameBuffer(raptorjs.width, raptorjs.height);
	// this.ssgiFramebuffer = this.createFrameBuffer(raptorjs.width/2, raptorjs.height, { });
}


/**
 * Create texture to render the shadow depth to.
 */
raptorjs.renderSystem.prototype.createShadowSurface = function() {
	var width = 2048;
	var height = 2048;
	
	this.shadowFramebuffer = this.createFrameBuffer(width, height,  { filter : gl.LINEAR,  format: gl.RGBA, internalformat: gl.RGBA	});//, filter : gl.LINEAR  type : gl.FLOAT 
	
	this.shadowTexture = raptorjs.createObject('texture');

	this.shadowTexture.data = this.shadowFramebuffer.texture;
	this.shadowTexture.dataType = 'framebuffer';
	this.shadowTexture.width = this.shadowFramebuffer.width;
	this.shadowTexture.height = this.shadowFramebuffer.height;
	
	this.shadowSampler = raptorjs.createObject("sampler2D");
	this.shadowSampler.texture = this.shadowTexture;
	this.shadowSampler.format = gl.RGBA;
	this.shadowSampler.internalformat = gl.RGBA;
	this.shadowSampler.anisotropic = 16;
	// this.shadowSampler.MIN_FILTER = gl.NEAREST;
	// this.shadowSampler.MAG_FILTER = gl.NEAREST;
};


/**
 * set scene
 * @param {(scene)}
**/
raptorjs.renderSystem.prototype.setScene = function(scene) {
	this.activeScene = scene;
}


/**
 * create deferred buffers
**/
raptorjs.renderSystem.prototype.createDeferredBuffers = function() {
	var plane = raptorjs.primitives.createPlane(2, 2, 1, 1);
	
	this.quadVertices = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, this.quadVertices);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(plane.vertexBuffer.data), gl.STATIC_DRAW);
	this.quadVertices.name = 'position';
	this.quadVertices.itemSize = 3;
	this.quadVertices.numItems = plane.vertexBuffer.data.length / 3;
	
	this.quadUv = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, this.quadUv);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(plane.uvBuffer.data), gl.STATIC_DRAW);
	this.quadUv.name = 'uv';
	this.quadUv.itemSize = 2;
	this.quadUv.numItems = plane.uvBuffer.data.length / 2;

	
	this.quadIndices = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.quadIndices);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(plane.indexBuffer.data), gl.STATIC_DRAW);
	this.quadIndices.name = 'index';
	this.quadIndices.itemSize = 1;
	this.quadIndices.numItems = plane.indexBuffer.data.length;
}


/**
 * create buffer
 * @param {(int)} itemSize
 * @param {(array)} dataArray
 * @param {(string)} name
 * @param {(boolean)} isFloatType
**/
raptorjs.renderSystem.prototype.createBuffer = function( itemSize, dataArray, name, isFloatType ) {
	var buffer = gl.createBuffer();

	var dataType = (isFloat(dataArray, false))?"float":"int";
	
	if(isFloatType) {
		gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(dataArray), gl.STATIC_DRAW);
	} else {
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(dataArray), gl.STATIC_DRAW);
	}
	
	buffer.name = name;
	buffer.itemSize = itemSize;
	buffer.numItems = dataArray.length / itemSize;
	buffer.dataType = dataType;

	return buffer;
}


/**
 * initialize webgl
 * @param {(Dom canvas)} canvas
**/
raptorjs.renderSystem.prototype.initializeWebgl = function(canvas) {
	try {
	
		var width = canvas.offsetWidth;
		var height = canvas.offsetHeight;
		
		canvas.width = raptorjs.math.nextHighestPowerOfTwo(width);
		canvas.height = raptorjs.math.nextHighestPowerOfTwo(height);
		
		console.log('adjusted resolution to width:', canvas.width , 'height', canvas.height);
		
		raptorjs.canvas = canvas;
		
		raptorjs.setLoadingText("Init Webgl");
		
		gl = canvas.getContext("webgl");//canvas.getContext("experimental-webgl",  {alpha: false});
		
		raptorjs.extensions.textureFloat = gl.getExtension('OES_texture_float');
		raptorjs.extensions.textureHalf = gl.getExtension('OES_texture_half_float');
		raptorjs.extensions.elementIndexUint = gl.getExtension('OES_element_index_uint'); 
		raptorjs.extensions.derivatives = gl.getExtension('OES_standard_derivatives');
		raptorjs.extensions.texture3d = gl.getExtension("OES_texture_3D");
		raptorjs.extensions.depthTexture = gl.getExtension("WEBKIT_WEBGL_depth_texture");
		raptorjs.extensions.anisotropic = gl.getExtension("WEBKIT_EXT_texture_filter_anisotropic");
		raptorjs.extensions.textureCompression = gl.getExtension("WEBKIT_WEBGL_compressed_texture_s3tc");
		raptorjs.extensions.OES_half_float_linear = gl.getExtension("OES_half_float_linear");
		raptorjs.extensions.WEBGL_draw_buffers = gl.getExtension("WEBGL_draw_buffers");
		
		
		
		raptorjs.setLoadingText("Init RaptorEngine");
		
		if(raptorjs.extensions.elementIndexUint)
			this.indexType = gl.UNSIGNED_INT;
		else
			this.indexType = gl.UNSIGNED_SHORT;
		
		
		var formats = gl.getParameter(gl.COMPRESSED_TEXTURE_FORMATS);
		 
		raptorjs.extensions.dxt5Supported = false;
		
		for(var i = 0; i<formats.length; i++) {

			if(formats[i] == raptorjs.extensions.textureCompression.COMPRESSED_RGBA_S3TC_DXT5_EXT) {
				raptorjs.extensions.dxt5Supported = true;
			}
		}
		 
		console.log(raptorjs.extensions, formats);
 		
		gl.viewportWidth = canvas.width;
		gl.viewportHeight = canvas.height;
		
		raptorjs.width = canvas.width;
		raptorjs.height = canvas.height;
		raptorjs.canvas = canvas;

	} catch (e) {  }
	
	if (!gl) {
		alert("Could not initialise WebGL, sorry :-(");
	}
}


/**
 * set graphics library
 * @param {(string)} name
**/
raptorjs.renderSystem.prototype.setGraphicsLibrary = function(name) {
	/*
		
	*/
}


/**
 * Calculate elapsed time and current time
 */
raptorjs.renderSystem.prototype.smoothTimeEvolution = function() {
	if(!raptorjs.firstTime)
		raptorjs.firstTime = new Date().getTime();

	var timeNow = new Date().getTime() - raptorjs.firstTime;
	if (raptorjs.lastTime != 0) {
		var elapsed = timeNow - raptorjs.lastTime;
		raptorjs.elapsed = elapsed / 1000;
		raptorjs.timeNow = timeNow;
	}
	raptorjs.lastTime = timeNow;
}


/**
 * get buffer by name
 * @param {(string)} name
**/
raptorjs.renderSystem.prototype.getBuffersByName = function(name) {
	var buffers = this.buffers;
	var returnbuffersDepth = [];
	
	for(var c = 0; c < buffers.length; c++) {
		var buffer = buffers[c];
		if(buffer.name == name)
			returnbuffersDepth.push(buffer);
	}
	
	return returnbuffersDepth;
}


/**
 * get deferred buffers by name
 * @param {(string)} name
**/
raptorjs.renderSystem.prototype.getDeferredBuffersByName = function(name) {
	var deferredBuffers = this.deferredBuffers;
	var returnbuffersDepth = [];
	
	for(var c = 0; c < deferredBuffers.length; c++) {
		var buffer = deferredBuffers[c];
		if(buffer.name == name)
			returnbuffersDepth.push(buffer);
	}
	
	return returnbuffersDepth;
}


/**
 * get deferred buffer by name
 * @param {(string)} name
**/
raptorjs.renderSystem.prototype.getDeferredBufferByName = function(name) {
	var deferredBuffers = this.deferredBuffers;
	var returnbuffersDepth = [];
	
	for(var c = 0; c < deferredBuffers.length; c++) {
		var buffer = deferredBuffers[c];
		if(buffer.name == name)
			returnbuffersDepth.push(buffer);
	}
	
	return returnbuffersDepth[0];
}


/**
 * get element
 * @param {(buffer)} buffer
 * @param {(int)} vertexIndex
**/
function getElement(buffer, vertexIndex) {
	var startId = buffer.itemSize * vertexIndex;
	var out = [];
	
	for(var c = 0; c<buffer.itemSize; c++) {
		out.push(buffer.data[c+startId]);
	}

	return out;
}


/**
 * create tangent and binormal vectors
 * @param {(array)} positionArray
 * @param {(array)} normalArray
 * @param {(array)} normalMapUVArray
 * @param {(array)} triangles
**/
raptorjs.renderSystem.prototype.createTangentsAndBinormals = function( positionArray, normalArray, normalMapUVArray, triangles) {

  // Maps from position, normal key to tangent and binormal matrix.
  var tangentFrames = {};

  // Rounds a vector to integer components.
  function roundVector(v) {
    return [Math.round(v[0]), Math.round(v[1]), Math.round(v[2])];
  }

  // Generates a key for the tangentFrames map from a position and normal
  // vector. Rounds position and normal to allow some tolerance.
  function tangentFrameKey(position, normal) {
    return roundVector(raptorjs.vector3.scale(position, 100)) + ',' +
        roundVector(raptorjs.vector3.scale(normal, 100));
  }

  // Accumulates into the tangent and binormal matrix at the approximate
  // position and normal.
  function addTangentFrame(position, normal, tangent, binormal) {
    var key = tangentFrameKey(position, normal);
    var frame = tangentFrames[key];
    if (!frame) {
      frame = [[0, 0, 0], [0, 0, 0]];
    }
    frame[0] = raptorjs.vector3.add(frame[0], tangent);
    frame[1] = raptorjs.vector3.add(frame[1], binormal);
    tangentFrames[key] = frame;
  }

  // Get the tangent and binormal matrix at the approximate position and
  // normal.
  function getTangentFrame(position, normal) {
    var key = tangentFrameKey(position, normal);
    return tangentFrames[key];
  }

  var numTriangles = triangles.numItems;
  for (var triangleIndex = 0; triangleIndex < numTriangles; ++triangleIndex) {
    // Get the vertex indices, uvs and positions for the triangle.
    var vertexIndices = getElement(triangles, triangleIndex);
    var uvs = [];
    var positions = [];
    var normals = [];
    for (var i = 0; i < 3; ++i) {
      var vertexIndex = vertexIndices[i];
      uvs[i] = getElement(normalMapUVArray, vertexIndex);
      positions[i] = getElement(positionArray,vertexIndex);
      normals[i] = getElement(normalArray,vertexIndex);
	 
		//if(triangleIndex == 0)
		//	console.log(uvs, vertexIndex, vertexIndices, triangleIndex, triangles);
		 
    }
	


    // Calculate the tangent and binormal for the triangle using method
    // described in Maya documentation appendix A: tangent and binormal
    // vectors.
    var tangent = [0, 0, 0];
    var binormal = [0, 0, 0];
	
	
    for (var axis = 0; axis < 3; ++axis) {
      var edge1 = [positions[1][axis] - positions[0][axis],
                   uvs[1][0] - uvs[0][0], uvs[1][1] - uvs[0][1]];
      var edge2 = [positions[2][axis] - positions[0][axis],
                   uvs[2][0] - uvs[0][0], uvs[2][1] - uvs[0][1]];
      var edgeCross = raptorjs.vector3.normalize(raptorjs.vector3.cross(edge1, edge2));
      if (edgeCross[0] == 0) {
        edgeCross[0] = 1;
      }
      tangent[axis] = -edgeCross[1] / edgeCross[0];
      binormal[axis] = -edgeCross[2] / edgeCross[0];
    }

    // Normalize the tangent and binornmal.
    var tangentLength = raptorjs.vector3.size(tangent);
    if (tangentLength > 0.00001) {
      tangent = raptorjs.vector3.scale(tangent, 1 / tangentLength);
    }
    var binormalLength = raptorjs.vector3.size(binormal);
    if (binormalLength > 0.00001) {
      binormal = raptorjs.vector3.scale(binormal, 1 / binormalLength);
    }

    // Accumulate the tangent and binormal into the tangent frame map.
    for (var i = 0; i < 3; ++i) {
      addTangentFrame(positions[i], normals[i], tangent, binormal);
    }
  }
	
  // Add the tangent and binormal streams.
  var numVertices = positionArray.numItems;
  var tangents = [];
  var binormals = [];

  // Extract the tangent and binormal for each vertex.
  for (var vertexIndex = 0; vertexIndex < numVertices; ++vertexIndex) {
    var position = getElement(positionArray,vertexIndex);
    var normal = getElement(normalArray, vertexIndex);
    var frame = getTangentFrame(position, normal);

    // Orthonormalize the tangent with respect to the normal.
    var tangent = frame[0];
    tangent = raptorjs.vector3.sub(
        tangent, raptorjs.vector3.scale(normal, raptorjs.vector3.dot(normal, tangent)));
    var tangentLength = raptorjs.vector3.size(tangent);
    if (tangentLength > 0.00001) {
      tangent = raptorjs.vector3.scale(tangent, 1 / tangentLength);
    }

    // Orthonormalize the binormal with respect to the normal and the tangent.
    var binormal = frame[1];
    binormal = raptorjs.vector3.sub(
        binormal, raptorjs.vector3.scale(tangent, raptorjs.vector3.dot(tangent, binormal)));
    binormal = raptorjs.vector3.sub(
        binormal, raptorjs.vector3.scale(normal, raptorjs.vector3.dot(normal, binormal)));
    var binormalLength = raptorjs.vector3.size(binormal);
    if (binormalLength > 0.00001) {
      binormal = raptorjs.vector3.scale(binormal, 1 / binormalLength);
    }

    tangents.push(tangent);
    binormals.push(binormal);
  }

  return {
    tangent: tangents,
    binormal: binormals};
	
	
};





raptorjs.renderSystem.prototype.createMultipleRenderTargets = function( ) {
	var properties = {type : gl.FLOAT };
	
	// filter : gl.LINEAR,  format: gl.RGBA, internalformat: gl.RGBA
	this.mtrFramebuffer = this.createFrameBuffer(raptorjs.width, raptorjs.height, [properties, properties, properties] );//, filter : gl.LINEAR  type : gl.FLOAT 
	
	var diffuseTexture = raptorjs.createObject('texture');

	diffuseTexture.data = this.mtrFramebuffer.texture[0];
	diffuseTexture.dataType = 'framebuffer';
	diffuseTexture.width = this.mtrFramebuffer.width;
	diffuseTexture.height = this.mtrFramebuffer.height;
	
	
	var diffuseSampler = raptorjs.createObject("sampler2D");
	diffuseSampler.texture = diffuseTexture;

	var normalTexture = raptorjs.createObject('texture');

	normalTexture.data = this.mtrFramebuffer.texture[1];
	normalTexture.dataType = 'framebuffer';
	normalTexture.width = this.mtrFramebuffer.width;
	normalTexture.height = this.mtrFramebuffer.height;
	
	
	var normalSampler = raptorjs.createObject("sampler2D");
	normalSampler.texture = normalTexture;
	

	
	var infoTexture = raptorjs.createObject('texture');

	infoTexture.data = this.mtrFramebuffer.texture[2];
	infoTexture.dataType = 'framebuffer';
	infoTexture.width = this.mtrFramebuffer.width;
	infoTexture.height = this.mtrFramebuffer.height;
	
	
	var infoSampler = raptorjs.createObject("sampler2D");
	infoSampler.texture = infoTexture;
	
	
	this.deferredShader = raptorjs.createObject("shader");
	this.deferredShader.createFomFile("shaders/deferred.shader");
	this.deferredShader.setUniform("viewProjection", this.quadViewProjection );
	this.deferredShader.setUniform("diffuseSampler", diffuseSampler);
	this.deferredShader.setUniform("normalSampler", normalSampler);
	this.deferredShader.setUniform("infoSampler", infoSampler);


}
	/*

	if (!raptorjs.extensions.WEBGL_draw_buffers) {
		console.log("multiple render targets not supported");
	} else {
		console.log("multiple render targets supported");
		
	}

	
	var diffuseSampler = raptorjs.createObject("sampler2D");
	diffuseSampler.texture = this.mtrColorTexture;
	diffuseSampler.format = gl.RGBA;
	diffuseSampler.internalformat = gl.RGBA;
	
	//diffuseSampler.WRAP_S = gl.CLAMP_TO_EDGE;
	//diffuseSampler.WRAP_T = gl.CLAMP_TO_EDGE;	
	
	
	this.deferredShader = raptorjs.createObject("shader");
	this.deferredShader.createFomFile("shaders/deferred.shader");
	this.deferredShader.setUniform("viewProjection", this.quadViewProjection);
	this.deferredShader.setUniform("texture", diffuseSampler);
}

	this.mtrRenderbuffer = gl.createRenderbuffer();
	this.mtrFramebuffer = gl.createFramebuffer();
	
	this.mtrColorTexture = gl.createTexture();
	this.mtrNormalTexture = gl.createTexture();
	
	gl.bindFramebuffer(gl.FRAMEBUFFER, this.mtrFramebuffer);
	this.mtrFramebuffer.width = 1024;
	this.mtrFramebuffer.height = 1024;
	
	gl.bindTexture(gl.TEXTURE_2D, this.mtrColorTexture);
    //gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    //gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    //gl.generateMipmap(gl.TEXTURE_2D);
	//gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1024, 1024, 0,  gl.RGBA, gl.UNSIGNED_BYTE, null);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1024, 1024, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
	 
	 
	 
	gl.bindTexture(gl.TEXTURE_2D, this.mtrNormalTexture);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    gl.generateMipmap(gl.TEXTURE_2D);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1024, 1024, 0,  gl.RGBA, gl.UNSIGNED_BYTE, null);

	
	gl.bindRenderbuffer(gl.RENDERBUFFER, this.mtrRenderbuffer);
	gl.framebufferTexture2D(gl.FRAMEBUFFER, raptorjs.extensions.WEBGL_draw_buffers.COLOR_ATTACHMENT0_WEBGL, gl.TEXTURE_2D, this.mtrColorTexture, 0);
	gl.framebufferTexture2D(gl.FRAMEBUFFER, raptorjs.extensions.WEBGL_draw_buffers.COLOR_ATTACHMENT1_WEBGL, gl.TEXTURE_2D, this.mtrNormalTexture, 0);
	

	
	gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, 1024, 1024);
	gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this.mtrRenderbuffer);
	
	
	raptorjs.extensions.WEBGL_draw_buffers.drawBuffersWEBGL([
	  raptorjs.extensions.WEBGL_draw_buffers.COLOR_ATTACHMENT0_WEBGL, // gl_FragData[0]
	  raptorjs.extensions.WEBGL_draw_buffers.COLOR_ATTACHMENT1_WEBGL
	]);
	

*/

/**
 * Create framebuffer
 * @param {(int)} width
 * @param {(int)} height
 * @param {(object)} properties
**/
raptorjs.renderSystem.prototype.createFrameBuffer = function(width, height, properties) { //type, depth
	
	if(Object.prototype.toString.call( properties ) === '[object Array]') {

	} else {
		
		if(!properties)
			var properties = {};
		
		properties = [properties];
		
	}
	
	var textures = [];
	var attachments = [];
	
	
	var WEBGL_draw_buffers = raptorjs.extensions.WEBGL_draw_buffers;
	
	
	var framebuffer = gl.createFramebuffer();

	framebuffer.width = width;
	framebuffer.height = height;
	
	gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);

	for(var c = 0; c<properties.length; c++) {
		
		var currentProperties = properties[c];
		var texture = gl.createTexture();

		if(properties.length.length == 1) {
			var attachment = gl.COLOR_ATTACHMENT0;
		} else {
			switch(c) {
				case 0:
					var attachment = WEBGL_draw_buffers.COLOR_ATTACHMENT0_WEBGL
				break;
				case 1: 
					var attachment = WEBGL_draw_buffers.COLOR_ATTACHMENT1_WEBGL
				break;
				case 2: 
					var attachment = WEBGL_draw_buffers.COLOR_ATTACHMENT2_WEBGL
				break;
				case 3: 
					var attachment = WEBGL_draw_buffers.COLOR_ATTACHMENT3_WEBGL
				break;
			}
		}

		attachments.push(attachment);
		
		gl.bindTexture(gl.TEXTURE_2D, texture);
		
		switch(currentProperties.filter) {
			case gl.NEAREST:
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
			break;
			case gl.LINEAR:
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
			break;
			default:
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		}

		if(currentProperties.anisotropic) {
			//var extension = raptorjs.extensions.anisotropic;
			//gl.texParameteri( gl.TEXTURE_2D, extension.TEXTURE_MAX_ANISOTROPY_EXT, properties.anisotropic );
		}
		
		var renderbuffer = gl.createRenderbuffer();
		
		if(!currentProperties.type)
			currentProperties.type = gl.UNSIGNED_BYTE;
		
		if(!currentProperties.internalformat)
			currentProperties.internalformat = gl.RGBA;

		if(!currentProperties.format)
			currentProperties.format = gl.RGBA;

		
		gl.texImage2D(gl.TEXTURE_2D, 0, currentProperties.internalformat, framebuffer.width, framebuffer.height, 0, currentProperties.format, currentProperties.type, null);
		gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
		
		
		if(currentProperties.type == "depth") {
			//gl.texImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT, framebuffer.width, framebuffer.height, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_SHORT, null);
			gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
			gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, texture, 0);
			gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, framebuffer.width, framebuffer.height);
			//gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);
		} else if(currentProperties.type) {
			
			//if(isMtr) {
				
			//	gl.texImage2D(gl.TEXTURE_2D, 0, properties.internalformat, framebuffer.width, framebuffer.height, 0, properties.format, properties.type, null);
			//	gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
			//	gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);
			//	gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, framebuffer.width, framebuffer.height);
			//	gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);
				
			//} else {
				
				
				gl.framebufferTexture2D(gl.FRAMEBUFFER, attachment, gl.TEXTURE_2D, texture, 0);
				gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, framebuffer.width, framebuffer.height);
				gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);
			//}
			
		}
		
		texture.attachment = attachment;
		textures.push(texture);
		
		gl.bindRenderbuffer(gl.RENDERBUFFER, null);
		gl.bindTexture(gl.TEXTURE_2D, null);
		
		
		
	}
	

	
	raptorjs.extensions.WEBGL_draw_buffers.drawBuffersWEBGL(attachments);
	
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);

	framebuffer.type = 'float';
	
	if(textures.length == 1) {
		framebuffer.texture = textures[0];
	} else {
		framebuffer.texture = textures;
	}
	
	framebuffer._className = "framebuffer";

	return framebuffer;
}

/*
raptorjs.renderSystem.prototype.createFrameBuffer = function(width, height, type, depth) {
	var framebuffer = gl.createFramebuffer();
	gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
	
	framebuffer.width = width;
	framebuffer.height = height;

	var texture = gl.createTexture();
	
	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

	if(type == "depth")
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT, framebuffer.width, framebuffer.height, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_SHORT, null);
	else if(type)
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, framebuffer.width, framebuffer.height, 0, gl.RGBA, type, null);
	else
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, framebuffer.width, framebuffer.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

	
	var renderbuffer = gl.createRenderbuffer();
	
	gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
	
	gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);
	gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, framebuffer.width, framebuffer.height);
	gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);
	

	gl.bindTexture(gl.TEXTURE_2D, null);
	gl.bindRenderbuffer(gl.RENDERBUFFER, null);
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	
	framebuffer.texture = texture;
	framebuffer._className = "framebuffer";
	
	return framebuffer;
} */


/**
 * Create sampler
**/
raptorjs.renderSystem.prototype.createSampler = function() {
	
}


/**
 * add scene
 * @param {(scene)} scene
**/
raptorjs.renderSystem.prototype.addScene = function(scene) {
	this.sceneNodes.push(scene);
}


/**
 * set camera
 * @param {(camera)} camera
**/
raptorjs.renderSystem.prototype.setCamera = function(camera) {
	this.activeCamera = camera;
}



/**
 * Setup shadow camera, Create basic view and renderpass. pssm 
 * @param {vector3} eye
 * @param {vector3} target
 * @param {vector3} up
 */
raptorjs.renderSystem.prototype.createShadowMaps = function(  ) {
	this.shadow = true;
	var camera = raptorjs.mainCamera;
	
	this.calculateSplitPoints(this.numberOfSplitpoints, camera.near, camera.far, this.lambda);
	
	
	var shadowMap = {};
	shadowMap.width = 1024;
	shadowMap.height = 1024;
	shadowMap.nearSplitPoint = this.splitPoints[this.shadowMaps.length];
	shadowMap.farSplitPoint = this.splitPoints[this.shadowMaps.length+1];

	this.shadowMaps.push( shadowMap );
}


/**
 * Calculate splitpoints for the near and far depth value's for the shadow sub frusta
 */
raptorjs.renderSystem.prototype.calculateSplitPoints = function(splitCount, nearDist, farDist, lambda) {
	if (splitCount < 2)
		console.log("Cannot specify less than 2 splits", "PSSMShadowCameraSetup::calculateSplitPoints");
		
	this.splitPoints = [];
	this.mOptimalAdjustFactors = new Array(splitCount);

	mSplitCount = splitCount;

	this.splitPoints[0] = nearDist;
	for (var i = 1; i < mSplitCount; i++)
	{
		var fraction = i / splitCount;
		var splitPoint = ( lambda * nearDist * Math.pow(farDist / nearDist, fraction) +
				(1.0 - lambda) * (nearDist + fraction * (farDist - nearDist)) );

		this.splitPoints[i] = splitPoint;
	}

	this.splitPoints[splitCount] = farDist;
	console.log(this.splitPoints);
	
}


/**
 * Update shadow pssm
 */
raptorjs.renderSystem.prototype.updateShadowsPSSM = function( shadowMap, lightDir ) {

	var matrix4 = raptorjs.matrix4;
	var camera = raptorjs.mainCamera;

	var SplitProjection = matrix4.perspective( 	raptorjs.math.degToRad(camera.fov),  
												raptorjs.width /  raptorjs.height, 
												.1, 
												1000 );
	
	var SplitView = camera.view;
	var SplitMatrix = matrix4.mul( SplitView, SplitProjection );

	var InverseViewProjection = matrix4.inverse(SplitMatrix);
	
	var points = [];

	var vertices = [[-1,1,-1],[1,1,-1],  [1,-1,-1],[-1,-1,-1],  //near
						  [-1,1,1],[1,1,1],[1,-1,1], [-1,-1,1]      ];
					 
	lightDir = raptorjs.vector3.normalize( lightDir );
	
	var lightRotation  =  matrix4.lookAt(	[ 0, 0, 0 ], 
											lightDir, 
											[ 0, 1, 0 ] );
													
	for(var c =0;c<vertices.length; c++) {
		var vert = vertices[c];
		
		var worldPosition = matrix4.transformPoint(InverseViewProjection, vert);
		

		points.push(matrix4.transformPoint(lightRotation, worldPosition));
	}
	

	var boundingBox = raptorjs.createObject('boundingBox');
	
	boundingBox.fitBoxToPoints_(points);
	
	var boxSize = raptorjs.vector3.sub(boundingBox.maxExtent, boundingBox.minExtent);
	var halfBoxSize = raptorjs.vector3.scale(boxSize, 0.5);
		
	var lightPosition = raptorjs.vector3.add(boundingBox.minExtent, halfBoxSize);

	lightPosition[2] = boundingBox.minExtent[2];//*3
	lightPosition = matrix4.transformPoint(matrix4.inverse(lightRotation),lightPosition); 

	var shadowEye = raptorjs.vector3.add(lightPosition, raptorjs.vector3.scale(lightDir, 1200) );
	
	var lightView = matrix4.lookAt(	shadowEye, //raptorjs.vector3.scale(lightDir, this.pssmOffset) 
									raptorjs.vector3.sub(lightPosition, lightDir), 
									[0, 0, 1]);

	var lightProjection = raptorjs.matrix4.orthographic(-1500, 1500, -1500, 1500, -2000, 3000);
	
	var lightViewProjection = matrix4.mul( lightView, lightProjection  );


	var shadowInfo = {};
	shadowInfo.eye = shadowEye;
	shadowInfo.view = lightView;
	shadowInfo.projection = lightProjection;
	shadowInfo.SplitProjection = SplitMatrix;
	shadowInfo.viewProjection = lightViewProjection;


	if(testest == 60) {
		console.log(shadowInfo);
	}
		
	return shadowInfo;
}


/**
 * Create Orthographic matrix
 * @param {int} width
 * @param {int} height
 * @param {int} near
 * @param {int} far
 * @param {int} offset
 */
function createOrthographic(width, height, near, far, offset) {
	var depth = far - near;
	var faddn = far + near;
	depth += offset;
	
	var x     =  2.0 / width;
	var y     =  2.0 / height;
	var z     =  -2.0 / depth;
	var zt    = -faddn / depth;

    return 	[  	[x, 0.0, 0.0, 0.0],
				[0.0, y, 0.0, 0.0],
				[0.0, 0.0, z, zt],
				[0.0, 0.0, 0.0, 1.0] ];
}


/**
 * isFloat
 * @param {array} array
 * @param {int} isfloat
 */
function isFloat(array, isfloat) {
	for(var c = 0; c < array.length; c++) 
	{
		var child = array[c];
		if(typeof child == "object") 
		{
			getFirstvalue(child, isfloat);
		} else {
			if(isFloatCheck(child))
				return true;
		}
	}
	return isfloat;
}


/**
 * get first value
 * @param {array} array
 * @param {int} isInt
 */
function getFirstvalue (array, isInt) {
	if(typeof array == "object") {
			return getFirstvalue(array[0]);
	} else {
		return array;
	}
}


/**
 * check if object is integer
 * @param {object} 
 */
function isInteger(f) {
    return typeof(f)==="number" && Math.round(f) == f;
}


/**
 * check if is float
 * @param {object} 
 */
function isFloatCheck(f) { 
	return typeof(f)==="number" && !isInteger(f); 
}


var testest = 0;

raptorjs.firstTime = false;