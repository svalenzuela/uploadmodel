/*
 * Copyright 2013, Raptorcode Studios Inc, 
 * Author, Kaj Dijkstra.
 * All rights reserved.
 *
 */

 
/**
 * Mesh object
**/
raptorjs.mesh = function() {
	this._className = 'mesh';
	this.fileName;
	this.name = "new";
	
	this.shaderName;
	
	this.vertexIndexBuffer;
	this.vertexPositionBuffer;
	this.vertexNormalBuffer;
	this.textureCoordBuffer;
	this.binormalBuffer = false;
	this.tangentBuffer = false;
	
	this.materials = [];
	this.subMeshes = [];
	
	this.jsonStruture;
	this.shader;
	this.forwardShader;
	
	this.renderType = "indexed";
	
	this.materialName;
	this.materialId = 1.0;
	this.useParallax = false;
	
	this.colorInfoShader;
	this.infoShader;
	
	this.materialName;
	this.mode = gl.TRIANGLES;
	
	//this.customShader = raptorjs.createObject("shader");
	//this.customShader.createLibraryFomFile("shaders/custom.default.shader");

	this.heightmap;

	this.useNormal = false;
};


/**
 * Set custom shader (Not yet in use)
 * @param {(String)} name
**/
raptorjs.mesh.prototype.setCustomShader = function( name ) {
	this.customShader = raptorjs.createObject("shader");
	this.customShader.createLibraryFomFile("shaders/custom."+name+".shader");
}


/**
 * Set custom shader (Not yet in use)
 * @param {(String)} name
**/
raptorjs.mesh.prototype.addHeightmap = function( heightmap ) {
	this.heightmap = heightmap;
	
	this.colorInfoShader.setUniform("heightmap", heightmap );
	this.infoShader.setUniform("heightmap", heightmap );
}


/**
 * set material id
 * @param {(int)} material id
**/
raptorjs.mesh.prototype.setMaterialId = function( id ) {
	this.materialId = id;
	//this.colorInfoShader.setUniform("materialId", this.materialId / 256 );
}


/**
 * Load mesh from file
 * @param {(String)} url to file
**/
raptorjs.mesh.prototype.loadMeshFromFile = function( url ) {
	this.jsonStruture = JSON.parse(raptorjs.loadTextFileSynchronous('media/models/'+url));
	this.fileName = url;
	this.parseLoadedMesh();
}


/**
 * Combine displacement and normal data in one texture
 * @param {(Array)} displacement data
 * @param {(Array)} normal data
 * @return {(Sampler)} sampler
**/
raptorjs.mesh.prototype.combineDisplacementNormal = function( displacement, normal ) {

	var displacementImage = displacement.data;
	var normalImage = normal.data;

	var canvas = document.createElement('Canvas');
	var width = displacement.width;
	var height = displacement.height;
	
	canvas.width  = width;
	canvas.height = height;
	
	canvas.getContext('2d').drawImage(displacementImage, 0, 0, width, height);
	var displacementpixelData = canvas.getContext('2d').getImageData(0, 0, width, height).data;
	
	
	canvas.getContext('2d').drawImage(normalImage, 0, 0, width, height);
	var normalpixelData = canvas.getContext('2d').getImageData(0, 0, width, height).data;

	var dataArray = [];

	for(var x = 0; x<width; x++) {
		for(var y = 0; y<height; y++) {
			var index = (x + (y * height) ) * 4;
			
			dataArray.push(normalpixelData[index] / 255);
			dataArray.push(normalpixelData[index+1] / 255);
			dataArray.push(normalpixelData[index+2] / 255);
			dataArray.push(displacementpixelData[index]  / 255);
				
		}
	}
	
	var text = raptorjs.textureFromArray(dataArray, width, height, true);
	var sampler = raptorjs.createObject("sampler2D");
	
	sampler.texture = dataArray;
	
	return sampler;
}


/**
 * Create tangent and binormal vectors (* This is slow!!)
**/
raptorjs.mesh.prototype.createTangentAndBinormal = function( ) {
	var bn = raptorjs.primitives.createTangentsAndBinormals(  this.vertexPositionBuffer,
															  this.vertexNormalBuffer,
															  this.textureCoordBuffer,
														  this.vertexIndexBuffer );

		
	
	if(bn.tangent) {
		
	
		var tangentArray = layoutArray(bn.tangent);							
		var binormalArray = layoutArray(bn.binormal);	
		
		
		

		this.binormalBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.binormalBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(binormalArray), gl.STATIC_DRAW);
		this.binormalBuffer.itemSize = 3;
		this.binormalBuffer.numItems = binormalArray.length / 3;
		this.binormalBuffer.data = binormalArray;
		
		this.tangentBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.tangentBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(tangentArray), gl.STATIC_DRAW);
		this.tangentBuffer.itemSize = 3;
		this.tangentBuffer.numItems = tangentArray.length / 3;
		this.tangentBuffer.data = tangentArray;
	
	}
}

/**
 * set mesh name based on the material types that are used 
 * @param {(MaterialObject)} Material
**/
raptorjs.mesh.prototype.setName = function( material ) {
	var name = this.name;
	
	if(material.normals.length > 0)
		name += "_normal";
		
	if(material.useParallax)
		name += "_parallax";
	
	if(material.textures.length > 0)
		name += "_texture";
		
	if(material.transparencyMaps.length > 0)
		name += "_transparent";
	
	if(material.specularMaps.length > 0)
		name += "_specular";
		
	this.shaderName = name;
}


/**
 * add material to mesh 
 * @param {(MaterialObject)} Material
**/
/*
raptorjs.mesh.prototype.addMaterial = function( material ) {

	this.materials.push(material);
	
	this.colorInfoShader = raptorjs.createObject("shader");

	this.colorInfoShader.definePragma("DEFERRED", (raptorjs.system.deferred) ? 1.0 : 0.0 );	

	this.colorInfoShader.definePragma("NORMAL_MAP", (material.normals.length > 0) ? 1.0 : 0.0 );
	this.colorInfoShader.definePragma("PARALLAX", (material.useParallax) ? 1.0 : 0.0 );
	this.colorInfoShader.definePragma("TEXTURE", (material.textures.length > 0) ? 1.0 : 0.0 );
	this.colorInfoShader.definePragma("TRANSPACENCY_MAP", (material.transparencyMaps.length > 0) ? 1.0 : 0.0 );	
	this.colorInfoShader.definePragma("SPECULAR_MAP", (material.specularMaps.length > 0) ? 1.0 : 0.0 );	

	//this.colorInfoShader.addLibrary(this.customShader, 0);
	//this.colorInfoShader.addLibrary(this.customShader, 1);

	this.setName(material);

	this.colorInfoShader.createFomFile("shaders/info.shader");

	if( material.specularMaps.length > 0 ) {
		this.colorInfoShader.setUniform("specularMapSampler",material.specularMaps[0] );
	}

	if( material.textures.length > 0 ) {
	
		material.textures[0].anisotropic = 4;
		
		this.colorInfoShader.setUniform("texture", material.textures[0] );
		
		if(	material.transparencyMaps.length > 0 ) {
			this.colorInfoShader.setUniform("transparencyMapSampler", material.transparencyMaps[0] );
		}
		
		if(	material.normals.length > 0 ) {
			this.colorInfoShader.setUniform("normalSampler", material.normals[0] );
		}
		
		if(material.useParallax) {
			this.colorInfoShader.setUniform("heightSampler", material.displacementMaps[0]);
		} 
		
	} else {
		var color = material.color;
		this.colorInfoShader.setUniform("rgb", color );
	}
	
	//if(raptorjs.system.deferred) {
		this.infoShader = raptorjs.createObject("shader");
		this.infoShader.definePragma("NORMAL_MAP", (material.normals.length > 0) ? 1.0 : 0.0 );
		this.infoShader.createFomFile("shaders/info.shader");
	//}
	
	this.colorInfoShader.setUniform("v_fresnel", .2);
	this.colorInfoShader.setUniform("v_roughness", .5);
	
	this.colorInfoShader.setUniform("sunColorR", 255/256, true); 
	this.colorInfoShader.setUniform("sunColorG", 243/256, true);
	this.colorInfoShader.setUniform("sunColorB", 163/256, true);
	

	this.shadow_kernel = [];

	this.shadow_kernel[0] = [-0.556641,-0.037109,-0.654297, 0.111328]; 
	this.shadow_kernel[1] = [0.173828,0.111328,0.064453, -0.359375]; 
	this.shadow_kernel[2] = [0.001953,0.082031,-0.060547, 0.078125]; 
	this.shadow_kernel[3] = [0.220703,-0.359375,-0.062500, 0.001953];
	this.shadow_kernel[4] = [0.242188,0.126953,-0.250000, -0.140625]; 
	this.shadow_kernel[5] = [0.070313,-0.025391,0.148438, 0.082031]; 
	this.shadow_kernel[6] = [-0.078125,0.013672,-0.314453, 0.013672];
	this.shadow_kernel[7] = [0.117188,-0.140625,-0.199219, 0.117188];
		
	// if forward shading
	this.colorInfoShader.setUniform("randomSampler",  raptorjs.system.randomSampler);
	this.colorInfoShader.setUniform("randomRotationSampler", raptorjs.system.randomRotationSampler );
	
	console.log( raptorjs.system.randomRotationSampler );
	
	this.colorInfoShader.setUniform("far", raptorjs.mainCamera.far );
	this.colorInfoShader.setUniform("screenWidth", raptorjs.width );
	this.colorInfoShader.setUniform("screenHeight", raptorjs.height );
	this.colorInfoShader.setUniform("shadowBias", 2.0001);
	this.colorInfoShader.setUniform("shadow_kernel", this.shadow_kernel);
	
	if(this.shadowType == "VARIANCE") {
		this.colorInfoShader.setUniform("shadowSampler1", raptorjs.system.shadowConvYSampler);
	} else {
		this.colorInfoShader.setUniform("shadowSampler1", raptorjs.system.shadowSampler);
	}
}
*/
raptorjs.mesh.prototype.addMaterial = function( material ) {
	this.material = material;
	
	this.shader = raptorjs.createObject("shader");

	this.shader.definePragma("TEXTURE", (material.textures.length > 0) ? 1.0 : 0.0 );
	this.shader.definePragma("NORMAL_MAP", (material.normals.length > 0) ? 1.0 : 0.0 );
	this.shader.definePragma("ROUGHNESS_MAP", (material.roughnessMaps.length > 0) ? 1.0 : 0.0 );
	this.shader.definePragma("SHADING_MODEL_ID", material.id );
	this.shader.definePragma("PARALLAX", (material.useParallax) ? 1.0 : 0.0 );
	this.shader.definePragma("DEFERRED", 1.0 );
	

	this.shader.createFomFile("shaders/color.shader");
	
	this.updateShader(material, this.shader);
	

	this.forwardShader = raptorjs.createObject("shader");
	
	
	
	this.forwardShader.definePragma("TEXTURE", (material.textures.length > 0) ? 1.0 : 0.0 );
	this.forwardShader.definePragma("NORMAL_MAP", (material.normals.length > 0) ? 1.0 : 0.0 );
	this.forwardShader.definePragma("ROUGHNESS_MAP", (material.roughnessMaps.length > 0) ? 1.0 : 0.0 );
	this.forwardShader.definePragma("SHADING_MODEL_ID", material.shadingModelID );
	this.forwardShader.definePragma("PARALLAX", (material.useParallax) ? 1.0 : 0.0 );
	this.forwardShader.definePragma("DEFERRED", 0 );
	
	this.forwardShader.createFomFile("shaders/color.shader");
	
	this.updateShader(material, this.forwardShader);
	
	//forwardShader
	this.updateMaterial();
}

raptorjs.mesh.prototype.updateShader = function( material, shader ) {

	shader.setUniform("far", raptorjs.mainCamera.far );
	//this.shader = raptorjs.resources.getShader("shaders/color.shader");
	
	if( material.textures.length > 0 ) {
	
		material.textures[0].anisotropic = 4;

		shader.setUniform("diffuseSampler", material.textures[0] );
		
		if(	material.transparencyMaps.length > 0 ) {
			shader.setUniform("transparencyMapSampler", material.transparencyMaps[0] );
		}
		
		if(	material.normals.length > 0 ) {
			shader.setUniform("normalSampler", material.normals[0] );
		}
		
		if( material.displacementMaps.length > 0  ) {
			shader.setUniform("heightSampler", material.displacementMaps[0]);
		} 
		
	} else {
		
		shader.setUniform("diffuseColor", material.diffuseColor );

		//var color = material.color;
		//this.shader.setUniform("rgb", color );
	}
	
	shader.setUniform("roughness", material.roughness );
	shader.setUniform("id", material.id );
	
}

raptorjs.mesh.prototype.updateMaterial = function(  ) {
	
	
	this.shader.setUniform("roughness", this.material.roughness );
	
	this.forwardShader.setUniform("roughness", this.material.roughness );
	this.forwardShader.setUniform("alpha", this.material.alpha );
	console.log("roughness", this.material.roughness,this.shader );
}

/**
 * load object from file >?>
 * @param {(string)} url
**/
raptorjs.mesh.prototype.loadObjFromFile = function( url ) {

	var objText = raptorjs.loadTextFileSynchronous(url);
	var obj = {};
	var vertexMatches = objText.match(/^v( -?\d+(\.\d+)?){3}$/gm);
	if (vertexMatches)
	{
		console.log(obj.vertices = vertexMatches.map(function(vertex)
		{
			var vertices = vertex.split(" ");
			vertices.shift();
			return vertices;
		}));
	}
	
	
}


/**
 * parse mesh, load extra mesh data into buffers
 * @param {(string)} url
**/
raptorjs.mesh.prototype.parseLoadedMesh = function( url ) {

	var subMesh = raptorjs.createObject('subMesh');
	
	if(this.jsonStruture.indices) {
		this.renderType = "indexed";
	
		subMesh.indices = this.jsonStruture.indices;

		this.vertexIndexBuffer = gl.createBuffer();
		
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.vertexIndexBuffer);
		
		if(raptorjs.extensions.elementIndexUint)
			gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(subMesh.indices), gl.STATIC_DRAW);
		else
			gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(subMesh.indices), gl.STATIC_DRAW);
		
		this.vertexIndexBuffer.itemSize = 3;
		this.vertexIndexBuffer.numItems = subMesh.indices.length;
		this.vertexIndexBuffer.data = subMesh.indices;
	} else {
		this.renderType = "array";
	}
	
	subMesh.vertices = this.jsonStruture.positions;
	
	this.vertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(subMesh.vertices), gl.STATIC_DRAW);
	this.vertexPositionBuffer.itemSize = 3;
	this.vertexPositionBuffer.numItems = subMesh.vertices.length / 3;
	this.vertexPositionBuffer.data = subMesh.vertices;
	
	if(this.jsonStruture.uv) {
	
		subMesh.textcoords = this.jsonStruture.uv;
		
		this.textureCoordBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(subMesh.textcoords), gl.STATIC_DRAW);
		this.textureCoordBuffer.itemSize = 2;
		this.textureCoordBuffer.numItems = subMesh.textcoords.length / 2;
		this.textureCoordBuffer.data = subMesh.textcoords;
		
	}
	
	if(this.jsonStruture.normals) {
	
		subMesh.normals = this.jsonStruture.normals;
	
		this.vertexNormalBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexNormalBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(subMesh.normals), gl.STATIC_DRAW);
		this.vertexNormalBuffer.itemSize = 3;
		this.vertexNormalBuffer.numItems = subMesh.normals.length / 3;
		this.vertexNormalBuffer.data = subMesh.normals;
	
	}

	this.subMeshes.push(subMesh);
}


/**
 * Create mesh from arrays
 * @param {(array)} indices
 * @param {(array)} vertices
 * @param {(array)} normals
 * @param {(array)} uvs
 * @param {(array)} tangents
 * @param {(array)} binormals
**/
raptorjs.mesh.prototype.createMeshFromArrays = function( indices, vertices, normals, uvs, tangents, binormals ) {

	//todo: loop trough sub meshes
	var subMesh = raptorjs.createObject('subMesh');
	

	if(indices) {
		this.renderType = "indexed";
	
		subMesh.indices = indices;

		this.vertexIndexBuffer = gl.createBuffer();
		
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.vertexIndexBuffer);
		
		//if(raptorjs.extensions.elementIndexUint)
		//	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(subMesh.indices), gl.STATIC_DRAW);
		//else
			gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

			
		this.vertexIndexBuffer.itemSize = 3;
		this.vertexIndexBuffer.numItems = indices.length;
		this.vertexIndexBuffer.data = indices;
		
	} else {
		this.renderType = "array";
	}

	subMesh.vertices = vertices;

	this.vertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
	this.vertexPositionBuffer.itemSize = 3;
	this.vertexPositionBuffer.numItems = vertices.length / 3;
	this.vertexPositionBuffer.data = vertices;
	
	if(uvs) {
		subMesh.textcoords = uvs;
		
		this.textureCoordBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(uvs), gl.STATIC_DRAW);
		this.textureCoordBuffer.itemSize = 2;
		this.textureCoordBuffer.numItems = uvs.length / 2;
		this.textureCoordBuffer.data = uvs;
	}
	
	
	if(normals) {
		subMesh.normals = normals;
		
		this.vertexNormalBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexNormalBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normals), gl.STATIC_DRAW);
		this.vertexNormalBuffer.itemSize = 3;
		this.vertexNormalBuffer.numItems = normals.length / 3;
		this.vertexNormalBuffer.data = normals;
	}

	
	
	if(tangents) {
		subMesh.tangents = tangents;
		
		this.tangentBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.tangentBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(tangents), gl.STATIC_DRAW);
		this.tangentBuffer.itemSize = 3;
		this.tangentBuffer.numItems = tangents.length / 3;
		this.tangentBuffer.data = tangents;
	} else {
		
		this.createTangentAndBinormal();
		
	}
	
	if(binormals) {
		this.binormalBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.binormalBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(binormals), gl.STATIC_DRAW);
		this.binormalBuffer.itemSize = 3;
		this.binormalBuffer.numItems = binormals.length / 3;
		this.binormalBuffer.data = binormals;
	}
	


	this.subMeshes.push(subMesh);
	

	this.boundingBox = raptorjs.createObject("boundingBox");
	this.boundingBox.fitBoxToPoints_( vertices );
	
}

/**
 * write content to ext console
 * @param {(string)} content
**/
function writeConsole(content) {
	top.consoleRef=window.open('','myconsole',
	'width=350,height=250'
	+',menubar=0'
	+',toolbar=1'
	+',status=0'
	+',scrollbars=1'
	+',resizable=1')
	top.consoleRef.document.writeln(
	'<html><head><title>Console</title></head>'
	+'<body bgcolor=white onLoad="self.focus()">'
	+content
	+'</body></html>'
	)
	top.consoleRef.document.close()
}


/**
 * Show tangent
 * @param {(submesh)} 
**/
raptorjs.mesh.prototype.showTangent = function( subMesh ) {
	writeConsole( this.fileName + ' Tangent ' + JSON.stringify(this.tangentBuffer.data) );
}


/**
 * Show Binormal
 * @param {(submesh)} 
**/
raptorjs.mesh.prototype.showBinormal = function( subMesh ) {
	var tangentArray = this.binormalBuffer.data;
	var a = [];
	for(var c = 0; c<tangentArray.length;c++){
	
		var s = tangentArray[c];

		a[c] = parseFloat(s.toFixed(5));
	}
	
	writeConsole( this.fileName + ' binormal ' + JSON.stringify(a) );
}

/**
 * add submesh to mesh
 * @param {(subMesh)} 
**/
raptorjs.mesh.prototype.addSubMesh = function( subMesh ) {

	this.vertexIndexBuffer = subMesh.indexBuffer;
	this.vertexPositionBuffer = subMesh.vertexBuffer;
	this.vertexNormalBuffer = subMesh.normalBuffer;
	this.textureCoordBuffer = subMesh.uvBuffer;
	this.tangentBuffer = subMesh.tangentBuffer;
	this.binormalBuffer = subMesh.binormalBuffer;
	
	this.subMeshes.push(subMesh);
}


