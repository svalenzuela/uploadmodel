var selected_entitys = false;
	$(document).ready(function(){
		var lightPosition = [0, 0.1, 0];
		var spotAngles = [35, .1];
		var LightDirection = [0,15, 1, 5.5];
		var SSAO = [ 0.15, 0.47, 0.5, 1.2 ];

	$("#lightColor").spectrum({
		color: "#f00"
	});

	$("#lightColor").on('move.spectrum', function(e, tinycolor) {
		
		raptorjs.system.deferredShader.setUniform("LightColor", [tinycolor._r/256, tinycolor._g/256, tinycolor._b/256] );
	});
	
		$("#position_x").change(function(){
		
			lightPosition[0] = $(this).val() ;
			raptorjs.system.deferredShader.setUniform("lightPosition", lightPosition );
			
			var light = raptorjs.system.scene.getEntityByName("$ColladaAutoName$_108");
			light.transform.translateTo(lightPosition[0], lightPosition[1], lightPosition[2]);
		});
		
		$("#position_y").change(function(){
			lightPosition[1] = $(this).val() ;
			raptorjs.system.deferredShader.setUniform("lightPosition", lightPosition );
			
			var light = raptorjs.system.scene.getEntityByName("$ColladaAutoName$_108");
			light.translateTo(lightPosition[0], lightPosition[1], lightPosition[2]);
		});
		
		$("#position_z").change(function(){
			lightPosition[2] = $(this).val() ;
			raptorjs.system.deferredShader.setUniform("lightPosition", lightPosition );
			
			var light = raptorjs.system.scene.getEntityByName("$ColladaAutoName$_108");
			
			light.translateTo(lightPosition[0], lightPosition[1], lightPosition[2]);
		});
		
		$("#spotAngleX").change(function(){
			spotAngles[0] = $(this).val() ;
			raptorjs.system.deferredShader.setUniform("SpotAngles", spotAngles );
		});
		
		$("#spotAngleY").change(function(){
		
			spotAngles[1] = $(this).val() ;
			raptorjs.system.deferredShader.setUniform("SpotAngles", spotAngles );
		});
		
		$("#LightDirectionX").change(function(){
		
			LightDirection[0] = $(this).val() ;
			raptorjs.system.deferredShader.setUniform("LightDirection", LightDirection );
		});
		
		$("#LightDirectionY").change(function(){
		
			LightDirection[1] = $(this).val() ;
			raptorjs.system.deferredShader.setUniform("LightDirection", LightDirection );
		});
		
		$("#LightDirectionZ").change(function(){
		
			LightDirection[2] = $(this).val() ;
			raptorjs.system.deferredShader.setUniform("LightDirection", LightDirection );
		});
		
		$("#SSAOX").change(function(){
		
			SSAO[0] = $(this).val() ;
			raptorjs.system.ambientocclusionShader.setUniform("parameters", SSAO );
			
		});
		
		$("#SSAOY").change(function(){
		
			SSAO[1] = $(this).val() ;
			raptorjs.system.ambientocclusionShader.setUniform("parameters", SSAO );
			
		});
		
		$("#SSAOZ").change(function(){
		
			SSAO[2] = $(this).val() ;
			raptorjs.system.ambientocclusionShader.setUniform("parameters", SSAO );
			
		});
		
		$("#SSAOW").change(function(){
		
			SSAO[3] = $(this).val() ;
			raptorjs.system.ambientocclusionShader.setUniform("parameters", SSAO );
			
		});

   var layout = [{
                type: 'layoutGroup',
                orientation: 'horizontal',
	
                items: [ 
				


					{
                    type: 'layoutGroup',
                    orientation: 'vertical',
                    width: $(document).width()-250,
                    items: [{
								type: 'documentGroup',
								height: $(document).height(),
								minHeight: 200,
								items: [{
									type: 'documentPanel',
									title: 'Document 1',
									contentContainer: 'Document1Panel'
								}]
							}
					]
                }, {
                    type: 'layoutGroup',
                    width: 250,
                    height: $(document).height(),
                    items: [{
                        type: 'documentGroup',
                        height: 500,
                        minHeight: 200,
                        items: [{
                            type: 'documentPanel',
                            title: 'World Outline',
                            contentContainer: 'SolutionExplorerPanel'
                        }]
                    }, {
                        type: 'documentGroup',
                        height: $(document).height()-530,
                        minHeight: 200,
                        items: [{
							type: 'documentPanel',
							title: 'Details',
							contentContainer: 'DetailsPanel'
                        }]
					
					

                    }]
                }]
            }];
						
			$("#jqxMenu").jqxMenu({ width: $(document).width(), height: '30px', theme: 'dark', animationShowDuration: 0, animationHideDuration: 0, animationShowDelay: 0, autoOpen: false});
			$('#jqxDockingLayout').jqxDockingLayout({ width: $(document).width(), height: $(document).height() - 30, layout: layout, theme: 'dark' });
			
			var form = document.getElementById('file-form');
			var fileSelect = document.getElementById('file-select');
			var uploadButton = document.getElementById('upload-button');
				
				
				
			/*form.onsubmit = function(event) {
			  event.preventDefault();

			  // Update button text.
			  uploadButton.innerHTML = 'Uploading...';

			  // The rest of the code will go here...
			  var files = fileSelect.files;
			  var formData = new FormData();
			  
				for (var i = 0; i < files.length; i++) {
				  var file = files[i];

				  // Check the file type.
				  if (!file.type.match('image.*')) {
					continue;
				  }

				  // Add the file to the request.
				  formData.append('files[]', file, file.name);
				}
				
				var xhr = new XMLHttpRequest();
				
				// Open the connection.
				xhr.open('POST', 'upload.asp', true);
				
				
				// Set up a handler for when the request finishes.
				xhr.onload = function () {
				  if (xhr.status === 200) {
					// File(s) uploaded.
					uploadButton.innerHTML = 'Upload';
				  } else {
					alert('An error occurred!');
				  }
				};
				
				
				// Send the Data.
				xhr.send(formData);
			}*/
			$("#UploadModel").change(function () {
			    var data = new FormData();
			    var files = $("#UploadModel").get(0).files;
			    if (files.length > 0) {
			        data.append("Model", files[0]);
			    }

			    $.ajax({
			        // url: "Controller/ActionMethod"
			        url: "/Home/UploadFile",
			        type: "POST",
			        processData: false,
			        contentType: false,
			        data: data,
			        success: function (response)
			        {
			            //code after success
			            raptorjs.assimpLoader.loadData(response);
			            raptorjs.mainCamera.update();
						raptorjs.mainPlayer.update();
						raptorjs.assimpLoader.buildModels();

						raptorjs.system.ready = true;			            
			        },
			        error: function (er) {
			            alert(er);
			        }

			    });
			});

				
	});

		
		function loadDetails(entity) {
			
			var transform = entity.transform;

			console.log( entity.name );
			
			var groups = [{ label: "Transform",
							type: "group",
							entity: entity,
							items: [{
										label: "Location",
										type: "vector",
										value: transform.position,
										callback: function(value, entity) {
											
											entity.transform.translateTo(value[0], value[1], value[2]);
											//createBounding( entity );
											var position = entity.transform.position;

											var box = raptorjs.system.scene.getEntityByName("boundigBox_entity");
											box.transform.translateTo( position[0], position[1], position[2]  );

										}
									},{

										label: "Rotation",
										type: "vector",
										value: transform.rotation,
										callback: function(value, entity) {
											//console.log(entity.transform);
											console.log("asd", value[0], value[1], value[2]);
												entity.transform.rotateTo(value[0], value[1], value[2]);

												//var boundigBox_entity = raptorjs.system.scene.getEntityByName("boundigBox_entity");
												//createBounding( entity );
												//boundigBox_entity.transform.translateTo( entity.transform.position );
												
											}
										
									},{

										label: "Scale",
										type: "vector",
										value: [transform.scale[0] * 100, transform.scale[1] * 100, transform.scale[2] * 100],
										callback: function(value, entity) {
										//console.log(entity.transform);
											entity.transform.scaleXYZ(value[0]/100, value[1]/100, value[2]/100);
										}
									}]
							}];
							
			$("#details").empty();
				
			parseDetails(entity, groups);
							
			switch(entity.type){
				case "Actor":
					var items = [];
					
					
					
					
					if(entity.mesh.material.textures.length > 0) {
						var url = entity.mesh.material.textures[0].texture.url;
					
					} else {
						var url = "none";
					}
						var base = 	{
										label: "Base Color",
										type: "image",
										value: [url],
										callback: function(value, entity) {
										
											raptorjs.resources.finishCallback = function() {
													console.log("HIEr komt ook");
													var diffuseTexture = raptorjs.resources.getTexture(value);
													var diffuseSampler = raptorjs.createObject("sampler2D");
													
													diffuseSampler.addTexture(diffuseTexture);
								
													entity.mesh.material.addTexture(diffuseSampler);
													
													entity.mesh.addMaterial(entity.mesh.material);

													loadDetails(entity);
													
													console.log(diffuseTexture, value);
													
												}
												raptorjs.resources.addTexture("media/textures/sponza/" + value, value, true);
												
												
										}
									};
										
										
						items.push(base);	
					
					
					
					
					
					
					
					var roughness = {
										label: "Roughness",
										type: "vector",
										value: [entity.mesh.material.roughness],
										callback: function(value, entity) {
											entity.mesh.material.roughness = value[0];
											entity.mesh.updateMaterial();
										}
									};
					var metallic = {
										label: "Metallic",
										type: "vector",
										value: [entity.mesh.material.metallic],
										callback: function(value, entity) {
											entity.mesh.material.metallic = value[0];
											raptorjs.system.deferredShader.setUniform("metallic", value[0]);
											
											entity.mesh.updateMaterial();
										}
									};
									
									
					var specular = {
										label: "Specular",
										type: "vector",
										value: [entity.mesh.material.specular],
										callback: function(value, entity) {
											entity.mesh.material.specular = value[0];
											entity.mesh.updateMaterial();
										}
									};

					var alpha = {
										label: "Alpha",
										type: "vector",
										value: [entity.mesh.material.alpha],
										callback: function(value, entity) {
											entity.mesh.material.alpha = value[0];
											entity.mesh.updateMaterial();
										}
									};		
									
					items.push(metallic);
					items.push(specular);					
					items.push(roughness);		
					
					
					
					

					
					
					
					
					
					
					if(entity.mesh.material.normals.length > 0) {
						var normalUrl =  entity.mesh.material.normals[0].texture.url;
					} else {
						var normalUrl =  "None";
					}
						var base = {
										label: "Normal",
										type: "image",
										value: [normalUrl],
										callback: function(value, entity) {
											raptorjs.resources.finishCallback = function() {
													console.log("HIEr komt ook");
													var diffuseTexture = raptorjs.resources.getTexture(value);
													var diffuseSampler = raptorjs.createObject("sampler2D");
													
													diffuseSampler.addTexture(diffuseTexture);
								
													entity.mesh.material.addNormal(diffuseSampler);
													entity.mesh.addMaterial(entity.mesh.material);
													//entity.mesh.updateShader(entity.mesh.material, entity.mesh.shader);

												}
												
												raptorjs.resources.addTexture("media/textures/sponza/" + value, value, true);
												
												loadDetails(entity);
		
										}
									};
										
										
						items.push(base);	
					
				
					

					items.push(alpha);
					
				
					var materialGroup = [{
						label: "Material",
						type: "group",
						entity: entity,
						items: items
					
						
					}];
					
					
					parseDetails(entity, materialGroup);
					
					
				break;
				
				case "PointLight":
					var items = [];
				
					var attenuation = {
										label: "Attenuation",
										type: "vector",
										value: [entity.attenuation],
										callback: function(value, entity) {
											entity.attenuation = value[0];

										}
									};
				
					items.push(attenuation);
					
					var sourceRadius = {
										label: "sourceRadius",
										type: "vector",
										value: [entity.sourceRadius],
										callback: function(value, entity) {
											entity.sourceRadius = value[0];

										}
									};
				
					items.push(sourceRadius);

					
					var sourceLength = {
										label: "sourceLength",
										type: "vector",
										value: [entity.sourceLength],
										callback: function(value, entity) {
											entity.sourceLength = value[0];

										}
									};
				
					items.push(sourceLength);
					

					
					var materialGroup = [{
						label: "Lightning properties",
						type: "group",
						entity: entity,
						items: items
					
						
					}];
					
					
					
					
					parseDetails(entity, materialGroup);
					
				break;
				
			}				
							
							
							
			

	
			//
			
			
			//switch(entity.type){
			
			//}
		
		}
		
		function parseDetails(entity, groups) {
			var collapseBox = document.createElement("div");	
	
			// create details menu
			for(var c = 0; c<groups.length; c++) {
				var current_group = groups[c];
				var items = current_group.items;
				
				var header = document.createElement("div");
				
				$(header).html(current_group.label);
				
				$(collapseBox).append(header);
				
				var content = document.createElement("div");
				
				for(var b = 0; b<items.length; b++) {
					var current_item = items[b];
					
					var row = document.createElement("div");
					$(row).addClass("row");
					
					var label = document.createElement("div");
					$(label).addClass("label").text(current_item.label);
					
					var values = document.createElement("div");
					
					$(values).addClass("values");
					
					var value = current_item.value;
					
					console.log( "2",  entity.name );
					
					switch(current_item.type){
						case "vector":
							for(var d = 0; d<value.length; d++) {
								
								
								console.log( "3",  entity.name );

								selected_entitys = [entity];
								
								if(value.length == 1) {
									var input1 = document.createElement("div");
									
									$(input1).jqxSlider({
										showTickLabels: true, tooltip: true, mode: "fixed", height: 60, width: 220, min: 0, max: 1, ticksFrequency: .05, value: value[0], step: .05,
										tickLabelFormatFunction: function (value)
										{
											
											//current_item.callback([value], selected_entitys[0]);
											
											if (value == 0) return value;
											if (value == 255) return value;
											return "";
										}
									});
									
								 $(input1).on('change', function (event) {
								   var value = $(this).jqxSlider('value');
								   
											var entity = $(this).data("entity");
										console.log( "3",  entity.name );
										$(this).data("callback")([value], entity);
								}).data({"callback": current_item.callback, "entity" : entity});
									
				
									
								} else {
								
									var input1 = document.createElement("input");
									$(input1).addClass("single_input").val(value[d]);
								
									$(input1).change(function(){
										var parentObject = $(this).parent(".values");
										var inputFields = parentObject.children(".single_input");
										var inputValues = [];
										
										inputFields.each(function(){
											inputValues.push(parseFloat($(this).val()));
										});
											
										var entity = $(this).data("entity");
										console.log( "3",  entity.name );
										$(this).data("callback")(inputValues, entity);
									
									}).data({"callback": current_item.callback, "entity" : entity});
								}
								
								$(values).append(input1);
	
								
							}
							
							
							
							
						break;
						
						case "image":
							
							var img = document.createElement("img");
							
							$(img).attr("src", current_item.value[0]).addClass("thumbnail");
							
							$(img).click(function(){
							
								
							
							});
							
							var currentUrl = current_item.value[0].split("/");
							var cleanUrl = currentUrl[currentUrl.length-1];
							
							var imageUrls = [cleanUrl];
							
							imageUrls = imageUrls.concat(JSON.parse(raptorjs.loadTextFileSynchronous("images.php")));
							
							var dropdownlist = document.createElement("div");
							//$(dropdownlist).attr("id", "dropdown_menu_" + b);
							
							
						$(dropdownlist).jqxDropDownList({
								filterable: true, selectedIndex: 0, source: imageUrls, itemHeight: 90, height: 30, width: 230,
								renderer: function (index, label, value) {
		
								
									urls = this.source;
									
									var datarecord = urls[index].toLowerCase();
									var imgurl = 'media/textures/sponza/' + datarecord;
									var img = '<img height="70" width="70" src="' + imgurl + '"/>';
									var table = '<table><tr><td style="width: 80px;">' + img + '</td><td style="color: white; size: 12px;">' + label + '</td></tr></table>';
									return table;
								},
								selectionRenderer: function (element, index, label, value) {
									var text = label.replace(/<br>/g, " ");
									return "<span style='left: 4px; top: 6px; position: relative;'>" + text + "</span>";
								},
								theme: "dark",
								enableBrowserBoundsDetection : true,
								dropDownHeight: 500
							}).data({"imageUrls" : imageUrls});
							
							$(dropdownlist).on('change', function (event) {
	
								var index = $(this).context.innerText;
								
								console.log("HIEr komt hij wel", index, $(this));
								
								var entity = $(this).data("entity");
								
								$(this).data("callback")(index, entity);
								
								
								//$("#jqxDropDownList").jqxDropDownList('selectIndex', index);
								//$("#jqxDropDownList").jqxDropDownList('ensureVisible', index);
								//updating = false;
								console.log(index);
							}).data({"entity" : entity, "callback": current_item.callback,});
							
							$(values).append(img, dropdownlist);
							
	
							
							
						break;
						
					}
					
					
					$(row).append(label, values);
					
					$(content).append(row);

				}
				
				$(collapseBox).append(content);
			
			}
			
			
			$(collapseBox).jqxExpander({ width: '100%', theme: "dark", expanded: true});
			$("#details").append(collapseBox);
		
		}

		
		
		function createBounding( entity ) {
		
		
				raptorjs.system.scene.removeEntityByName("boundigBox_entity");
				var material = raptorjs.createObject("material");
				
				var diffuseTexture = raptorjs.resources.getTexture("alpha");
				var diffuseSampler = raptorjs.createObject("sampler2D");
				diffuseSampler.addTexture(diffuseTexture);
				
				material.addTexture(diffuseSampler);
				material.diffuseColor = [1,1,1];
				material.alpha = .9;
				
				var box = entity;
				var boundingbox = box.mesh.boundingBox;
				
				
				var mesh = raptorjs.primitives.createCubeFromBoundingbox(boundingbox);
				
			
				mesh.addMaterial(material);

				var boundigBox_entity = raptorjs.createObject("entity");
				boundigBox_entity.addMesh(mesh);
				boundigBox_entity.name = "boundigBox_entity";
				boundigBox_entity.transform.position = box.transform.position;
				boundigBox_entity.transform.scale = box.transform.scale;
				boundigBox_entity.transform.update();

				raptorjs.system.scene.addEntity( boundigBox_entity );
				
				raptorjs.system.scene.rootEntity.children[0].addChild(boundigBox_entity);
				
		}
		
		function readPixel() {
		
			var e = raptorjs.canvas.event;
			var evt = window.event;
			
			
			var x = (evt.pageX - $("#raptorEngine").offset().left);
			var y = raptorjs.height - (evt.pageY - $("#raptorEngine").offset().top);


			var height = 1;
			var width = 1;
				
			var fb = gl.createFramebuffer();
			gl.bindFramebuffer(gl.FRAMEBUFFER, fb);
			gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, raptorjs.system.infoSampler.texture.glTexture, 0);
			
			if (gl.checkFramebufferStatus(gl.FRAMEBUFFER) == gl.FRAMEBUFFER_COMPLETE) {
			  var pixels = new Uint8Array(width * height * 4);
			  gl.readPixels(x, y, width, height, gl.RGBA, gl.UNSIGNED_BYTE, pixels);
			}
			
			var selected_id = pixels[3];
			console.log(x, y, selected_id);
			
			
			var entitys = raptorjs.system.scene.entitys;
			
			for(var c =0; c<entitys.length; c++) {
				if(entitys[c].mesh){
					console.log(selected_id, entitys[c].mesh.material.id);
				
					if(entitys[c].mesh.material.id == selected_id) {
							
						createBounding( entitys[c] );
						loadDetails(entitys[c] );
						
						
					}
				}
			}
 			
			
			
		}
		
		function debug( position ) {
		

			var material = raptorjs.createObject("material");

			var diffuseTexture = raptorjs.resources.getTexture("default");
			var diffuseSampler = raptorjs.createObject("sampler2D");
			diffuseSampler.addTexture(diffuseTexture);

			material.addTexture(diffuseSampler);
			material.diffuseColor = [1,1,1];
			material.alpha = .9;

			var mesh = raptorjs.primitives.createCube(2.6);
			mesh.addMaterial(material);

			var boundigBox_entity = raptorjs.createObject("entity");
			boundigBox_entity.addMesh(mesh);
			boundigBox_entity.name = "asd";
		
			boundigBox_entity.transform.scale = [1, 1, 1];

			boundigBox_entity.transform.position = position ;
			boundigBox_entity.transform.update();

			raptorjs.system.scene.addEntity( boundigBox_entity );
			raptorjs.system.scene.rootEntity.children[0].addChild(boundigBox_entity);
			
		}
		
		function getRay(x, y){ 
		
			var view = raptorjs.mainCamera.view;
			var projection = raptorjs.mainCamera.projection;
			
			var inverseViewProjectionMatrix =  raptorjs.matrix4.inverse(  raptorjs.matrix4.composition(projection, view) );
			var normScreenX = 0;
			var normScreenY = 0;
	  /*
			var ray_nds = [x, y, -1.0, 1.0];

			var view = raptorjs.mainCamera.view;
			var projection = raptorjs.mainCamera.projection
			
			var inverse_projection = raptorjs.matrix4.inverse(projection);
			var inverse_view = raptorjs.matrix4.inverse(view);
			
			
			var ray_eye = raptorjs.matrix4.transformVector4(inverse_projection, ray_nds);
		
			ray_eye[2] = -1;
			ray_eye[3] = 0;
			
			var ray_world = raptorjs.matrix4.transformVector4( inverse_view, ray_eye) ;

			console.log(ray_world);
		*/
					var near =	raptorjs.matrix4.transformPoint( inverseViewProjectionMatrix, [normScreenX, normScreenY, 0] );
					var far =	raptorjs.matrix4.transformPoint( inverseViewProjectionMatrix, [normScreenX, normScreenY, 1] );
		console.log(raptorjs.vector3.scale(near, 100));
		//console.log(raptorjs.vector3.scale(near, 100));
			debug( raptorjs.vector3.scale(near, 10) );
		
		}
		
		function parseMenu(parentEntity, source) {
		
			var currentNode = {};
			
			currentNode.icon = '/Scripts/RAPTOR/third party/js/jqwidgets/styles/images/earth.png';
			
			switch(parentEntity.type) {
				case "transform":
					currentNode.icon = '/Scripts/RAPTOR/third party/js/jqwidgets/styles/images//folder.png'; 
				break;
				
				case "PointLight":
					currentNode.icon = '/Scripts/RAPTOR/third party/js/jqwidgets/styles/images//light.png'; 
				break;
			
				case "Actor":
					currentNode.icon = '/Scripts/RAPTOR/third party/js/jqwidgets/styles/images//mesh.png'; 
				break;
			
			
			}
			
			
			currentNode.label = parentEntity.name;
			currentNode.id = parentEntity.name;
			currentNode.expanded = true;
			currentNode.entity = parentEntity;
			
			var tree_items = [];
			
			for(var c = 0; c<parentEntity.children.length; c++) {
				var entity = parentEntity.children[c];
				
				var tree_item = parseMenu(entity, tree_items);

			}
			
			currentNode.items = tree_items;
			
			source.push(currentNode);
			
			return currentNode;
			
		}
		
		
		var initDockingStation = function() {
		
			  // initialize a jqxTree inside the Solution Explorer Panel
				
				var rootEntity = raptorjs.system.scene.rootEntity;
				
				var source = [];
				
				var finalSource = parseMenu(rootEntity.children[0], source);
				
				$('#solutionExplorerTree').jqxTree({ source: [finalSource], width: 290, theme: 'dark' });
				$('#solutionExplorerTree').on('select', function (event) {
					var args = event.args;
					var item = $('#solutionExplorerTree').jqxTree('getItem', args.element);
					var current_entity = raptorjs.system.scene.getEntityByName(item.label);
					
					
					
					createBounding( current_entity );
					
					loadDetails(current_entity);
				});

		
		}