/*
 * Copyright 2013, Raptorcode Studios Inc, 
 * Author, Kaj Dijkstra.
 * All rights reserved.
 *
 */
 

/**
 * light
**/
raptorjs.samplerCube = function() {


	this.textures = [];
	this.cubeTexture;
	this.id = ++samplerId;
	
	this.FLIP_Y = true;
	
	this.MIN_FILTER = gl.LINEAR;
	this.MAG_FILTER = gl.LINEAR;
	
	this.WRAP_S = gl.REPEAT;
	this.WRAP_T = gl.REPEAT;
	
	this.datatype = gl.RGB;
	this.format = gl.RGB;
	this.internalFormat = gl.RGB;
	
	this.target = gl.TEXTURE_CUBE_MAP;
	
	
	this.type = gl.UNSIGNED_BYTE;

	this.alpha = 1.0;
	
	this.binded = false;
	this.anisotropic = false;
	
	//this.setTarget();
}

raptorjs.samplerCube.prototype.setTarget = function(target) {
	
	 var faces = [	gl.TEXTURE_CUBE_MAP_POSITIVE_X,
					gl.TEXTURE_CUBE_MAP_NEGATIVE_X,
					gl.TEXTURE_CUBE_MAP_POSITIVE_Y,
					gl.TEXTURE_CUBE_MAP_NEGATIVE_Y,
					gl.TEXTURE_CUBE_MAP_POSITIVE_Z,
					gl.TEXTURE_CUBE_MAP_NEGATIVE_Z ];
					
	for(var c =0; c<faces.length; c++) {
		
		var texture = raptorjs.createObject("texture");
		
		texture.face = faces[c];
		
		this.addTexture( texture );
		
	}
}

raptorjs.samplerCube.prototype.getTexture = function( face ) {

	if( face ) {
		
		for(var c = 0; c < this.textures.length; c++) {
			
			if( face == this.textures[c].face ) {
				
				return this.textures[c];
				
			}
			
		}
		
	} else {
		
		return this.textures[0];
		
	}
	
}

raptorjs.samplerCube.prototype.getTextures = function( ) {
	return this.textures;
}

raptorjs.samplerCube.prototype.addTexture = function(texture, face) {

	
	this.textures.push({texture: texture, face : face});
}

/**
 * bind sampler to shader
 * @param {(shader)} shader
**/
raptorjs.samplerCube.prototype.bind = function( shader ) {
	this.cubeTexture = gl.createTexture();
	
	var glTexture = this.cubeTexture;
	
	gl.enable ( gl.BLEND ) ;
	//gl.activeTexture(gl.TEXTURE0 + this.id);
	
	gl.bindTexture(gl.TEXTURE_CUBE_MAP,  glTexture );
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, this.FLIP_Y);

	if(!this.binded) 
		this.id = shader.samplerId++;
		
		
	for(var c = 0; c<this.textures.length; c++) {
		
		var texture = this.textures[c].texture;
		var data = texture.data;
		var type = texture.dataType;
		var face = this.textures[c].face;
		
		this.type = type;
		
		console.log("binding", texture, data );
		var mips = [];
		var width = texture.width;
		var height = texture.height;
			
			//serialize texture data type
			switch( type ) {
				case "float":
					gl.texImage2D(face, 0, this.format, width, height, 0, this.internalFormat, gl.FLOAT, data);
				break;
				case "int":
					gl.texImage2D(face, 0, this.format, width, height, 0, this.internalFormat, gl.UNSIGNED_BYTE, data);
				break;
				case "depth":
					gl.texImage2D(face, 0, gl.DEPTH_COMPONENT, width, height, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_SHORT, null);
				break;
				case "image":
					gl.texImage2D(face, 0, this.format, this.internalFormat, gl.UNSIGNED_BYTE, data);
				break;
				case "canvas":
					gl.texImage2D(face, 0, this.format, width, height, 0, this.internalFormat, this.UNSIGNED_BYTE, data);
				break;
				case "COMPRESSED_RGBA":

					//var textureCompression = raptorjs.extensions.textureCompression;
					var mipmaps = texture.mipmaps;
					
					var width = mipmaps[0].width;
					var height = mipmaps[0].height;
					
					for(var i = 0; i < mipmaps.length; i++) {
						var mipmap = mipmaps[i];
						
						gl.compressedTexImage2D(texture.face, i, mipmap.internalFormat, mipmap.width, mipmap.height, 0, mipmap.byteArray);
					}
				
				break;
			}
			
		if (isPowerOfTwo(width) && isPowerOfTwo(height) ) {
		
		//	gl.texParameteri(this.target, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		//	gl.texParameteri(this.target, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);// mipmaps > 1 ? gl.LINEAR_MIPMAP_LINEAR : gl.LINEAR 
		//	gl.texParameteri(this.target, gl.TEXTURE_WRAP_S, this.WRAP_S);
		//	gl.texParameteri(this.target, gl.TEXTURE_WRAP_T, this.WRAP_T);
		gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		
		
			//gl.generateMipmap(gl.TEXTURE_2D);
			
		} else {

			gl.texParameteri(this.target, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
			gl.texParameteri(this.target, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
			gl.texParameteri(this.target, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
			
		}
			
			

		}

		

		
		gl.bindTexture(this.target, null);
		
		this.binded = true;
		
	
}


/**
 * check if object is array
 * @return {(boolean)} 
**/
function is_array(input){
	return typeof(input)=='object'&&(input instanceof Array);
}