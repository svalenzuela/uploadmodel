/**
 * Raptor Engine - Core
 * Copyright (c) 2013 RAPTORCODE STUDIOS
 * All rights reserved.
 *

 */
  
/**
 *	 Author: Kaj Dijksta
 */

	var raptorjs = raptorjs || {};
	var gl;
	
	//core vars
	raptorjs.math;
	raptorjs.resourceManager;
	raptorjs.events;
	raptorjs.mainCamera;
	raptorjs.mainPlayer;
	raptorjs.client = {};
	raptorjs.elapsed = 0;
	raptorjs.timeNow = 0.01;
	raptorjs.extensions = {};
	raptorjs.canvas;
	raptorjs.engine;
	raptorjs.resources;
    raptorjs.lastTime = 0;
	
	//plugin related vars
	raptorjs.seaInstance;
	raptorjs.skyInstance;
	raptorjs.grayImage;
	raptorjs.sunLight;
	raptorjs.errorTexture;
	raptorjs.planet;
	raptorjs.oceanInstance;
	raptorjs.pointSprite;
	raptorjs.ocean;
	raptorjs.assimpLoader;
	
	/**
	 * Application
	 */
	raptorjs.application = function ( ) {
	
		//allocate Objects
		raptorjs.mainPlayer = raptorjs.createObject("player");
		raptorjs.events = raptorjs.createObject("eventManager");
		raptorjs.scene = raptorjs.createObject("sceneNode");
		raptorjs.mainCamera = raptorjs.createObject("camera");
		raptorjs.assimpLoader = raptorjs.createObject("assimp");
		
		
		raptorjs.createErrorTexture();

		
		
		//set camera and scene
		//raptorjs.system.setCamera( raptorjs.mainCamera );
		//raptorjs.system.setScene( raptorjs.scene );
	
		
		raptorjs.resources = raptorjs.createObject("resourceManager");
	
		raptorjs.resources.addTexture("media/textures/white.png", 'white');
		//raptorjs.resources.loadShader("shaders/color.shader");
			
	
		raptorjs.resources.finishCallback = function( ) {	
		
			
			raptorjs.system.createQuad();
			//raptorjs.system.createShadows();
			
			raptorjs.system.createMultipleRenderTargets();
			
			
			tick();



	//raptorjs.assimpLoader.load("sponza.json");
	//raptorjs.assimpLoader.load("Light_Bulb.json");
	//	raptorjs.assimpLoader.load("colorLight.json");	
		//	raptorjs.assimpLoader.load("thuis.json");
			
		//	raptorjs.assimpLoader.load("tt.json");	
		//raptorjs.assimpLoader.load("colorLight.json");	
		//raptorjs.assimpLoader.load("lol.json");
		//raptorjs.assimpLoader.load("troll.json");			
		//	raptorjs.assimpLoader.load("scale.json");	
		//raptorjs.assimpLoader.load("f35.json");
		////	raptorjs.assimpLoader.load("bf.json");
		// raptorjs.assimpLoader.load("sphere.json");
			raptorjs.resources.finishCallback = function( ) {		

		
				raptorjs.mainCamera.update();
				raptorjs.mainPlayer.update();
				raptorjs.assimpLoader.buildModels();

				raptorjs.system.ready = true;
		
				var material = raptorjs.createObject("material");
				
				var normalTexture = raptorjs.resources.getTexture("leather");
				var normalSampler = raptorjs.createObject("sampler2D");
				normalSampler.addTexture(normalTexture);
				
				
				
				var diffuseTexture = raptorjs.resources.getTexture("color");
				var diffuseSampler = raptorjs.createObject("sampler2D");
				diffuseSampler.addTexture(diffuseTexture);
				//material.addNormal(normalSampler);
				material.addTexture(diffuseSampler);
				//material.diffuseColor = [88,175,255];
				material.alpha = 1;
				
				//var box = raptorjs.system.scene.getEntityByName("Box001");

				//var mesh = raptorjs.primitives.createCube(2.6);
				var mesh = raptorjs.primitives.createSphere(2.6, 200, 200) 
			
				mesh.addMaterial(material);
				
				
				var entity = raptorjs.createObject("entity");
				entity.addMesh(mesh);
				entity.name = "new box";

			//	entity.transform.world = raptorjs.matrix4.scale(raptorjs.matrix4.copyMatrix (box.transform.world), [1.2, 1.2, 1.2]);

				
				
				raptorjs.system.scene.addEntity( entity );
				
				raptorjs.system.scene.rootEntity.children[0].addChild(entity);
				
				/*
				
				
				var box = raptorjs.system.scene.getEntityByName("Box001");
				var world = box.transform.world;
				var normWorld = raptorjs.matrix3.transpose(normMatrix((world)));
				
				var x = Math.atan2(normWorld[1][2], normWorld[2][2]) * (180/Math.PI) ;
				var y = Math.atan2(-normWorld[0][2], Math.sqrt( (normWorld[1][2] * normWorld[1][2]) + (normWorld[2][2] * normWorld[2][2])   ) ) * (180/Math.PI) ;
				var z = Math.atan2(normWorld[0][1], normWorld[0][0]) * (180/Math.PI) ;// * (180/Math.PI)
				
				console.log("asddsa", normWorld, x, y, z);
				*/
				initDockingStation();
				
				

			}

			raptorjs.resources.loadNextTexture();
		}
		
		raptorjs.resources.addTexture("media/textures/alpha.png", 'alpha');
		raptorjs.resources.addTexture("media/textures/defaultnouvs2.png", 'default');
		raptorjs.resources.addTexture("media/textures/rotrandom.png", 'random');
		raptorjs.resources.addTexture("media/textures/sponza/color.png", 'color');
		raptorjs.resources.addTexture("media/textures/sponza/leather_bump1.png", 'leather');
		var diffuseTexture = raptorjs.resources.getTexture("color");
		/*
		raptorjs.resources.addTexture("media/textures/cubemaps/posx.jpg", 'positive_x');
		raptorjs.resources.addTexture("media/textures/cubemaps/posy.jpg", 'positive_y');
		raptorjs.resources.addTexture("media/textures/cubemaps/posz.jpg", 'positive_z');
		
		raptorjs.resources.addTexture("media/textures/cubemaps/negx.jpg", 'negative_x');
		raptorjs.resources.addTexture("media/textures/cubemaps/negy.jpg", 'negative_y');
		raptorjs.resources.addTexture("media/textures/cubemaps/negz.jpg", 'negative_z');
		
		*/
		
				raptorjs.resources.addTexture("media/textures/cubemaps/positive_x.png", 'positive_x');
		raptorjs.resources.addTexture("media/textures/cubemaps/positive_y.png", 'positive_y');
		raptorjs.resources.addTexture("media/textures/cubemaps/positive_z.png", 'positive_z');
		
		raptorjs.resources.addTexture("media/textures/cubemaps/negative_x.png", 'negative_x');
		raptorjs.resources.addTexture("media/textures/cubemaps/negative_y.png", 'negative_y');
		raptorjs.resources.addTexture("media/textures/cubemaps/negative_z.png", 'negative_z');
		raptorjs.resources.loadNextTexture();
    }
	
	
	/**
	 * Rendercallback gets called every frame
	 */
    raptorjs.renderCallback = function ( ) {
		if(raptorjs.system.ready) {
		
			raptorjs.mainPlayer.update();
			raptorjs.mainCamera.update();
			raptorjs.system.render();
			
			//raptorjs.system.render();
		//	raptorjs.lightshafts.update();
		} else {
			//raptorjs.logo.update();
		
		}
		
		
		//raptorjs.oceanInstance.pipeline();
		//raptorjs.planet.update();
		//raptorjs.oceanInstance.drawQuad(raptorjs.ocean.shader, null);

    }
	
	
	/**
	 * Initialize raptor framework
	 */
	raptorjs.initialize =  function () {
        var canvas = document.getElementById("raptorEngine");

		raptorjs.system = raptorjs.createObject("renderSystem");
		raptorjs.system.setGraphicsLibrary("WebGL_1.0");
        raptorjs.system.initializeWebgl(canvas);
		
		raptorjs.application();
    }

	
	/**
	 * create raptorjs object
	 * @param {string} name of class
	 */
	raptorjs.createObject = function(className) {
		var instance =  raptorjs[className];
		var object = new instance;
		return object;
		if (typeof instance != 'function') {
			throw 'raptorjs doesnt seem to contain the namespace: ' + className
		} else {
			return object;
		}
	};

	window.unload = function( ) {
		
	}

	
	/**
	 * load url
	 * @param {string} url 
	 */
	raptorjs.loadTextFileSynchronous = function(url) {
		var request;
		console.log('abc');
		if (window.XMLHttpRequest) {
			request = new XMLHttpRequest();
			if (request.overrideMimeType) {
			  request.overrideMimeType('text/plain');
			}
		} else if (window.ActiveXObject) {
			request = new ActiveXObject('MSXML2.XMLHTTP.3.0');
		} else {
			throw 'XMLHttpRequest is disabled';
		}
		
		request.open('GET', url, false);
		request.send(null);
		
		if (request.readyState != 4) {
			throw error;
		}
		
		return request.responseText;
			return shaders;
	}
	
	
	/**
	 * Deprecated
	 */
	raptorjs.clone = function (obj) {
		var cloneObject = raptorjs.createObject(obj._className);
		for(var el in obj) {
			cloneObject[el] = obj[el];
		}
		return cloneObject;
	}
	
	
	/**
	 * Tick
	 */
    function tick() {
        requestAnimFrame(tick);
		raptorjs.system.smoothTimeEvolution();
        raptorjs.renderCallback();
    }
	
	
	/**
	 * Benchmark a function
	 */
	function benchmarkFunction(funct, times) {
		var date1 = new Date(); 
		var milliseconds1 = date1.getTime(); 
		var j = 0;
		
		for (i = 0; i < times; i++) { 
			funct();
		}
		
		var date2 = new Date(); 
		var milliseconds2 = date2.getTime(); 

		var difference = milliseconds2 - milliseconds1; 
		console.log(difference, 'millisec'); //, funct
	}
	raptorjs.lastTimeCheck = 0;
	
	/*
	 *	Create error texture
	 */
	raptorjs.createErrorTexture = function() {
		var dataArray = [];
		var width = 512;
		var height = 512;
		
		for( var y = 0; y < height; y++ )
		{
			for( var x = 0; x < width; x++ )
			{
				dataArray.push(x / width);
				dataArray.push( y / width);

				dataArray.push(  x / x / width);
				dataArray.push(  y * x / width); 
			}
		}
		
		var text = raptorjs.textureFromArray(dataArray, width, height, true);
		var sampler = raptorjs.createObject("sampler2D");
		sampler.texture = text;
	
		raptorjs.errorTexture = sampler;
	}
	
	raptorjs.setLoadingText = function ( message ) {
		
		var date2 = new Date(); 
		var milliseconds1 = raptorjs.lastTimeCheck;
		var milliseconds2 = date2.getTime(); 
		var difference = milliseconds2 - milliseconds1; 
		
		console.log(" time : ", difference, message);
	
		raptorjs.lastTimeCheck =  new Date().getTime();
		//$(".loadingScreen").text(message);
		
	}
	
	
	function removeElementById(id)
	{
		return (elem=document.getElementById(id)).parentNode.removeChild(elem);
	}
	
	
