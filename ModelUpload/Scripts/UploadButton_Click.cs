protected void UploadButton_Click(object sender, EventArgs e)
		{
		    if(Upload.HasFile)
		    {
		        try
		        {
		            string filename = Path.GetFileName(FileUploadControl.FileName);
		            Upload.SaveAs(Server.MapPath("~/") + filename);
		            StatusLabel.Text = "Upload status: File uploaded!";
		        }
		        catch(Exception ex)
		        {
		            StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
		        }
		    }
		}